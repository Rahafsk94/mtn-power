<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;

/*Route::group(['middleware' => ['role:user']], function () {


});*/
Route::get('test-notification', 'HomeController@sendToken');
Route::post('/', 'HomeController@storeToken')->name('send');
Route::post('forget-password','auth\ForgotPasswordController@submitForgetPasswordForm')->name('forget.password.post'); 
Route::get('passwords/reset', 'auth\ForgotPasswordController@showForgetPasswordForm')->name('forget.password.get');
Route::get('reset-password/{token}', 'auth\ForgotPasswordController@showResetPasswordForm')->name('reset.password.get');
Route::post('reset-password', 'auth\ForgotPasswordController@submitResetPasswordForm')->name('reset.password.post');
Route::get('/mark-as-read', 'auth\ForgotPasswordController@markAsRead')->name('mark-as-read');

        Auth::routes();

        Route::post('sitecodename', 'SiteController@siteCodeName')->name('sitecodename');
        Route::post('lo', [App\Http\Controllers\loController::class, 'logout'])->name('lo');

       

        Route::group(['middleware' => ['auth']], function() {
            Route::get('solars/{solar}/e', 'SolarController@editnumber');
            Route::post('solar/s', 'SolarController@updatenumber');
        
        Route::resource('sites', 'siteController');
        Route::get('/siteimport', 'sitecontroller@import_excel')->name('siteimport');  
        Route::get('powersolution', 'siteController@solution')->name('solution');
        Route::resource('generators', 'GeneratorController');
        Route::get('generator/warehouse', 'GeneratorController@warehouse')->name('generatorswarehouse');
        Route::get('generators/add/action', 'GeneratorController@addaction')->name('addaction');
        Route::resource('tanks', 'TankController');
        
        Route::resource('solars', 'SolarController');  
        Route::get('solar/warehouse', 'SolarController@warehouse')->name('solarswarehouse');
        Route::get('tanks/newcreate/{generator}', 'TankController@creategeneratortank')->name('newtankcreate');
        Route::post('tanks/newcreate/{generator}', 'TankController@storegeneratortank')->name('newtankscreate');
        Route::get('tanksimport', 'TankController@import_excel')->name('tanksimport');
        Route::post('import-tank', 'TankController@import');
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
        Route::get('/poweractions', [App\Http\Controllers\HomeController::class, 'poweractions'])->name('poweractions');
       
        Route::get('warehousesolution', 'warehouseController@solution')->name('warehoussolution');

  
        Route::resource('warehouses', 'WarehouseController');
       
        Route::get('battery/warehouse', 'BatteryController@warehose')->name('batterywarehouse');
       
        Route::post('cheack_warehouse', 'WarehouseController@cheackWarehouse')->name('cheack_warehouse');



        Route::get('cabinet/warehouse', 'CabinetController@warehouse')->name('cabinetwarehouse');
        Route::get('cabinet/shipment/create', 'CabinetController@craeteShipment')->name('ch');
       Route::post('cabinet/shipments/create', 'CabinetController@addShipment')->name('csh');

       Route::resource('batteries', 'BatteryController');

       Route::get('battery/shipment/create', 'BatteryController@craeteShipment');
       Route::post('battery/shipment', 'BatteryController@addShipment')->name('batteryshipment');
       Route::resource('cabinets', 'CabinetController');
        Route::group(['middleware' =>['role:Management']], function () {

        Route::resource('roles', 'RoleController');
        Route::resource('users', 'UserController');
        Route::get('site/track', 'AuditsController@track');
        Route::get('cabinet/track', 'AuditsController@cabinetTrack')->name('cabinet_track');
        Route::get('generator/track', 'AuditsController@index')->name('track');
        Route::get('generator/track/{generator}', 'AuditsController@trackshow')->name('generatortrack');
      
        Route::get('tank/track', 'AuditsController@tankTrack')->name('tank_track');
        Route::get('solar/track', 'AuditsController@solarTrack')->name('solar_track');

        Route::post('site/destroy/{id}',  'SiteController@des')->name('des');
        Route::get('siteexport','sitecontroller@export');
        Route::get('battery/track', 'AuditsController@batterytrack')->name('battery-track');

                                                                 });

    


    
     
        Route::resource('amperes', 'AmpereController');
      
     

        Route::get('import-file', 'HomeController@import')->name('import-file');
        Route::get('batteryimport', 'BatteryController@import_ex')->name('batteryimport');
        Route::post('import-battery', 'BatteryController@importExcelCSV');
        Route::get('generatorimport', 'GeneratorController@import_ex')->name('generatorimport');
        Route::post('import-excel-csv-file', 'GeneratorController@importExcelCSV');
        Route::get('ampereimport', 'AmpereController@import_ex')->name('ampereimport');
        Route::post('import-ampere', 'AmpereController@importExcelCSV');
       

        Route::get('solarimport', 'SolarController@import_excel')->name('solarimport');
        Route::post('import-solar', 'SolarController@import');
        Route::get('cabinet_import', 'CabinetController@import_excel')->name('cabinetimport');
        Route::post('import-cabinet', 'CabinetController@import');
       
        Route::post('import', 'sitecontroller@import');  



    Route::get('genes/{generator}/e', 'GeneratorController@editnumber');
 
    Route::post('generator/s', 'GeneratorController@updatenumber');

   
    Route::get('batts/{battery}/e', 'BatteryController@editnumber');
    Route::post('battery/s', 'BatteryController@updatenumber');
    Route::get('battery/shipment/create', 'BatteryController@craeteShipment')->name('batteryshipmentcreate');
 
    Route::get('in', 'GeneratorController@in');

   

    Route::post('tabledit/action', 'GeneratorController@action')->name('tabledit.action');
    Route::get('searchform', [App\Http\Controllers\GeneratorController::class,'compare'])->name('searchform');
   /* Route::get('searchform', 'GeneratorController@form');*/

    Route::get('searchs', 'GeneratorController@search')->name('searchs');

    Route::get('cabinet/dismantle', 'GeneratorpoweractionController@cabinetDismantle');
    Route::get('cabinet/reshuffling', 'GeneratorpoweractionController@cabinetReshuffling');
    Route::get('cabinet/install', 'GeneratorpoweractionController@cabinetInstall');



    Route::get('sitesearch', [App\Http\Controllers\siteController::class,'search'])->name('sitesearch');
    
 
    Route::get('reshuffling', 'GeneratorpoweractionController@reshuffling')->name('re');
    Route::get('dismantle', 'GeneratorpoweractionController@dismantle')->name('dismantle');
    Route::get('battery/dismantle', 'GeneratorpoweractionController@batteryDismantle')->name('bdismantle');
    Route::get('battery/reshuffling', 'GeneratorpoweractionController@batteryReshuffling')->name('bre');
    Route::get('install', 'GeneratorpoweractionController@install')->name('install');
    Route::get('battery/install', 'BatteryController@install')->name('batteryinstall');
    Route::get('solar/install', 'SolarController@install')->name('solarinstall');
    Route::get('poweractions/create/', 'GeneratorpoweractionController@create')->name('powercreate');
    Route::put('/poweractions/create', 'GeneratorpoweractionController@store')->name('powerscreate');
    Route::put('/poweractions/create/rep', 'GeneratorpoweractionController@storee')->name('powerscreaterep');
  //  Route::put('/poweractions/reinstall', 'GeneratorpoweractionController@reinstall')->name('reinstall');
    Route::put('/poweractions/create/battery', 'GeneratorpoweractionController@batterystore')->name('powerscreatebattery');
   
    Route::get('generator/shipment/create', 'GeneratorController@craeteShipment')->name('shipmentcreate');
    Route::get('solar/shipment/create', 'SolarController@craeteShipment')->name('solarshipmentcreate');
    Route::post('solar/shipment', 'solarController@addShipment')->name('solarshipment');
    Route::post('generator/shipment', 'GeneratorController@addShipment')->name('shipment');
  

    Route::get('poweraction', [App\Http\Controllers\GeneratorController::class,'powerAction'])->name('poweraction');
  
   
    Route::resource('gene', 'AuditsController');
 
    Route::get('ampere/track', 'AuditsController@amperetrack');
   
    Route::get('/users/{id}/password', 'UserController@password')->name('editpassword');
    Route::post('/users/{id}/password', 'UserController@changePassword')->name('userpassword');

    Route::get('cabinetactions', 'CabinetController@actions')->name('cabinetactions');
    Route::get('bactions', 'BatteryController@actions')->name('bactions');
    Route::get('solar/actions', 'SolarController@addaction')->name('solaractions');



    Route::delete('tanks/{tank}','TankController@destroy')->name('tankdes');
  

 

    
 



});


Route::get('/p', [App\Http\Controllers\siteController::class, 'test']);



 Route::get('oi', 'sitecontroller@exportt');
 Route::post('oi', 'sitecontroller@exportt');
 Route::get('generator/export/',  'GeneratorController@export')->name('geneexport');

 Route::get('cabinets/{cabinet}/e', 'CabinetController@editnumber');
 Route::post('cabinets/s', 'CabinetController@updatenumber');

 Route::get('dismantle/{generator}/e', 'generatorController@editdismantle');
 Route::post('dismantle/s', 'generatorController@updatedismantle');
 
 Route::get('reshuffling/{generator}/e', 'generatorController@editreshuffling');
 Route::post('reshuffling/s', 'generatorController@updatereshuffling');
 Route::get('cabinetreshuffling/{cabinet}/e', 'cabinetController@editreshuffling');
 Route::post('cabinetreshuffling/s', 'cabinetController@updatereshuffling');

 Route::get('batterydismantle/{battery}/e', 'batteryController@editdismantle');
 Route::post('batterydismantle/s', 'batteryController@updatedismantle');
  
 Route::get('batteryreshuffling/{battery}/e', 'batteryController@editreshuffling');
 Route::post('batteryreshuffling/s', 'batteryController@updatereshuffling');

 Route::get('cabinetdismantle/{cabinet}/e', 'cabinetController@editdismantle');
 Route::post('cabinetdismantle/s', 'cabinetController@updatedismantle');

 Route::get('solarsreshuffling/{solar}/e', 'solarController@editreshuffling');
 Route::post('solarsreshuffling/s', 'solarController@updatereshuffling');
 Route::get('solar/reshuffling', 'GeneratorpoweractionController@solarReshuffling')->name('solarre');


 Route::get('solardismantle/{solar}/e', 'solarController@editdismantle');
 Route::post('solardismantle/s', 'solarController@updatedismantle');
 Route::get('solar/dismantle', 'GeneratorpoweractionController@solarDismantle')->name('solardismantle');
 Route::get('cab/{cabinet}/e', 'CabinetController@editnumber');
 Route::post('cab/s', 'CabinetController@updatenumber');
 Route::post('cheack_site', 'AmpereController@cheackSite')->name('cheack_site');
 Route::post('cheack_generator', 'GeneratorController@cheackGenerator')->name('cheack_generator');
 Route::post('cheack_site_generator', 'GeneratorController@cheackSiteGenerator')->name('cheack_site_generator');
 Route::POST('cheack_cabinet', 'BatteryController@cheackCabinet')->name('cheack_cabinet');
// Route::POST('cheack_cabinets', 'BatteryController@cheackCabinets')->name('cheack_cabinets');
 Route::POST('cheack_solar', 'BatteryController@cheackSolar')->name('cheack_solar');
 //Route::POST('cheack_solars', 'BatteryController@cheackSolars')->name('cheack_solars');
 Route::POST('cheack_tankgenerator', 'TankController@cheackTankGenerator')->name('cheack_tankgenerator');
/*Route::group(['middleware' => ['role:admin']], function () {
Route::get('users','UserController@index');
});*/
//Route::get('search', [App\Http\Controllers\siteController::class,'search'])->name('sitesearch')->middleware('roles');
/*Route::get('home/',function(){
    return view('home.hello');
});*/

//Route::get('o',[App\Http\Controllers\HomeController::class, 'admin']);
/*Route::get('search',[
    'uses'=>'siteController@search',
    'as'=>'sites.search',
    'middleware'=>'roles',
    'roles'=>['user']
])->name('sitesearch');*/
/*Route::group(['middleware'=>'roles',

'roles'=>['admin']
],function(){
    Route::resource('Sites', 'siteController');
    Route::get('sitesearch', [App\Http\Controllers\siteController::class,'search'])->name('sitesearch');
});*/
/*Route::resource('Sites', 'siteController')->middleware('role:admin');
Route::get('sitesearch', [App\Http\Controllers\siteController::class,'search'])->name('sitesearch');*/


/*Route::get('r', function () {
    if(auth()->user()){
        auth()->user()->assignRole();
        return view('layouts.hi');
    }

});*/

/*Route::group(['middleware' => ['role:User']], function () {
    Route::resource('Sites', 'siteController');
    Route::get('sitesearch', [App\Http\Controllers\siteController::class,'search'])->name('sitesearch');
});*/
Route::view('hell','y',['id'=>'rahaf']);
Route::get('hell/{id}/{idd}',function(string $id,string $idd){


    return 'id'.$id.'idd'.$idd;
});
