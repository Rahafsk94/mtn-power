<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

use Spatie\Permission\Models\Role;

use Spatie\Permission\Models\Permission;



class CreateAdminUserSeeder extends Seeder

{
  

    public function run()

    {

        $user = User::create([

            'name' => 'Rahaf',
            'email' => 'Rahaf@gmail.com',
            'password' => bcrypt('12345678'),

        ]);
        $role1 = Role::create(['name' => 'Management']);
        $role2 = Role::create(['name' => 'Super admin']);
        $role3 = Role::create(['name' => 'Admin']);
        $role4 = Role::create(['name' => 'User']);
   
        $permissions = Permission::pluck('id','id')->all();

       // $role1->syncPermissions($permissions);
 Role::findByName('Management')->givePermissionTo($permissions);
        $user->assignRole('Management');

    }

}
