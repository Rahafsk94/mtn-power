<?php



namespace Database\Seeders;



use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Permission;



class PermissionTableSeeder extends Seeder

{

    /**

     * Run the database seeds.

     *

     * @return void

     */

    public function run()

    {

        $permissions = [
            'browse',
            'delete',
            'import',
            'export',
            'create',
        'create user',
            'create role',
            'logistic admin',
            'edit',
            'addaction',
            'logistic user',
            'power admin',
            'power user',
            'user',
            'admin',
            'manage user',
            'manage roles',
            'manage permissions',
            'track'

        ];



        foreach ($permissions as $permission) {

             Permission::create(['name' => $permission]);

        }

    }

}
