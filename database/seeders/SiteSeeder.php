<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Site;
class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {

            $site = Site::create([
    
                'site_code' => 'warehouse',  
                'finance_code' => 'warehouse',
                'site_name' => 'warehouse',
                'priority' => '10',
                'area' => 'Damascus',
                'location' =>  'Damascus',
                'sub_location' =>  'Damascus',
                'oracle_location' =>  'Damascus',
                'province' =>  'Damascus',
                'tx_category' => 'tx',
            'installation_date'=>'2023-03-09',
         
           
                'band'=> '1',
                'zone' => 'dam',
                'c_r' => 'C',
                'supplier' => 'warehouse',
                'coordinates_e' => 'e',
                'coordinates_n' => 'n',
                'rationning_hours' => '10',
    
            ]);
    
            
      
    
    
        }
    }
}
