<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Poweraction;
class PoweractionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $poweractions = [
           
           
        
        'installation', 
        'dismantle',
        'reshuffling',
         ];
         
        foreach ($poweractions as $poweraction) {

            Poweraction::create(['action_name' => $poweraction]);

       }
    }
}
