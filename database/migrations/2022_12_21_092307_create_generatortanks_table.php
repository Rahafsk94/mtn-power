<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneratortanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generatortanks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tank_id')->unsigned();
            $table->bigInteger('generator_id')->unsigned();
            $table->foreign('tank_id')->references('id')->on('tanks')->ondelete('cascade')->onupdate('cascade');
            $table->foreign('generator_id')->references('id')->on('generators')->ondelete('cascade')->onupdate('cascade');
            $table->date('installation_date')->nullable();
            $table->date('end_date')->nullable();
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generatortanks');
    }
}
