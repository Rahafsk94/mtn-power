<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCabinetsTable extends Migration
{
    public function up()
    {
        Schema::create('cabinets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('site_id')->unsigned()->nullable();
            $table->foreign('site_id')->references('id')->on('sites')->ondelete('cascade')->onupdate('cascade');
            $table->bigInteger('warehouse_id')->unsigned()->nullable();
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->ondelete('cascade')->onupdate('cascade');
            $table->integer('number');
            $table->string('cabinet_name');
            $table->string('oracle_code');
            $table->string('is_used');
            $table->boolean('is_active')->default(1);
            $table->date('installation_date')->nullable();
            $table->date('shipment_date')->nullable();
            $table->date('dismantle_date')->nullable();
            $table->timestamps();
        });
    }

  
    public function down()
    {
        Schema::dropIfExists('cabinets');
    }
}
