<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolarsTable extends Migration
{
    
    public function up()
    {
        Schema::create('solars', function (Blueprint $table) {
            $table->id();
            $table->string('po');
            $table->string('controller');
        
            $table->string('solar_project')->nullable();
     
            $table->string('panel_brand');
            $table->string('panel_capacity');
            $table->bigInteger('site_id')->unsigned()->nullable();
            $table->bigInteger('warehouse_id')->unsigned()->nullable();
            $table->Integer('number_of_panel');
            $table->Integer('number_of_charger');
            $table->foreign('site_id')->references('id')->on('sites')->ondelete('cascade')->onupdate('cascade');
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->ondelete('cascade')->onupdate('cascade');
            $table->boolean('is_active')->default(1); 
            $table->date('installation_date')->nullable();
            $table->date('shipment_date')->nullable();
            $table->date('dismantle_date')->nullable();
             $table->string('status')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solars');
    }
}
