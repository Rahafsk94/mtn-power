<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatteriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batteries', function (Blueprint $table) {
            $table->id();
            $table->biginteger('site_id')->unsigned()->nullable();
            $table->biginteger('user_id')->unsigned()->nullable();
            $table->biginteger('cabinet_id')->unsigned()->nullable();
            $table->foreign('site_id')->references('id')->on('sites')->onupdate('cascade')->ondelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onupdate('cascade')->ondelete('cascade');
            $table->foreign('cabinet_id')->references('id')->on('cabinets')->onupdate('cascade')->ondelete('cascade');
            $table->String('battery_brand');
            $table->biginteger('solar')->unsigned()->nullable();
            $table->foreign('solar')->references('id')->on('solars')->onupdate('cascade')->ondelete('cascade');
            $table->float('capacity');
            $table->integer('numbers');
            $table->integer('po');
            $table->string('status')->nullable();
            $table->date('installation_date')->nullable();
            $table->date('shipment_date')->nullable();
            $table->date('dismantle_date')->nullable();
            $table->String('oracle_code')->nullable();
            $table->boolean('is_active')->default(1);
            $table->bigInteger('warehouse_id')->unsigned()->nullable();
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->ondelete('cascade')->onupdate('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('batteries');
    }
}
