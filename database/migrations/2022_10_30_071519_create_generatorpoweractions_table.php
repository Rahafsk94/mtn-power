<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneratorpoweractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generatorpoweractions', function (Blueprint $table) {
            $table->id();
            $table->String('user_name');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('generator_id')->unsigned()->nullable();
            $table->bigInteger('cabinet_id')->unsigned()->nullable();
            $table->bigInteger('solar_id')->unsigned()->nullable();
            $table->bigInteger('battery_id')->unsigned()->nullable();
            $table->Integer('pr')->nullable();
            $table->Integer('rm')->nullable();
            $table->Integer('trx')->nullable();
            $table->String('old_site')->nullable();
            $table->String('new_site')->nullable();
            $table->bigInteger('action_id')->unsigned();
            $table->foreign('battery_id')->references('id')->on('batteries')->ondelete('cascade')->onupdate('cascade');
            $table->foreign('cabinet_id')->references('id')->on('cabinets')->ondelete('cascade')->onupdate('cascade');
            $table->foreign('solar_id')->references('id')->on('solars')->ondelete('cascade')->onupdate('cascade');
            $table->foreign('generator_id')->references('id')->on('generators')->ondelete('cascade')->onupdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade')->onupdate('cascade');
            $table->foreign('action_id')->references('id')->on('poweractions')->ondelete('cascade')->onupdate('cascade');
            $table->timestamps();

         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generatorpoweractions');
    }
}
