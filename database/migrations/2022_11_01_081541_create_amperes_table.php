<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmperesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amperes', function (Blueprint $table) {
            $table->id();
            $table->biginteger('site_id')->unsigned()->nullable();
            $table->string('ampere_provider',15);
            $table->string('payment_method',8);
            $table->string('payment_type');
            $table->float('agreed_rh');
            $table->float('ampere_capacity');
            $table->string('nots')->nullable();
            $table->date('installation_date');
            $table->boolean('is_active')->default(1);
            $table->date('end_date')->nullable();
            $table->foreign('site_id')->references('id')->on('sites')->onupdate('cascade')->ondelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amperes');
    }
}
