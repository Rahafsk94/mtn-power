
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table)
        {

            $table->id();
            $table->string('site_code',40);
            $table->string('finance_code');
            $table->string('site_name',40);
            $table->integer('priority');
            $table->string('area');
            $table->string('location');
            $table->string('sub_location');
            $table->string('oracle_location');
            $table->string('province');
            $table->string('tx_category');
            $table->date('installation_date');
            $table->date('end_date')->nullable();
            $table->boolean('is_active')->default(1);
            $table->string('band');
            $table->string('zone',5);
            $table->string('c_r');
            $table->string('supplier');
            $table->string('coordinates_e');
            $table->string('coordinates_n');
            $table->double('rationning_hours');
            $table->boolean('on_off_air')->default(1);
            $table->integer('generator')->default(0);
            $table->integer('battery')->default(0);
            $table->integer('ampere')->default(0);
            $table->integer('solar')->default(0);
            $table->integer('tank')->default(0);
            $table->integer('cabinet')->default(0);


           


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
