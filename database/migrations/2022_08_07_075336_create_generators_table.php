<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateGeneratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generators', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('site_id')->unsigned()->nullable();
            $table->bigInteger('warehouse_id')->unsigned()->nullable();
            $table->String('updated_by')->nullable();
            $table->foreign('site_id')->references('id')->on('sites')->ondelete('cascade')->onupdate('cascade');
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->ondelete('cascade')->onupdate('cascade');
            $table->String('generator_name');
            $table->biginteger('generator_capacity');
            $table->String('engine_brand');
            $table->String('owners');
            $table->boolean('is_active')->default(1);
            $table->date('installation_date')->nullable();
            $table->date('shipment_date')->nullable();
            $table->date('end_date')->nullable();
            $table->String('site_code_gen_id');
            $table->String('oracle_code')->nullable();
            $table->date('dismantle_date')->nullable();
            $table->float('runing_hour');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generators');
    }
}
