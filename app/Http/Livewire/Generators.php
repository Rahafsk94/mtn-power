<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Generator;

class Generators extends Component
{

    public $site_id , $generator_id, $site_code;    public $updateMode = false;
    public $inputs = [];
    public $i = 1;
    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }
    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    private function resetInputFields(){

        $this->site_id = '';
    }

    public function update()
    {
        $validatedDate = $this->validate([
                'site_id.0' => 'required',
          
            ],
            [
                'site_id.0.required' => 'Account field is required',
          
            ]
        );
   
        foreach ($this->site_id as $key => $value) {
            Account::update(['site_id' => $this->site_id[$key]]);
        }
  
        $this->inputs = [];
   
        $this->resetInputFields();
   
        session()->flash('message', 'Account Added Successfully.');
    }

    public function render()
    {
        $data = Generator::all();
        return view('livewire.generator',['data' => $data]);
    }
}
