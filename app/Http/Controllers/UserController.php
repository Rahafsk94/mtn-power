<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use DB;
use Hash;
use Carbon;
class UserController extends Controller
{


    public $id;

 function __construct()
    {
       
       
        $this->middleware(function ($request, $next) {
        $this->id = Auth::user()->id;


        return $next($request);
       });
       $this->middleware(['auth','role:Management'])->except('password');

    }

    public function index(Request $request)
    {
      
if(Auth::User()->hasRole('Management')){
        $data = User::where('is_active','=','1')->get();
        return view('users.index' , compact('data'));}


    
    else{
        return redirect() -> route('home') ;
    }
    }
 
    public function create()
    {
        
        if(Auth::User()->hasRole('Management')){
        $roles = Role::pluck('name','name') -> all();
        return view('users.create',compact('roles'));}
    
    else{
        return redirect() -> route('home') ;
    }
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
        $input = $request -> all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user -> assignRole($request -> input('roles'));
        return redirect() -> route('users.index') -> with('success','User created successfully');
    }


    public function show($id)
    {if(Auth::User()->hasRole('Management')){
        $user = User::find($id);
        return view('users.show',compact('user'));}
    }



    public function edit($id)
    {
        if(Auth::User()->hasRole('Management')){
            
        $user = User::find($id);
        
        $roles = Role::pluck('name','name') -> all();
        $userRole = $user->roles -> pluck('name','name') -> all();
       $t= $user->load('roles');
     $c=  $t->roles;

        return view('users.edit',compact('user','roles','userRole','c'));}
    }


    public function update(Request $request, $id)
    {
        $this -> validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,

            'roles' => 'required'
        ]);
        $input = $request->all();
        if( ! empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        }
        $user = User::find($id);
        $user -> update($input);
        DB::table('model_has_roles') -> where('model_id',$id) -> delete();
        $user -> assignRole($request -> input('roles'));
        return redirect() -> route('users.index') -> with('success','User updated successfully');
    }

   
    
    
    
     public function destroy($id)
    {if(Auth::User()->hasRole('Management')){
       /* if(Auth::User()->name!='Rahaf'){
     $user=   User::find($id) ;
     dd($user);
        return redirect() -> route('users.index')  -> with('success','User deleted successfully');}
        else{
        return redirect() -> route('users.index')  -> with('success','This user cannot be deleted');}*/

        $user=   User::find($id) ;
        if( $user->name == 'Rahaf'){
            return redirect() -> route('users.index')  -> with('danger','This user cannot be delete');
        }
        else{
            $user->is_active='0';
            $user->end_date= Carbon\Carbon::now();
         $user->save();
         return redirect()->back() -> with('success','User deleted');
        }
    }}

    public function password($user)
    {
      
        if($this->id==$user){
        
            $p=User::get();
            return view('users.password',compact('p','user'));}
            else{

                return redirect()->route('users.index');
            }
        
       
    }

    public function changePassword(Request $request,$user){
 
        if (!(Hash::check($request->get('current-password'), \Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = \Auth::user();
    
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
    
        return redirect()->back()->with("success","Password changed successfully !");
    
    }

}
