<?php

namespace App\Http\Controllers;

use App\Models\Poweraction;
use Illuminate\Http\Request;

class PoweractionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Poweraction  $poweraction
     * @return \Illuminate\Http\Response
     */
    public function show(Poweraction $poweraction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Poweraction  $poweraction
     * @return \Illuminate\Http\Response
     */
    public function edit(Poweraction $poweraction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Poweraction  $poweraction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poweraction $poweraction)
    {
        //
    }


    public function destroy(Poweraction $poweraction)
    {
        //
    }
}
