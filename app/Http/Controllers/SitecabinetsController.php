<?php

namespace App\Http\Controllers;

use App\Models\Sitecabinets;
use App\Models\Cabinet;
use App\Models\Site;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use DB;

class SitecabinetsController extends Controller
{
    
    public function index()
    {
     
    $sitecabinets=Sitecabinets::get(); 
        return view('sitecabinets.index',compact('sitecabinets'));
    }

    
    public function create()
    {
 
        return view('sitecabinets.create');
    }

    public function store(Request $request)
    {
       
        
        $request->validate( 
            [

                'used'=> 'required',
                'oracle_code'=> 'required|exists:cabinets,oracle_code',
                'site_code'=> 
                    'required|exists:sites,site_code',
               

                'cabinet_name' => 
                    'required|exists:cabinets,cabinet_name',
                  
                'numbers' =>'required|numeric',
        
                'installation_date' => 'required|date',
                
            ]
        ,
        
     
            [
                'cabinet_id.unique'=>'this cabinet has alrady exsit do you want to add new one '
                
               ]
        
        
        );

            $data = $request->all();
            $site = Site::query()->where('site_code',$request->get('site_code'))->first();
            $cabinet = Cabinet::query()->where('cabinet_name',$request->get('cabinet_name'))->first();
            $cabinetused = Cabinet::query()->where('used',$request->get('used'))->first();

            $siteid=$site->id;
            $cabinetid=$cabinet->id;

$sitecabinets=DB::table('sitecabinets')->where(function( $query) use ($siteid,$cabinetid,$request)
{
    $query->where('cabinet_id','=',$cabinetid)
    ->where('site_id','=',$siteid)
    ->where('oracle_code','=',$request->get('oracle_code'));
})->first();

$cabinetname= $cabinet->cabinet_name;
     
$thecabinet=Cabinet::query()->where('cabinet_name','=',$cabinetname)->get();



$usedcabinet=DB::table('cabinets')->where(function( $query) use ($cabinetname,$request)
{
    $query->where('cabinet_name','=', $cabinetname)->where('used','=', $request->get('used'))
    ->where('oracle_code','=', $request->get('oracle_code')
);
 

})->first();
if(! $usedcabinet){
    return redirect()->route('sitecabinets.create')->with('success','لا يوجد هذه الكبينة في المستودع');
}


$usedCabinetId= $usedcabinet->id ;
    
$theUsedCabinet= Cabinet::find($usedCabinetId);







      if($sitecabinets){

           
      
             $id=$sitecabinets->id ;
             $sitecabinetsid= Sitecabinets::find($id);
             $sitecabinetsid->numbers=$sitecabinetsid->numbers +  $request->get('numbers') ;
            
          if($theUsedCabinet->number  <   $request->get('numbers') ){
          
            return redirect()->route('sitecabinets.create')->with('success','There aren’t enough cabinets to use');
          }
          else{
          
          
          
            $sitecabinetsid->save() ;
 
          }
}

else{
    
            $sitecabinet = new sitecabinets($data);
            $sitecabinet->site()->associate( $site );
            $sitecabinet->cabinet()->associate( $cabinet );

          
             
            if($theUsedCabinet->number  <   $request->get('numbers') ){
            
                return redirect()->route('sitecabinets.create')->with('success','There aren’t enough cabinets to use');
              }
              else{
              
              
                $sitecabinet->save();
     
              }


    }
    
$theUsedCabinet->number = $theUsedCabinet->number - $request->get('numbers') ;
    $theUsedCabinet->save();

    return redirect()->route('sitecabinets.index')->with('success','a new cabinet  added successfully');
}

 
    public function show(Sitecabinets $sitecabinets)
    {
        //
    }

  
    public function edit(Sitecabinets $sitecabinets)
    {
    }

 
    public function update(Request $request, Sitecabinets $sitecabinets)
    {
        //
    }

    public function destroy(Sitecabinets $sitecabinets)
    {
       
        $sitecabinets->delete();

         return redirect()->route('cabinets.index')->with('success','site deleted successfully');
    }
    
    public function d(Sitecabinets $sitecabinets)
    {

     
        $sitecabinets->numbers= $sitecabinets->numbers -1;
        
     $cabinetid= $sitecabinets->cabinet_id;
  
    $cabinets=Cabinet::find($cabinetid);
   
   
    $xcabinets=DB::table('cabinets')->where(function( $query) use($cabinets)
    {
        $query->where('cabinet_name','=',$cabinets->cabinet_name)
        ->where('used','=','yes')
        ->where('oracle_code','=',$cabinets->oracle_code);
    })->first();

    
if($xcabinets){

    $id=$xcabinets->id ;
    $tt= Cabinet::find($id);
    $tt->number=$tt->number+1;
   $tt->save(); 
      
 }
 else 
     {

        $newCabinet=$cabinets->replicate();
        $newCabinet->number=1;
        $newCabinet->used='yes';
        $newCabinet->save();
    
   
     }
     if( $sitecabinets->numbers>0){
        $sitecabinets->save();
   
    
       
       }
        else{
          
            $sitecabinets->delete();
         
        }
        return redirect()->route('sitecabinets.index')->with('success','site deleted successfully');
}
    
}
