<?php

namespace App\Http\Controllers;
use App\Models\Site;
use App\Models\Generator;
use App\Models\Ampere;
use App\Models\Cabinet;
use App\Models\Solar;
use App\Models\Battery;
use App\Models\Tank;
use App\Models\User;
use App\Models\Permission;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Rules\Sitecodename;
use App\Imports\SitesImport;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Carbon\Carbon;

 use App\Exports\SiteExport;
 use App\Exports\viewExport;
 use DataTables;
 use App\DataTables\SitesDataTable;  
 use App\Exports\searchSiteExport;

 use App\Http\Traits\MyTrait;


class sitecontroller extends Controller
{
    

    function __construct(){
      // $this->middleware('auth');
       
   }

 /*  public function test($erornum,$msg){
    return response()->json([
        'status'=>false,
'error number'=>$erornum,
'message'=>$msg
    ]) ;
  }
  public function testsuccess($erornum,$msg){
    return response()->json([
        'status'=>true,
'success number'=>$erornum,
'message'=>$msg
    ]) ;
  }


  public function getdata($erornum,$msg,$data){
    return response()->json([
        'status'=>true,
'success number'=>$erornum,
'message'=>$msg,
'data'=>$data
    ]) ;
  }

*/

//use MyTrait;
    public function index(Request $request)
    {
 

        
  /*  $site =Site::find($request->id);
    return $this->hi();*/
       
   if ($request->ajax()) {
            $data = Site::select('*') ->where('is_active', '!=', '0');
        
            return Datatables::of($data)
    
                    ->addIndexColumn() 
                ->editColumn('installation_date', function ($site) {
    
                        return $site->installation_date->format('d/m/Y ');
                    })
                  
                    ->addColumn('action', function($row)
                    {
    
        

                           $btn ='<a href="sites/'.$row->id.'/edit" class="fa fa-edit  style="font-size: 19px, color: rgb(255, 0, 0) ,margin-right:9px;  "></a>';


                            return $btn;
                    }) 
                    
                    ->addColumn('delete', function($row)
                    {
    
                        return 
                       ' <button class="bi bi-trash"  style="color: red"  onclick="myFunction('.$row->id.')"' . $row->id . '  ></button>';
                   
        

                      
                          
                    })->rawColumns([ 'action','delete'])
                    
                    ->make(true);}
   
                    return view('sites.index');

/*$site=Site::find($request->id);
if(!$site){
    return $this->test('100','غير موجود');}
    else
    {
$site->update(['site_name'=>$request->site_name]);
        return $this->getdata('400',' موجود',$site);
    }*/
}

    


    public function create()
    {
        if(Auth::User()->can('create')){
        return view('sites.create');
        }
        else return redirect()->route('home');
    }

    public function store(Request $request)
    {

        
        $request->validate(
            [
                'site_code'=>['nullable','unique:sites', new Sitecodename],
                'on_off_air'=>'required|boolean',
                'finance_code'=>'required',
                'site_name'=>'required',
                'priority'=>'required|numeric',
                'area'=>'required',
                'location'=>'required',
                'sub_location'=>'required',
                'oracle_location'=>'required',
                'province'=>'required',
                'tx_category'=>'required',
                
               
                'band'=>'required',
                'zone'=>'required',
                'c_r'=>'required',
                'supplier'=>'required',
                'coordinates_e'=>'required',
                'coordinates_n'=>'required',
                'rationning_hours'=>'required'
            ]
        );

        $data = $request->all();

        $dt = new \DateTime();
        
        $now= $dt->format('Y-m-d H:i:s');
    
        $site = new Site($data);
        $site['installation_date']=$now;
     
       
       $site['site_code']=strtoupper($request->input('site_code'));
        $site->save();
        return redirect()->route('sites.index')->with('success','Site added successfully');
    }


   public function siteCodeName(Request $request)
    {
        $site_name=$request->site_name;
        $sName=['ALP','DMR','DAM','DRA','DRZ','HMA','HMS','HSK','IDB','LTK','SWD','TRS'];
       $x=strtoupper(substr( $site_name,0,3));
       if(!in_array( $x,$sName)){
        return response()->json(['a'=>true]);
       }
    }

    public function des($id)
    {

        if(Auth::User()->can('delete')){
        $site = Site::find($id);
        $generator=Generator::where('site_id','=',$id)->get();
        $battery=Battery::where('site_id','=',$id)->get();
        $ampere=Ampere::where('site_id','=',$id)->get();
        $solar=Solar::where('site_id','=',$id)->get();
        $cabinet=Cabinet::where('site_id','=',$id)->get();
        $tank=Tank::where('site_id','=',$id)->get();
        
foreach($generator as $gene){
    $gene->site_id=null;
    $gene->save();
}


foreach($battery as $batt){
    $batt->site_id=null;
    $batt->save();
}
foreach($ampere as $amp){
    $amp->site_id=null;
    $amp->save();
}
foreach($solar as $sol){
    $sol->site_id=null;
    $sol->save();
}
foreach($cabinet as $cab){
    $cab->site_id=null;
    $cab->save();
}
foreach($tank as $tan){
    $tan->site_id=null;
    $tan->save();
}
if($site){
$site->is_active='0';
$site->end_date='2023-09-10';
$site->generator='0';
$site->battery='0';
$site->ampere='0';
$site->solar='0';
$site->tank='0';
$site->cabinet='0';
$site->save();


}
         return response()->json($site);}
    }
    public function edit(Site $site)
    {
        if(\Auth::user()->can('edit')){
        return view('sites.edit',compact('site')); }
    }


   
    public function update(Request $request, Site $site)
    {
        $request->validate([
          'site_code' => ['required','unique:sites,site_code,'.$site->id,new siteCodeName() ],
            'finance_code'=>'required',
            'site_name'=>'required',
            'priority'=>'required|numeric',
            'area'=>'required',
            'location'=>'required',
            'sub_location'=>'required',
            'oracle_location'=>'required',
            'province'=>'required',
            'tx_category'=>'required',
            'on_off_air'=>'required',
            'rationning_hours'=>'required|numeric',
            'band'=>'required',
            'zone'=>'required',
            'c_r'=>'required',
            'supplier'=>'required',
            'coordinates_e'=>'required',
            'coordinates_n'=>'required',

        ]);
        $site->update($request->all());
       return redirect()->route('sites.index')->with('success','site updated successfully');
    }
    
  
   

    public function destroy(site $site)
    {


        $site->delete();

        return redirect()->route('sites.index')->with('success','site deleted successfully');
      

    }
   
   

  public function solution(){
    $solution=Site::select('site_code','generator','battery','ampere','solar','tank','cabinet')->where('is_active','=','1')->get();

    return view('sites.powersolution',compact('solution'));
}




    public function search(Request $request, site $site){
        $sitesearch = $request->input('sitesearch');
        $site = Site::query()
        ->where('site_code', 'LIKE', "{$sitesearch}")->get();


        return view('sites.search', compact('site'));

    }


    function import_excel(){
        if(Auth::User()->can('import')){
        return view('sites.import');}
        else{
            return redirect()->route('home');
        }
    }
    function import(Request $request){
        if($request->file('file')){
            $import =  Excel::import(new SitesImport, request()->file('file'));
            $msg_success = "Data Uploaded Succesfully! ";
            $msg_danger = "Data Uploaded failed! ";
            if ($import) {
                return redirect('/siteimport')->with('success', strtoupper($msg_success));
            }else{
               return redirect('/siteimport')->with('danger', strtoupper($msg_danger));
            }
        }
        else{
            $msge = "please choose your file! ";
            return redirect('/siteimport')->with('choose_file', strtoupper($msge));
        }
    }

    public function export()
    {
        if(Auth::User()->hasRole('Management')){
        return Excel::download(new SiteExport, 'sites.xlsx');
    }}
   
  
    public function exportt(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('post'))
        {
            $sitesearch = $request->input('sitesearch');


            if ($request->has('search'))
            {
                $site = Site::query()
                ->where('site_code', 'LIKE', "{$sitesearch}")->get();
        
        
                return view('sites.search', compact('site'));
            } 

        if($request->has('exportExcel'))
        $sitesearch = $request->input('sitesearch');
        return Excel::download(new searchSiteExport($sitesearch ), 'sites.xlsx');
    }}
    public function exporttt(Request $request)
    
    {

        $sitesearch = $request->input('sitesearch');

        return Excel::download(new viewExport('sitesearch'), 'sites.xlsx');
       }
     
}
  
    
