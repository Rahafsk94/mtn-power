<?php

namespace App\Http\Controllers;

use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class WarehouseController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }
    public function index()
    {

        $warehouses=Warehouse::get();
        return view('warehouses.index',compact('warehouses'));
    }




    public function solution(){
        $warehouses=Warehouse::select('name','generator','battery','solar','cabinet')->where('is_active','=','1')->get();
    
        return view('warehouses.powersolution',compact('warehouses'));
    }


    public function create()
    {
        if(Auth::User()->can('create')){
        return view('warehouses.create');}
        
        else{ return redirect()->back();}
    }

    
    public function store(Request $request)
    {
        $request->validate([
         'name'=>'required|unique:warehouses',
         'owner'=>'required',
         'location'=>'required',
        ]);
        $warehouse= new Warehouse($request->all());
       
        $warehouse->save();
        return redirect()->route('warehouses.index')->with('success','Warehouse added successfully');
    }


    public function show(Warehouse $warehouse)
    {
        //
    }


    public function edit(Warehouse $warehouse)
    {
        if(Auth::User()->can('edit')){
        return view('warehouses.edit',compact('warehouse'));}
else{
    return view('warehouses.index');
}
    }

   
    public function update(Request $request, Warehouse $warehouse)
    {
        if(Auth::User()->can('edit')){

            $request->validate([
                "name"=>'required|unique:warehouses,name,'.$warehouse->id,
                'location'=>'required',
                'owner'=>'required'
            ]);

            $warehouse->update($request->all());
            return redirect()->route('warehouses.index')->with('success','updated successfully');
        }
    }

  
    public function destroy(Warehouse $warehouse)
    {
        //
    }
    
    public function cheackWarehouse(Request $request)
    {

        
 $warehouse_name=Warehouse::where('name','=',$request->get('warehouse_name'))->where('is_active','=','1')->first();
 if($warehouse_name){
 return response()->json(['a'=>$warehouse_name]);
    }
else{
    return response()->json(['b'=>true]);
}


}
}
