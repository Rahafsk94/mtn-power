<?php

namespace App\Http\Controllers;

use App\Models\Cabinet;
use App\Models\Site;
use App\Models\Warehouse;
use App\Models\Sitecabinets;
use App\Models\Generatorpoweraction;
use App\Models\Poweraction;
use App\Models\Battery;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Imports\CabinetImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use DB;
class CabinetController extends Controller
{
     function __construct(){
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $cabinets=Cabinet::where('is_active','=','1')->whereNotNull('site_id')->get();
        return view('cabinets.index',compact('cabinets'));

    }

   
    public function create()
    {
        if(Auth::User()->can('create')){
        
       return view('cabinets.create');}
       else{
        return redirect()->route('cabinets.index');
       }
    }


    public function actions()
    
    {
        if(Auth::User()->can('admin')){
        $warehouses=Warehouse::get();
        $cabinets=Cabinet::whereNotnull('site_id')->where('is_active','=','1')->get();
        return view('cabinets.actions',compact('cabinets','warehouses'));}
        else{
            return redirect()->route('cabinets.index');
        }
    }



  
    public function store(Request $request )
    {
        $request->validate(
            [
                'site_code'=>'required|exists:sites,site_code',
                'cabinet_name'=>'required',
      'pr'=>'required|numeric',
                'is_used'=>'required',
                'oracle_code'=>'required',
                'installation_date'=>'required|date',

              
            ]
        );

        $cabinet= new Cabinet($request->all());

        $site=Site::query()->where('site_code','=',$request->get('site_code'))->first();
        $cabinet->site()->associate( $site );
        $cabinet->number='1';
        $cabinet->cabinet_name =strtoupper($request->get('cabinet_name'));
        $cabinet->save();

        $site->cabinet=  $site->cabinet()->where('is_active','=','1')->count();
        $site->save();
        $generatorpoweraction =new Generatorpoweraction();
        $x="installation";
        $generatorpoweraction['user_id'] =  \Auth::user()->id;
        $generatorpoweraction['user_name'] =  \Auth::user()->name;
        $generatorpoweraction['cabinet_id']=$cabinet->id;
        $generatorpoweraction['pr']= $request->get('pr');
        $poweraction = Poweraction::query()->where('action_name',$x)->first();
        $generatorpoweraction->poweraction()->associate(  $poweraction );
        
        
        
        
        
        $generatorpoweraction->save() ;
        
        return redirect()->route('cabinets.index')->with('success','Cabinet added successfully');
    }



    public function warehouse()
    {
   
     $cabinets=Cabinet::whereNotnull('warehouse_id')->where('is_active','=','1')->get();

     return view('cabinets.storage',compact('cabinets'));


  
    }
  

    public function craeteShipment()
    {
        if(Auth::User()->can('create')){
        return view('cabinets.createwarehouse');}
        else{
            return redirect()->route('cabinets.index');
           }

    }

    public function addShipment( Request $request)
    {
        $request->validate([

         'warehouse_name'=>'required||exists:warehouses,name',
         'number'=>'required|numeric',
         'cabinet_name'=>'required',
         'oracle_code'=>'required',
         'shipment_date'=>'required|date', 

                          ]);
                          $warehouse=Warehouse::query()->where('name','=',$request->get('warehouse_name'))->first();

        $cabinet= new Cabinet($request->all());
        $cabinet->warehouse()->associate($warehouse);
     
        $warehouse->cabinet=$warehouse->cabinet  + $request->number;

        $cabinet->save();
        $warehouse->save();
         return  redirect()->route('cabinetwarehouse')->with('no','a new cabinet added successfully');
    }
    public function show(Cabinet $cabinet)
    {
        return view('cabinets.show',compact('cabinet'));
    }

   
    public function edit(Cabinet $cabinet)
    
    {
        if(Auth::User()->can('edit')){
        return view('cabinets.edit',compact('cabinet'));}
        else{
            return redirect()->route('cabinets.index');
           }
    }

  

    public function update(Request $request, Cabinet $cabinet)
    {
        $request->validate([
           'oracle_code'=>'required',
           'cabinet_name'=>'required',
      

        ]);

        $cabinet->update(['oracle_code'=>$request->get('oracle_code'),
        'cabinet_name'=>strtoupper($request->get('cabinet_name')),
    ]);

      
        return redirect()->route('cabinets.index')->with('success', ' The cabinet updated successfully');
    }

    public function destroy(Cabinet $cabinet)
    {
        if(Auth::User()->hasRole('Management')){
        $battery=Battery::query()->where('cabinet_id','=',$cabinet->id)->get();
        foreach ($battery as $batt){
       $batt->cabinet_id=null ;
       $batt->save();
            }
            $site=Site::where('id','=',$cabinet->site_id)->first();
           
        $cabinet->is_active='0';
        $cabinet->site_id=null;
        $cabinet->warehouse_id=null;
        $cabinet->is_used='used';
    
        $cabinet->save();
        $site->cabinet=$site->cabinet()->where('is_active','=','1')->count() ;
        $site->save();
        return redirect()->route('cabinets.index')->with('success','Cabinet deleted');}
    }





    


    function import_excel(){
        if(Auth::User()->can('import')){
        return view('cabinets.import');}
        else{
            return redirect()->route('home');
        }
    }
    function import(Request $request){
        $validatedData = $request->validate([

            'file' => 'required',
  
         ]);
  
         Excel::import(new CabinetImport,$request->file('file'));
  
             
         return redirect('cabinet_import')->with('status', 'The file has been  imported to database ');
     
    }




    public function editreshuffling(Cabinet $cabinet)
    {
        return response()->json($cabinet);


    }



         public function editnumber(Cabinet $cabinet)
         {
             return response()->json($cabinet);
    

         }
         public function updatenumber(Request $request)
         {
     
     
             $cabinet =  Cabinet::updateOrCreate(
                 ['id'=>$request->id],
               
             );
             $cabinet->number =  $cabinet->number - request('number');
      
             if( $cabinet->number == 0  ){

                
                $cabinet->number= $request->number;
                $site=Site::query()->where('site_code','=',$request->get('site_code'))->first();
                $warehouse=Warehouse::query()->where('id','=',$cabinet->warehouse_id)->first();
                $cabinet->site()->associate(  $site );
                
                $site->cabinet = $site->cabinet + $request->number;
                $warehouse->cabinet=$warehouse->cabinet  - request('number');
                $cabinet->installation_date=$request->get('installation_date');
                $cabinet->warehouse_id=null;
        
                $cabinet->save();
                $site->save();
                $warehouse->save();
              }
else if ($cabinet->number > 0){

    $cabinets=  $cabinet->replicate();
    $cabinets['number']=$request->get('number');
    $warehouse=Warehouse::query()->where('id','=',$cabinet->warehouse_id)->first();
    $warehouse->cabinet=$warehouse->cabinet  - request('number');
    $cabinets['warehouse_id']=null;
    $site=Site::query()->where('site_code','=',$request->get('site_code'))->first();
    $cabinets->site()->associate($site);
    $site->cabinet = $site->cabinet + $request->number;
    $cabinets['installation_date']=$request->get('installation_date');
    $cabinets->save();
    $site->save();
    $cabinet->save();
    $warehouse->save();
    $generatorpoweraction =new Generatorpoweraction();
$x="installation";
$generatorpoweraction['user_id'] =  \Auth::user()->id;
$generatorpoweraction['user_name'] =  \Auth::user()->name;
$generatorpoweraction['cabinet_id']=$request->get('id');;
$generatorpoweraction['pr']= $request->get('pr');
$poweraction = Poweraction::query()->where('action_name',$x)->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );





$generatorpoweraction->save() ;
}

         

             

             return  response()->json($cabinet);

     
         }


         
    public function updatereshuffling(Request $request)
    {
        if(Auth::User()->can('admin')){
     
        $request->validate([

              'action_name'=>'required',
              'site_code'=>'required',
              'id'=>'required',
              'trx'=>'required',

                     ]);

       $cabinet= Cabinet::updateOrCreate(
           ['id'=>$request->id],

       );

       $generatorpoweraction= new Generatorpoweraction($request->all());
       $generatorpoweraction['user_id'] =  \Auth::user()->id;
       $generatorpoweraction['cabinet_id']=$request->id;
       $generatorpoweraction['user_name'] =  \Auth::user()->name;


      $poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
      $generatorpoweraction->poweraction()->associate(  $poweraction );

$oldSiteId=Site::find($cabinet->site_id);
$oldSiteCode=$oldSiteId->site_code;
$generatorpoweraction['old_site']=$oldSiteCode;
if(strtoupper($request->site_code) == strtoupper($oldSiteCode))
{

return  response()->json($cabinet);
}
else{
    
$generatorpoweraction['new_site']=$request->site_code;
$oldSiteId->cabinet =$oldSiteId->cabinet -1;
$site=Site::query()->where('site_code',$request->site_code)->first();
$site->cabinet=$site->cabinet +   1;
$cabinet->site()->associate( $site );
$cabinet->is_used='Used';
$cabinet->installation_date=$request->installation_date;
$battery=Battery::where('cabinet_id','=',$request->id)->get();
foreach($battery as $batt){
    $batt->cabinet_id=null;
    $batt->save();
}

$oldSiteId->save();
$cabinet->save();


$site->save();
$generatorpoweraction->save();

       return  response()->json( $cabinet);}
    }
    }
    public function editdismantle(Cabinet $cabinet)
    {
        return response()->json($cabinet);


    }




    public function updatedismantle(Request $request)
    {

       $request->validate([
               'rm'=>'required',
               'warehouse_name'=>'required|exists:warehouses,name',
               'action_name'=>'required',
                         ]);

       $cabinet =  Cabinet::updateOrCreate(
           ['id'=>$request->id],

       );
      
       $generatorpoweraction= new Generatorpoweraction($request->all());
       $generatorpoweraction['user_id'] =  \Auth::user()->id;
       $generatorpoweraction['user_name'] =  \Auth::user()->name;
       $generatorpoweraction['cabinet_id'] =$request->id;
       $poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
       $generatorpoweraction->poweraction()->associate(  $poweraction );
       $x= $request->id;
      

       $oldSiteId=Site::find($cabinet->site_id);
     
       $oldSiteId->cabinet=$oldSiteId->cabinet- 1;
       $oldSiteCode=$oldSiteId->site_code;
       $generatorpoweraction['old_site']=$oldSiteCode;
       $cabinet['site_id']   =null;
      
       $cabinet['is_used'] ='Used';
       $cabinet['dismantle_date']=$request->get('dismantle_date');
       $cabinet['shipment_date']=null;
       $battery=Battery::where('cabinet_id','=',$request->id)->get();
       foreach($battery as $batt){
           $batt->cabinet_id=null;
           $batt->save();
       }
       $site = Site::query()->where('site_code',$request->get('warehouse_name'))->first();
       $warehouse = Warehouse::query()->where('name',$request->get('warehouse_name'))->first();
       $warehouse->cabinet = $warehouse->cabinet + 1;
      $cabinet->warehouse()->associate(  $warehouse )  ;
      
               $generatorpoweraction->save();
         
               $cabinet->save();
              
               $warehouse->save();
               $oldSiteId->save();
       return  response()->json( $cabinet);
   }




    }


