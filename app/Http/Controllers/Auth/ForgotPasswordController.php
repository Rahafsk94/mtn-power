<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Mail; 
use Hash;
use Illuminate\Support\Str;
use DB; 
use Carbon\Carbon; 
use App\Models\User;
use App\Notifications\ForgetPasswordNotification;
use Illuminate\Support\Facades\Notification;

class ForgotPasswordController extends Controller
{
    

    use SendsPasswordResetEmails;
    

      public function storeToken()
    {
  /*   $testNotification = User::first();
   
     $testNotification->notify(new EmailNotification (900));
    dd($testNotification->notifications);*/
 

    return redirect()->back()->with('status','Your deposit was successful!');
    }



    public  function showForgetPasswordForm(){
        return view('auth.passwords.email');
    }


 
    public function submitForgetPasswordForm(Request $request)
    {

        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        $token = Str::random(64);

        DB::table('password_resets')->insert([
            'email' => $request->email, 
            'token' =>$token, 
            'created_at' => Carbon::now()
          ]);
/*$mail=$request->email;
      Mail::send('email.forgetPassword', ['token' => $token, 'mail'=>$mail ], function($message) use($request){
            $message->to($request->email);
            $message->subject('Reset Password');
        });*/
       $user=User::find('1');
       
     $user->notify(new ForgetPasswordNotification($token));
       

      return back()->with('message', 'We have e-mailed your password reset link!');
    }

    public function showResetPasswordForm($token) { 
       return view('auth.passwords.reset', ['token' => $token]);
    }

   
    public function submitResetPasswordForm(Request $request)
    {

        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        $updatePassword = DB::table('password_resets')
                            ->where([
                              'email' => $request->email, 
                              'token' => $request->token
                            ])
                            ->first();

        if(!$updatePassword){
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $user = User::where('email', $request->email)
                    ->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email'=> $request->email])->delete();

    return redirect('/login')->with('success', 'Your password has been changed!');
    }


    public function markAsRead(){
      \Auth::user()->unreadNotifications->markAsRead();
      return redirect()->back();
  }





}












