<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
   

    use AuthenticatesUsers;

    
    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
/*login using name*/
    public function username()
    {
      $userName=  request()->input('username');
   $field=   filter_var($userName,FILTER_VALIDATE_EMAIL)?'email':'name';
    request()->merge([$field=>$userName]);
        return $field;
    }

public function login(Request $request){

   
$this->validateLogin($request);
if(Auth::attempt(array($this->username() => request()->input('username'), 'password' => $request->password))){
$u=User::Where($this->username(),'=',request()->input('username'))->first();


if ($u->is_active=='1'){
    $request->session()->regenerate();

   // Auth::loginUsingId($user->id, TRUE);
return $this->sendLoginResponse($request);
}
else if($u->is_active=='0'){
    
    Auth::logout($u);
    return redirect()->route('login')->with('success','The user has expired');
}}
else{
    $this->validateLogin($request);
/*Auth::loginUsingId($user->id, TRUE);
$request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
*/

return $this->sendFailedLoginResponse($request);

}


    }

    

}