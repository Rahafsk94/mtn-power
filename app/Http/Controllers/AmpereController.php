<?php

namespace App\Http\Controllers;

use App\Models\Ampere;
use Illuminate\Http\Request;
use App\Models\Site;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\AmpereImport;
use App\Exports\AmpereExport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class AmpereController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        $ampere = Ampere::where('is_active','=','1')->get();

        return view('ampere.index',compact('ampere'));
    }

   
    public function create()
    {
        if(Auth::User()->can('create')){
     
       
       return view('ampere.create');}
       else return redirect()->route('amperes.index');
    }
    public function cheackSite(Request $request ){


      if($request->get('site_code')==null){
        return response()->json(['b' =>true,
    'a'=>true
    ]);
      }
      else{
        $site_code= Site::where('site_code','=',$request->get('site_code'))->where('is_active','=','1')->first();
        return response()->json(['a' =>$site_code ]);
      }

         
        
    }

    public function store(Request $request)
    {
        $request->validate([
        'site_code'=>'required|exists:sites,site_code',
        'ampere_provider'=>'required',
        'ampere_capacity'=>'required|numeric',
        'payment_method'=>'required',
        'payment_type'=>'required',
        'agreed_rh'=>'required|numeric',
        'installation_date'=>'required|date',
    
        ]);
        $ampere=new Ampere( $request->all());

        $site = Site::query()->where('site_code',$request->get('site_code'))->first();
        $ampere->site()->associate( $site );

  $ampere->save();

       return redirect()->route('amperes.index')->with('success','Ampere added successfully');
    }

 

    public function edit(ampere $ampere)
    {
        if(Auth::User()->can('edit')){
        return view('ampere.edit',compact('ampere'));}
        else{
            return redirect()->route('amperes.index');
        }
    }

  
    public function update(Request $request, ampere $ampere)
    {

        $request->validate([
            'ampere_provider'=>'required',
            'payment_method'=>'required',
            'payment_type'=>'required',
            'agreed_rh'=>'required|numeric',
            'ampere_capacity'=>'required|numeric',
            'nots',

    ]);
        $ampere->update($request->all());
        return  redirect()->route('amperes.index')->with('o','ampere updated successfully');
    }

    public function destroy(ampere $ampere)
    {
        if(Auth::User()->can('delete')){
        $ampere['is_active']='0';
    
        $ampere['end_date']= Carbon::now();
       
        Ampere::notActive($ampere);
        $ampere['site_id']=null;
        $ampere->save();
        return redirect()->route('amperes.index')->with('success','ampere deleted successfully');}
        else{
            return redirect()->route('amperes.index');
        }
    }
    
   function import_ex(){

    if(Auth::User()->can('import')){
    return view('ampere.import');
          }
          else{

            return redirect()->route('home');
         }
        }

    function importExcelCSV(Request $request){
        $validatedData = $request->validate([

            'file' => 'required',
  
         ]);
  
         Excel::import(new AmpereImport,$request->file('file'));
  
             
         return redirect('ampereimport')->with('status', 'The file has been excel/csv imported to database in laravel 9');
     
    }
   
    
   
    


}
