<?php

namespace App\Http\Controllers;

use App\Models\Generatortank;
use Illuminate\Http\Request;

class GeneratortankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

   
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    public function show(Generatortank $generatortank)
    {
        //
    }

 
    public function edit(Generatortank $generatortank)
    {
        //
    }

    public function update(Request $request, Generatortank $generatortank)
    {
        //
    }

    
    public function destroy(Generatortank $generatortank)
    {
        //
    }
}
