<?php

namespace App\Http\Controllers;

use App\Models\Solar;

use App\Models\Site;
use App\Models\Battery;
use App\Models\Warehouse;
use App\Models\Poweraction;
use Illuminate\Http\Request;
use App\Models\Generatorpoweraction;
use App\Imports\SolarImport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Carbon;
class SolarController extends Controller
{
    
    public function index()
    {
        $solar=Solar::whereNotnull('site_id')->where('is_active','=','1')->get();
      
        return view('solars.index',compact('solar'));
    }



    public function addaction()
    {
        if (Auth::User()->can('admin')){
        $warehouses=Warehouse::get();
        $p=Poweraction::get();

      
       $solar=Solar::whereNotnull('site_id')->where('is_active','=','1')->get();

       return view('solars.actions',compact('solar','p','warehouses'));}
       else{

        return redirect()->route('solars.index');
       }

     
  
    }



public function warehouse()
    {
    
     
        $p=Poweraction::get();
$solars=Solar::whereNotnull('warehouse_id')->where('is_active','=','1')->get();

return view('solars.storage',compact('solars','p'));

     }


    public function editreshuffling(Solar $solar)
    {
        return response()->json($solar);


    }


    public function updatereshuffling(Request $request)
    {

     
        $request->validate([

            'action_name'=>'required', 
            'site_code'=>'required',
            'id'=>'required',
            'trx'=>'required',
               
                    ]);

       $solar = Solar::updateOrCreate(
           ['id'=>$request->id],

       );
 
       $generatorpoweraction= new Generatorpoweraction($request->all());
       $generatorpoweraction['user_id'] =  \Auth::user()->id;
       $generatorpoweraction['solar_id']=$request->id;
       $generatorpoweraction['user_name'] =  \Auth::user()->name;


$poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
$generatorpoweraction->poweraction()->associate($poweraction);
$id= $solar->site_id;



$oldSiteId=Site::find($id);
$oldSiteCode=$oldSiteId->site_code;

$generatorpoweraction['old_site']=$oldSiteCode;

if(strtoupper($request->site_code) == strtoupper($oldSiteCode)){
    
    return  response()->json( $solar);
} 
else{
    $generatorpoweraction['new_site']=$request->site_code;
    $oldSiteId->solar =$oldSiteId->solar - $solar->number_of_panel;
    $site=Site::query()->where('site_code',$request->site_code)->first();
    $site->solar=$site->solar  + $solar->number_of_panel;
    $solar->site()->associate( $site );
    $solar['status']='used';
   
    $solar['installation_date']=$request->installation_date;
    $battery=Battery::where('solar','=',$request->id)->get();
foreach ($battery as $batterysolar){

    $batterysolar->solar=null;
    $batterysolar->save();
        }
    $oldSiteId->save();
    $solar->save();
    

$site->save();
$generatorpoweraction->save();

           return  response()->json( $solar);
           }}


    
    
   
           public function editdismantle(Solar $solar)
           {
               return response()->json($solar);
  
  
           }




           public function updatedismantle(Request $request)
           {
  
              $request->validate([
  
      
                      'rm'=>'required',
                      'warehouse_name'=>'required|exists:warehouses,name',
                      'action_name'=>'required',
                  ]);
  
              $solar =  Solar::updateOrCreate(
                  ['id'=>$request->id],
  
              );
              $generatorpoweraction= new Generatorpoweraction($request->all());
              $generatorpoweraction['user_id'] =  \Auth::user()->id;
              $generatorpoweraction['user_name'] =  \Auth::user()->name;
              $generatorpoweraction['solar_id'] =$request->id;
              $poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
              $generatorpoweraction->poweraction()->associate(  $poweraction );
  
         
              $solar->number_of_panel= $solar->number_of_panel -  $request->get('number');
              $id=$solar->site_id;
              $oldSiteId=Site::find($id);
              $oldSiteCode=$oldSiteId->site_code;
              $warehouse = Warehouse::query()->where('name',$request->get('warehouse_name'))->first();

              if( $solar->number_of_panel == 0 ){
                $solar->number_of_panel = $request->number;
                $solar->warehouse()->associate($warehouse);
                $solar->status='used';
                $solar->dismantle_date= $request->get('dismantle_date');
            $solar->shipment_date=null;
                $solar->site_id=null;
                $battery=Battery::where('solar','=',$request->id)->get();
foreach ($battery as $batterysolar){

    $batterysolar->solar=null;
    $batterysolar->save();
        }
                $solar->save();
              }
else{
    $newSolar = $solar->replicate();
    $newSolar->number_of_panel = $request->number;
    $newSolar->warehouse()->associate(  $warehouse );
    $newSolar->site_id=null;
    $newSolar->status='used';
    $newSolar->dismantle_date= $request->get('dismantle_date');
    $newSolar->shipment_date=null;
 $newSolar->save();

}
$warehouse->solar= $warehouse->solar +$request->number;
$oldSiteId->solar= $oldSiteId->solar  -$request->number;

$oldSiteId->save();
$warehouse->save();

$generatorpoweraction['old_site']=$oldSiteCode;
$solar->save();
              $generatorpoweraction->save();

              return  response()->json( $solar);
          }


    public function install()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','installation')->first();
        $id=  $poweraction->id;
       
      $installations=Generatorpoweraction::where('action_id','=',$id)->WhereNotNull('solar_id')->get();
 
        return view('solars.installation',compact('installations'));
    }
    public function create()
    {
        if(Auth::User()->can('create')){
        return view('solars.create');}
        else{
            return redirect()->route('solars.index');
        }
    }
    public function craeteShipment()
    {        if(Auth::User()->can('create')){

        return view('solars.createwarehouse');}
        else{
            return redirect()->route('solars.index');
        }

    }


    public function store(Request $request)
    {
     $request->validate([
        'site_code'=>'required|exists:sites,site_code',
        'number_of_panel'=>'required|numeric',
        'controller'=>'required',
        'po'=>'required',
        'pr'=>'required',
        'panel_brand'=>'required',
        'status'=>'required',
        'panel_capacity'=>'required|numeric',
        'number_of_charger'=>'required|numeric',
        'solar_project'=>'required|numeric',
        'installation_date'=>'required|date'
    
    ]);
    $site=Site::query()->where('site_code',$request->get('site_code'))->first();
    $site->solar= $site->solar + $request->get('number_of_panel');
    $solar=new Solar($request->all());
    $solar->site()->associate( $site );
    $solar->save();
    $site->save();
    $generatorpoweraction= new Generatorpoweraction();	
    $generatorpoweraction['solar_id']=$solar->id ;
    $generatorpoweraction['pr']=$request->get('pr');
    $poweraction = Poweraction::query()->where('action_name','installation')->first();
    $generatorpoweraction['action_id']=$poweraction->id;
    $generatorpoweraction['user_name']=\Auth::user()->name;
    $generatorpoweraction->save();
    return redirect()->route('solars.index')->with('success','Solar added successfully');
    }

   

    public function addShipment(Request $request)
    {
     $request->validate([
      
        'po'=>'required',
        'controller'=>'required',
      
        'solar_project'=>'required',
        'panel_brand'=>'required',
        'panel_capacity'=>'required|numeric',
        'number_of_charger'=>'required|numeric',
        'number_of_panel'=>'required|numeric',
        'warehouse_name'=>'required|exists:warehouses,name',
        'shipment_date'=>'required|date'
    
    ]);
    $warehouse=Warehouse::query()->where('name',$request->get('warehouse_name'))->first();
    $warehouse->solar =  $warehouse->solar + $request->get('number_of_panel') ;
    $solar=new Solar($request->all());
    $solar['status']='new';
    $solar->warehouse()->associate( $warehouse);
    $solar->save();
    $warehouse->save();
    return redirect()->route('solarswarehouse')->with('success','Solar added successfully');
    }


    public function show(Solar $solar)
    {
        //
    }


    public function edit(Solar $solar)
    {


        if(Auth::User()->can('edit')){

            if($solar['is_active']=='1'){
return view('solars.edit',compact('solar'));}
else{
    return redirect()->route('solars.index');
}
}
else{
    return redirect()->route('solars.index');
}
    }

  
    public function update(Request $request, Solar $solar)
    {
        $request->validate([

            'panel_brand'=>'required',
            'panel_capacity'=>'required',
            'controller'=>'required',
            'solar_project'=>'required',
           'number_of_charger'=>'required',
            'po'=>'required'
         
        ]);
        $solar->update($request->all());
        return redirect()->route('solars.index')->with('success','solar updated successfully');
    }

    public function destroy(Solar $solar)
    {

        if(Auth::User()->can('delete')){
        $solar['is_active']='0' ;
  

   
   $site_id=$solar['site_id'];
 
   $solar['site_id']=null;
   $solar['status']='used';
   $site=Site::where('id','=',$site_id)->first();

   $site->solar=$site->solar - $solar['number_of_panel'];
   $solar->battery();
  $r= $solar->battery();





 $b=Battery::where('solar','=',$solar['id'])->get();
   foreach ($b as $u){

$u->solar=null;
$u->save();
    }

   $solar->save();
   $site->save();


     return redirect()->route('solars.index')->with('success','Solar deleted successfully');
    }
else{
    return redirect()->route('solars.index')->with('success',"You don't have permission to delete this solar");
}

}
    

    public function editnumber(Solar $solar)
         {
            if (Auth::User()->can('admin'))
{
             return response()->json($solar);
}

         }

         public function updatenumber(Request $request)
         {
            $request->validate([

    
                'site_code'=>'exists:sites,site_code|required',
                    'pr'=>'required',]);

            $solar =  Solar::updateOrCreate(
                ['id'=>$request->id],

            );
            $warehouse= Warehouse::query()->where('id',$solar->warehouse_id)->first();

            $site = Site::query()->where('site_code',$request->get('site_code'))->first();
            $site->solar = $site->solar + $request->number;
            $solar->number_of_panel = $solar->number_of_panel - $request->number;
 if( $solar->number_of_panel == 0 ){
    $solar->number_of_panel= $request->number;
    $solar->site()->associate(  $site );
    $solar->installation_date=$request->get('installation_date');
    $solar->warehouse_id=null;
         
    
          }

else   if( $solar->number_of_panel > 0 ){
    $solars=  $solar->replicate();
    $solars->warehouse_id=null;
    $solars->number_of_panel= $request->number;
    $solars->installation_date=$request->get('installation_date');
    $solars->site()->associate(  $site );
    $solars->save();
}


$warehouse->solar= $warehouse->solar -$request->number;

$generatorpoweraction =new Generatorpoweraction();
$x="installation";
$generatorpoweraction['user_id'] =  \Auth::user()->id;
$generatorpoweraction['user_name'] =  \Auth::user()->name;
$generatorpoweraction['solar_id']=$request->get('id');;
$generatorpoweraction['pr']= $request->get('pr');
$poweraction = Poweraction::query()->where('action_name',$x)->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );





$generatorpoweraction->save() ;
$warehouse->save();
            $solar->save();
            $site->save();

            return  response()->json( $solar);


         }




         function import_excel(){
            if(Auth::User()->can('import')){
            return view('solars.import');}
            else{
                return redirect()->route('home');
            }
        }
        function import(Request $request){
            $validatedData = $request->validate([

                'file' => 'required',
      
             ]);
      
             Excel::import(new SolarImport,$request->file('file'));
      
                 
             return redirect('solarimport')->with('u', 'The file has been  imported to database ');
         
        }



}
