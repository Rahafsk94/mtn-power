<?php

namespace App\Http\Controllers;

use App\Models\Battery;
use App\Models\Site;
use App\Models\Solar;
use App\Models\Cabinet;
use App\Models\Generatorpoweraction;
use App\Models\Poweraction;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Facades\Excel;
use App\Imports\BatteryImport;
use Carbon;
class BatteryController extends Controller
{


public function __construct(){
$this->middleware('auth');

}
    
    public function index()
    {

        $battery=Battery::whereNotnull('site_id')->where('is_active','=','1')->get();

        return view('batteries.index',compact('battery'));

    }



    public function actions()
    {
        $warehouses=Warehouse::get();
        $batteries=Battery::whereNotnull('site_id')->where('is_active','=','1')->get();
        return view('batteries.actions',compact('batteries','warehouses'));
    }

    public function create()
    {
        if(Auth::User()->can('create')){
        return view('batteries.create');}
        else{
            return redirect()->route('batteries.index');
        }
    }

    public function warehose()
    {

        $batteries=Battery::whereNotnull('warehouse_id')->where('is_active','=','1')->get();
 
 
        return view('batteries.storage',compact('batteries'));
    }
   
   


    public function install()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','installation')->first();
        $id=  $poweraction->id;
       
        $installations=Generatorpoweraction::where('action_id','=',$id)->WhereNotNull('battery_id')->get();
 
        return view('batteries.installation',compact('installations'));
    }


   
    public function store(Request $request)
    {

        $request->validate([

            'site_code'=>'exists:sites,site_code|nullable',
            'cabinet_id'=>'exists:cabinets,id|nullable',
            'solar'=>'exists:solars,id|nullable',
            'battery_brand'=>'required',
            'capacity'=>'required',
            'numbers'=>'required',
            'status'=>'required',
            'oracle_code'=>'required',
            'installation_date'=>'date',
            'po'=>'required',
        

        ],
        [
            'battery_brand.required' => 'The battery is required!',
          
        ]
    
    );


        $battery= new Battery($request->all());

        $site = Site::query()->where('site_code',$request->get('site_code'))->first();
     $user=   Auth::User()->name;
  
        $battery->site()->associate( $site );
        $site->battery = $site->battery +$request->get('numbers');
        $site->save();
        $battery->save();
      /* $generatorpoweraction =new Generatorpoweraction();
       
        $generatorpoweraction['user_id'] =  \Auth::user()->id;
        $generatorpoweraction['user_name'] =  \Auth::user()->name;
        $generatorpoweraction['battery_id']= $battery->id;
        $generatorpoweraction['pr']= $request->get('pr');
       
        $generatorpoweraction->poweraction()->associate(  $poweraction );
        
        $generatorpoweraction->save() ;*/
        $x="installation";
        $poweraction = Poweraction::query()->where('action_name',$x)->first();
        $generatorpoweraction = Generatorpoweraction::create([
            'battery_id'=>  $battery->id,
            'user_name'=>\Auth::user()->name,
            'user_id'=>\Auth::user()->id,
            'pr'=>$request->get('pr'),
            'action_id'=>$poweraction->id
        ]);
        
        
        return redirect()->route('batteries.index')->with('success',  'Battery added successfully');


    }
  

    public function craeteShipment()
    {
        if(Auth::User()->can('create')){
        return view('batteries.createwarehouse');}
        else{
            return redirect()->route('batteries.index');
        }

    }



    public function editdismantle(Battery $battery)
    {
        return response()->json($battery);


    }




    public function updatedismantle(Request $request)
    {

       $request->validate([
               'rm'=>'required',
               'warehouse_name'=>'required|exists:warehouses,name',
               'action_name'=>'required',
                         ]);

       $battery =  Battery::updateOrCreate(
           ['id'=>$request->id],

       );
      
       $generatorpoweraction= new Generatorpoweraction($request->all());
       $generatorpoweraction['user_id'] =  \Auth::user()->id;
       $generatorpoweraction['user_name'] =  \Auth::user()->name;
       $generatorpoweraction['battery_id'] =$request->id;
       $poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
       $generatorpoweraction->poweraction()->associate(  $poweraction );
       $x= $request->id;
    
       $oldSiteId=Site::find($battery->site_id);
     
       $oldSiteId->battery=$oldSiteId->battery - $battery->numbers;
       $oldSiteCode=$oldSiteId->site_code;
       $generatorpoweraction['old_site']=$oldSiteCode;
       $battery['site_id']   =null;
       $battery['cabinet_id']   =null;
       $battery['solar']   =null;
       $battery['status'] ='Used';
       $battery['dismantle_date']=$request->get('dismantle_date');
       $battery['shipment_date']=null;
       $site = Site::query()->where('site_code',$request->get('warehouse_name'))->first();
       $warehouse = Warehouse::query()->where('name',$request->get('warehouse_name'))->first();
       $warehouse->battery = $warehouse->battery + $battery->numbers;
      $battery->warehouse()->associate(  $warehouse )  ;
      
               $generatorpoweraction->save();
         
               $battery->save();
              
               $warehouse->save();
               $oldSiteId->save();
       return  response()->json( $battery);
   }





   function import_ex(){

    if(Auth::User()->can('import')){
    return view('batteries.import');
          }
          else{

            return redirect()->route('home');
         }
        }
          function importExcelCSV(Request $request){
     
    if(Auth::User()->can('import')){
    $validatedData = $request->validate([

        'file' => 'required',

     ]);

     Excel::import(new BatteryImport,$request->file('file'));

         
     return redirect('batteryimport')->with('status', 'The file has been  imported to database ');}

 
}


    
    public function editnumber(Battery $battery)
         {
             return response()->json($battery);


         }




         public function editreshuffling(Battery $battery)
         {
             return response()->json($battery);


         }
         public function updatenumber(Request $request)
         {
            $request->validate([

    
                'site_code'=>'exists:sites,site_code|required',
                'cabinet_id'=>'exists:cabinets,id|nullable',
                'solar'=>'exists:solars,id|nullable',
                    'pr'=>'required',
                    'installation_date'=>'required',
              
                
                ]);

            $battery =  Battery::updateOrCreate(
                ['id'=>$request->id],
             
            );
            $warehouse= Warehouse::query()->where('id',$battery->warehouse_id)->first();

            $site = Site::query()->where('site_code',$request->get('site_code'))->first();
            $site->battery=   $site->battery+  $request->number;
           $battery->numbers = $battery->numbers - $request->number;
          if( $battery->numbers == 0 ){
            $battery->numbers= $request->number;
            $battery->site()->associate(  $site );
            $battery->installation_date=$request->get('installation_date');
            $battery->warehouse_id=null;
            $battery->solar=$request->get('solar');
            $battery->cabinet_id=$request->get('cabinet_id');

          }
          else  if( $battery->numbers > 0 ){
           $batteryy= $battery->replicate();


            $batteryy->cabinet_id=$request->get('cabinet_id');
            $batteryy->warehouse_id=null;
            $batteryy->solar=$request->get('solar');
            $batteryy->numbers=$request->get('number');
            $batteryy->installation_date=$request->get('installation_date');
            $batteryy->site()->associate(  $site );
            $batteryy->save();
           
        }
        $warehouse->battery= $warehouse->battery -$request->number;
        $warehouse->save();
$battery->save();
$site->save();
$generatorpoweraction =new Generatorpoweraction();
$x="installation";
$generatorpoweraction['user_id'] =  \Auth::user()->id;
$generatorpoweraction['user_name'] =  \Auth::user()->name;
$generatorpoweraction['battery_id']=$request->get('id');;
$generatorpoweraction['pr']= $request->get('pr');
$poweraction = Poweraction::query()->where('action_name',$x)->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );





$generatorpoweraction->save() ;
            return  response()->json( $battery);


         }


    public function addShipment(Request $request)
    {

        $request->validate([

            'warehouse_name'=>'required|exists:warehouses,name',
            'battery_brand'=>'required',
            'capacity'=>'required',
            'numbers'=>'required',
            'status'=>'required',
            'oracle_code'=>'required',
            'po'=>'required',
            'shipment_date'=>'date|required'

            
        

        ]); 
        
        $warehouse = Warehouse::query()->where('name',$request->get('warehouse_name'))->first();
$warehouse->battery =  $warehouse->battery + $request->get('numbers') ;
        $batteries =new Battery($request->all());

        $batteries['user_id']=  \Auth::user()->id;
        $batteries->warehouse()->associate( $warehouse );
 
        $batteries->save();
        $warehouse->save();
        return redirect()->route('batterywarehouse')->with('success',  'Shipment added successfully');
    }



    public function show(Battery $battery)
    {
        //
    }

   
    public function edit(Battery $battery){
    if(Auth::User()->can('edit'))
    {
        $cabinet=Cabinet::where('site_id','=',$battery->site_id)->get();
        $solar=Solar::where('site_id','=',$battery->site_id)->get();

        return view('batteries.edit',compact('battery','cabinet','solar'));
    }
    else{
        return redirect()->route('batteries.index');
    }
    }

    
    public function update(Request $request, Battery $battery)
    {
        $request->validate([

                'cabinet_id'=>'exists:cabinets,id|nullable',
                
                'solar'=>'exists:solars,id|nullable',
                'battery_brand'=>'required',
                'capacity'=>'required',
                'oracle_code'=>'required',
              

                        ]);
            
  $data = $request->all();


  $battery -> update ($data);


$battery->save();
            return redirect()->route('batteries.index')->with('success','battery updated successfully');
    }

    public function destroy(Battery $battery)
    {
if(Auth::User()->can('delete')){
        $site=Site::find( $battery->site_id);
        $site->battery= $site->battery -  $battery->numbers;
        $site->save();
       $battery->is_active='0';
       $battery->status='Used';
       $battery->site_id=null;
       $battery->solar=null;
       $battery->cabinet_id=null;
       $battery->save();
         return redirect()->route('batteries.index')->with('success','battery deleted successfully');}
         else{
            return redirect()->route('batteries.index');
         }
    }



    public function updatereshuffling(Request $request)
    {
        if(Auth::User()->can('admin')){
     
        $request->validate([

              'action_name'=>'required',
              'site_code'=>'required',
              'id'=>'required',
              'trx'=>'required',

                     ]);

       $battery = Battery::updateOrCreate(
           ['id'=>$request->id],

       );

       $generatorpoweraction= new Generatorpoweraction($request->all());
       $generatorpoweraction['user_id'] =  \Auth::user()->id;
       $generatorpoweraction['battery_id']=$request->id;
       $generatorpoweraction['user_name'] =  \Auth::user()->name;


      $poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
      $generatorpoweraction->poweraction()->associate(  $poweraction );

$oldSiteId=Site::find($battery->site_id);
$oldSiteCode=$oldSiteId->site_code;
$generatorpoweraction['old_site']=$oldSiteCode;
if(strtoupper($request->site_code) == strtoupper($oldSiteCode)){

return  response()->json( $battery);
}
else{
    
$generatorpoweraction['new_site']=$request->site_code;
$oldSiteId->battery =$oldSiteId->battery - $battery->numbers;
$site=Site::query()->where('site_code',$request->site_code)->first();
$site->battery=$site->battery  +   $battery->numbers;
$battery->site()->associate( $site );
$battery->installation_date=$request->installation_date;
$battery->cabinet_id=$request->cabinet_id;
$battery->solar=$request->solar;
$oldSiteId->save();
$battery->save();


$site->save();
$generatorpoweraction->save();

       return  response()->json( $battery);}
    }
    }
 
 
 
    public function cheackCabinet(Request $request){

$site=Site::where('site_code','=',$request->get('site_code'))->first();
if($site){
    $cabinet=Cabinet::where('site_id','=',$site->id)->where('is_active','=','1')->get();
    
    return response()->json($cabinet);
  }
    }


 
    public function cheackSolar(Request $request){

        $site=Site::where('site_code','=',$request->get('site_code'))->first();
        if($site){
            $solar=Solar::where('site_id','=',$site->id)->where('is_active','=','1')->get();
            
            return response()->json($solar);
          }
            }


/*
            public function cheackSolars(Request $request ){


                if($request->get('solar_id')==null){
                  return response()->json(['b' =>true,
              'a'=>true
              ]);
                }
                else{
                  $s= Solar::where('id','=',$request->get('solar_id'))->first();
                  return response()->json(['a' =>$s ]);
                }
          
                   
                  
              }
*/
          /*    public function cheackCabinets(Request $request ){


                if($request->get('cabinet_id')==null){
                  return response()->json(['b' =>true,
              'a'=>true
              ]);
                }
                else{
                  $ca= Cabinet::where('id','=',$request->get('cabinet_id'))->first();
                  return response()->json(['a' =>$ca ]);
                }
          
                   
                  
              }*/

}
