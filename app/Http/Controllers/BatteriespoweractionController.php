<?php

namespace App\Http\Controllers;

use App\Models\Batteriespoweraction;
use Illuminate\Http\Request;

class BatteriespoweractionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Batteriespoweraction  $batteriespoweraction
     * @return \Illuminate\Http\Response
     */
    public function show(Batteriespoweraction $batteriespoweraction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Batteriespoweraction  $batteriespoweraction
     * @return \Illuminate\Http\Response
     */
    public function edit(Batteriespoweraction $batteriespoweraction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Batteriespoweraction  $batteriespoweraction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Batteriespoweraction $batteriespoweraction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Batteriespoweraction  $batteriespoweraction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Batteriespoweraction $batteriespoweraction)
    {
        //
    }
}
