<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;


use Illuminate\Http\Request;

class loController extends Controller
{
    function logout(Request $request){
        Auth::logout();
        return redirect()->back();
    }
 
}
