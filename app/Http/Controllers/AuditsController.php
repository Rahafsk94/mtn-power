<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Site;
use App\Models\User;
use App\Models\Audit;
use App\Models\Tank;
use App\Models\Solar;
use App\Models\Battery;
use App\Models\Ampere;
use App\Models\Cabinet;
use App\Models\Generator;

class AuditsController extends Controller
{
    function __construct(){
         $this->middleware('role:Management');
    }
    public function index( )
    {
        $generator=Generator::whereHas('audits')->get() ;


 
    return view('generators.tracking',compact('generator'));
    }
   
    
    
    
    public function track()
{

 $site = Site::whereHas('audits')->get() ;

   return view('sites.tracking',compact('site'));
          

}

   


public function amperetrack()
{

 
    $ampere = Ampere::whereHas('audits')->get() ;

   return view('ampere.tracking',compact('ampere'));
          

}
public function batterytrack()
{

 
    $battery = Battery::whereHas('audits')->get() ;

   return view('batteries.tracking',compact('battery'));
          

}
public function trackshow($generator){
  

$audit =Audit::where('auditable_id','=',$generator)->where('auditable_type','=','App\Models\Generator')->where('event','=','updated')->first();

return view('generatorpoweractions.show',compact('audit'));


}
public function tanktrack()
{

 
    $tank=Audit::where('auditable_type','=','App\Models\Tank')->get();

   return view('tanks.tracking',compact('tank'));
          

}


public function solartrack()
{
    if(\Auth::User()->hasRole('Management')){

    $solar = Solar::whereHas('audits')->get();

   return view('solars.tracking',compact('solar'));}
          

}


public function cabinetTrack()
{
    if(\Auth::User()->hasRole('Management')){

    $cabinet= Cabinet::whereHas('audits')->get();

   return view('cabinets.tracking',compact('cabinet'));}
          

}
}