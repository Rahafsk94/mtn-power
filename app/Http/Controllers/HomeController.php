<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Video;
use  App\Events\ViedoeViwer;
use App\Models\User;



class HomeController extends Controller
{
   
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    
    
    public function index()
    {
        return view('home');
    }
  
    public function poweractions()
    {
        return view('poweractions');
    }
    public function import()
    {
    if( Auth::User()->can('import')){
        return view('import');
    }
    else{
        
        return redirect()->route('home');
    
    }
    }
}
