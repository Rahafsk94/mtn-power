<?php
namespace App\Http\Controllers;
use App\Models\Generator;
use App\Models\Site;
use App\Models\Warehouse;
use App\Models\Poweraction;
use App\Models\Generatorpoweraction;
use App\Models\User;
use App\Models\Generatortank;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GeneratorsImport;
use  App\Exports\GeneratorExport;
use Illuminate\Support\Facades\Auth;
use App\Models\Audits; 
use DB;
use Carbon;


class GeneratorController extends Controller
{




    function __construct(){
        $this->middleware('auth');
   }
 
    public function index()
    {
        

      
        
$generator=Generator::whereNotnull('site_id')->where('is_active','=','1')->get();

return view('generators.index',compact('generator'));

  
    }
   
    public function warehouse()
    {
    
     
   
$generator=Generator::whereNotnull('warehouse_id')->where('is_active','=','1')->get();

return view('generators.storage',compact('generator'));



     
  
    }

    public function addaction()
    {

        if(Auth::User()->can('admin')){
        $warehouses=Warehouse::get();
        $p=Poweraction::get();

      

       $generator=Generator::whereNotnull('site_id')->where('is_active','=','1')->get();

return view('generators.actions',compact('generator','p','warehouses'));}
else{
    return redirect()->route('generators.index');
}

     
  
    }


 public function in()

     {
        $p=Generator::get();
         return view('generators.index',compact('p'));
 
     }


     function fetch_data(Request $request)
     {
         if($request->ajax())
         {
             $data = DB::table('generators')->get();
             echo json_encode($data);
         }
     }




  



     function action(Request $request)
     {
         if($request->ajax())
         {
             if($request->action == 'edit')
             {
                 $data = array(
                     'generator_name'	=>	$request->generator_name,
                   
                 );
                 DB::table('generators')
                     ->where('id', $request->id)
                     ->update($data);
             }
             if($request->action == 'delete')
             {
                 DB::table('generators')
                     ->where('id', $request->id)
                     ->delete();
             }
             return response()->json($request);
         }
     }

    public function create()
    {
        if(Auth::User()->can('create')){
$generator=Generator::get();
        return view('generators.create',compact('generator'));}
        else{
            return redirect()->route('generators.index') ;
        }

    }

    public function craeteShipment()
    {
        if(Auth::User()->can('create')){
$generator=Generator::get();
        return view('generators.createwarehouse',compact('generator'));}
        else{
            return redirect()->route('generators.index');
        }

    }

    public function store(Request $request)
    {
      
        $request->validate([

    
            'site_code'=>'exists:sites,site_code|required',
            'generator_name'=>'required',
            'generator_capacity'=>'required|numeric',
            'engine_brand'=>'required',
            'owners'=>'required',
            'installation_date'=>'required|date',
            'site_code_gen_id'=>'required',
            'runing_hour'=>'required|numeric',
            'pr'=>'required|numeric',
            'oracle_code'=>'numeric|nullable'
        ]);


        $generator= new Generator( $request->all());

        $site = Site::query()->where('site_code',$request->get('site_code'))->first();

$generator->site()->associate( $site );
 
$generator->save();
$generatorpoweraction =new Generatorpoweraction();
$x="installation";
$generatorpoweraction['user_id'] =  \Auth::user()->id;
$generatorpoweraction['user_name'] =  \Auth::user()->name;
$generatorpoweraction['generator_id']= $generator->id;
$generatorpoweraction['pr']= $request->get('pr');
$poweraction = Poweraction::query()->where('action_name',$x)->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );

$generatorpoweraction->save() ;



        return redirect()->route('generators.index')->with('success',  'generator added successfully');


    }
  
    public function addShipment( Request $request)
    {

                $request->validate([

            'generator_name'=>'required',
            'generator_capacity'=>'required|numeric',
            'engine_brand'=>'required',
            'activation_date'=>'date',
            'site_code_gen_id'=>'required',
            'oracle_code'=>'required',
            'number'=>'required|numeric|gt:0',
            'warehouse_name'=>'required|exists:warehouses,name',
        ]);
        $warehouse = Warehouse::query()->where('name',$request->get('warehouse_name'))->first();


$x=$request['number'];
$warehouse->generator=$warehouse->generator()->whereNull('end_date')->where('is_active','=','1')->count() +$x;

$warehouse->save();
$generator= new generator($request->all());
//dd($generator->whereNull('end_date')->where('is_active','=','1')->where('warehouse_id','=',$generator->warehouse_id)->count());
$generator->warehouse()->associate(  $warehouse  );


for ( $i=$x ; $i > 0; $i-- ){

        $data[] = array(
            'warehouse_id' => $generator->warehouse_id,
        'site_id' => $generator->site_id,
        'generator_name' => $request->input('generator_name'),
        'generator_capacity' => $request->input('generator_capacity'),
        'engine_brand' => $request->input('engine_brand'),
        'owners' => 'MTN',
        'oracle_code' => $request->input('oracle_code'),
        'shipment_date' => $request->input('activation_date'),
        'site_code_gen_id' => $request->input('site_code_gen_id'),
        'runing_hour' => '0',
        'user_id' =>  \Auth::user()->id,

        );

}

DB::table('generators') -> insert($data);


return redirect()->route('generatorswarehouse')->with('success','generator updated successfully');;

    }


    public function show(generator $generator)
    {
        return view('generators.show',compact('generator'));

    }

    public function edit(generator $generator)
    {
        if(Auth::User()->can('edit')){
        $sites=Generator::get();
        return view('generators.edit',compact('generator','sites')) ;}
        else{
            return redirect()->route('generators.index');
        }
    
    }

    public function update(Request $request, generator $generator)
    {
        $request->validate([

           
            'generator_name'=>'required',
            'generator_capacity'=>'required|numeric',
            'engine_brand'=>'required',
            'owners'=>'required',
            'oracle_code'=>'required',
    
            'site_code_gen_id'=>'required',
            'runing_hour'=>'required|numeric',
  ]);


  $data = $request->all();

  $data['updated_by'] = \Auth::user()->name;

  $generator -> update ($data);

  $generator->save();
       return redirect()->route('generators.index')->with('success','generator updated successfully');
    }


    public function destroy(generator $generator)
    {
        if(Auth::User()->can('delete')){
    
        $g=  $generator->generatorpoweraction()->first();

        $r= $generator->generatortank()->get();
    
        if ($g){
            $generator->is_active ="0";
            $generator->generatortank()->delete();
$site=Site::where('id','=',$generator->site_id)->first();

            $generator->site_id=null;
            $generator->warehouse_id=null;
            
            $generator->end_date= Carbon\Carbon::now();

            $generator->save();
        $site->generator=    $site->generator()->where('is_active','=','1')->count() ;
            $site->save();
         return redirect()->route('generators.index')->with('success','The generator has deleted successfully');
        }
        else{

         
         $generator->delete();
        return redirect()->route('generators.index')->with('danger','The generator has deleted successfully');
     }
    }
    else{

        return redirect()->route('generators.index');
    }
    }

    public function search(Request $request){
        

        // Get the search value from the request
        $searchs = $request->input('searchs');

        // Search in the title and body columns from the posts table
          
      /*  $generators=DB::table('generators')->select('*')
        ->join('sites', function ($join) use($request){  
        
               $searchs = $request->input('searchs');
        $join->on('sites.id', '=', 'generators.site_id')  ->where('site_code', 'LIKE', "{$searchs}")
        ;
        })
        ->get();*/
      $generators = Generator::query()->whereHas('site',function($query) use ($searchs){
        return $query->where('site_code','LIKE', "{$searchs}");

      })->get();
            return view('generators.search', compact('generators'));

        // Return the search view with the resluts compacted
    }
    public function compare(Request $request){
        $gen=null;

                $x = $request->get('x');

                if($x!=null){
                     $gen =  Generator::query()
                        ->where('runing_hour', '<=', $x)

                        ->get();
                }

                $y = $request->get('y');

                if($y!=null){
                     $gen = Generator::query()
                        ->where('runing_hour', '>=', $y)

                        ->get();
                }
                $u = $request->get('u');
                $z = $request->get('z');
                if($u!=null && $z!=null){


                    $gen = Generator::query()
                    ->whereBetween('runing_hour', [$u,$z])->get();
            }

                return view('generators.runingsearch',compact('gen'));

            }


            function import_ex(){
                if(Auth::User()->can('import')){
                return view('generators.import');}
                else{
                 return redirect()->route('home');

                }
            }
            function importExcelCSV(Request $request){
                $validatedData = $request->validate([
 
                    'file' => 'required',
          
                 ]);
          
                 Excel::import(new GeneratorsImport,$request->file('file'));
          
                     
                 return redirect('generatorimport')->with('status', 'The file has been  imported to database ');
             
            }
        

    public function export() 
    {
        return Excel::download(new  GeneratorExport, 'generator.xlsx');
    }


    public function powerAction(){


       $site=Site::get();
  
        return view('generators.hi',compact('site'));
            

        
       
    }
    public function view()
    {
   
    $generatordata=Generator::find(1);
    $tanks=Tank::get();
    return view('tanks.index', compact('tanks', 'generatordata'));
    }
           

    public function editnumber(Generator $generator)
         {
             return response()->json($generator);
         }
         
         public function updatenumber(Request $request)
         {
            $request->validate([

    
                'site_code'=>'exists:sites,site_code|required',
                    'pr'=>'required',]);

            $generator =  Generator::updateOrCreate(
                ['id'=>$request->id],

            );

            $site = Site::query()->where('site_code',$request->get('site_code'))->first();
            $warehouse= Warehouse::query()->where('id',$generator->warehouse_id)->first();
            
            $generator->site()->associate(  $site );

            $site->generator= $site->generator()->whereNull('end_date')->where('is_active','=','1')->count() + 1;

            $warehouse->generator=  $warehouse->generator()->whereNull('end_date')->where('is_active','=','1')->count() - 1;


            $generator->warehouse_id=null;
       
            $generator->installation_date= $request->get('installation_date') ;
            $site->save();
            $warehouse->save();
            $generator->save();
            $generatorpoweraction =new Generatorpoweraction();
$x="installation";
$generatorpoweraction['user_id'] =  \Auth::user()->id;
$generatorpoweraction['user_name'] =  \Auth::user()->name;
$generatorpoweraction['generator_id']=$request->get('id');
$generatorpoweraction['pr']= $request->get('pr');
$poweraction = Poweraction::query()->where('action_name',$x)->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );





$generatorpoweraction->save() ;
            return  response()->json( $generator);


         }


         public function editdismantle(Generator $generator)
         {
             return response()->json($generator);


         }
         public function editreshuffling(Generator $generator)
         {
             return response()->json($generator);


         }

         public function updatedismantle(Request $request)
         {

            $request->validate([

    
                    'rm'=>'required',
                    'warehouse_name'=>'required|exists:warehouses,name',
                    'action_name'=>'required',
                ]);

            $generator =  Generator::updateOrCreate(
                ['id'=>$request->id],

            );
            $generatorpoweraction= new Generatorpoweraction($request->all());
            $generatorpoweraction['user_id'] =  \Auth::user()->id;
            $generatorpoweraction['user_name'] =  \Auth::user()->name;
            $generatorpoweraction['generator_id'] =$request->id;
            $poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
            $generatorpoweraction->poweraction()->associate(  $poweraction );


            $x= $request->id;
                    
            $generator=Generator::find($x);
            $id=$generator->site_id  ;
            $oldsite=Site::find($id);
         $oldsite->generator=$oldsite->generator()->whereNull('end_date')->where('is_active','=','1')->count() -1  ;


         $warehouse = Warehouse::query()->where('name',$request->get('warehouse_name'))->first();
         $warehouse->generator =$warehouse->generator()->whereNull('end_date')->where('is_active','=','1')->count() +1 ;
        $generator->warehouse()->associate(  $warehouse )  ;
        $generatortank=Generatortank::where('generator_id','=',$request->id);
        $generatortank->delete();
        $id= $generator->site_id;


       $oldSiteId=Site::find($id);
       $oldSiteCode=$oldSiteId->site_code;
        $oldSiteCode;
       $generatorpoweraction['old_site']=$oldSiteCode;



       
               $generator->site_id=null;
               $generator->dismantle_date=$request->get('dismantle_date');
               $generator->shipment_date=null;
   
             $warehouse->save();
         $oldsite->save();
                    $generatorpoweraction->save();
                    $generator->save();

            
            return  response()->json( $generator);
        }




        public function updatereshuffling(Request $request)
        {
if(Auth::User()->can('admin')){
         
            $request->validate([

                'action_name'=>'required',
                 
            'site_code'=>'required',
   'id'=>'required',
                 
                 'trx'=>'required',
                         ]);

           $generator =  Generator::updateOrCreate(
               ['id'=>$request->id],

           );
    
           $generatorpoweraction= new Generatorpoweraction($request->all());
           $generatorpoweraction['user_id'] =  \Auth::user()->id;
           $generatorpoweraction['generator_id']=$request->id;
           $generatorpoweraction['user_name'] =  \Auth::user()->name;

    
$poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );
$generator=Generator::find($request->id);
$id= $generator->site_id;
$oldSiteId=Site::find($id);
$oldSiteCode=$oldSiteId->site_code;
$generatorpoweraction['old_site']=$oldSiteCode;
if(strtoupper($request->site_code) == strtoupper($oldSiteCode)){
    return  response()->json( $generator);
}
else{
    $generatortank=Generatortank::where('generator_id','=',$request->id);
    $generatortank->delete();
    $generatorpoweraction['new_site']=$request->site_code;
    $oldSiteId->generator =$oldSiteId->generator()->whereNull('end_date')->where('is_active','=','1')->count()-1;
    $site=Site::query()->where('site_code',$request->site_code)->first();
    $site->generator=$site->generator()->whereNull('end_date')->where('is_active','=','1')->count() +1;
    $generator->site()->associate( $site );
    $generator->installation_date=$request->get('date');
    $oldSiteId->save();
    $generator->save();
    

$site->save();
$generatorpoweraction->save();

           return  response()->json( $generator);}}
        }


        public function cheackGenerator(Request $request ){



  
            if($request->get('generator_id')==null){
              return response()->json(['b' =>true,
          'a'=>true
          ]);
            }
            else{
                $generator_id=Generator::where('id','=',$request->get('generator_id'))->first();
              return response()->json(['a' =>$generator_id ]);
            }
      
               
              
          }




          public function cheackSiteGenerator(Request $request ){
            $site=Site::where('site_code','=',$request->get('site_code'))->where('is_active','=','1')->first();




            $generator=Generator::where('site_id','=',$site->id)->where('is_active','=','1')->get();
          
            $generator_id=Generator::where('id','=',$request->get('generator_id'))->first();
      


 

              return response()->json(  $generator);
            
      
               
              
          }

}
