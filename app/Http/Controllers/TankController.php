<?php

namespace App\Http\Controllers;

use App\Models\Tank;
use App\Imports\TankImport;
use Illuminate\Http\Request;
use App\Models\Generator;
use App\Models\Generatortank;
use App\Models\Site;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Carbon;

class TankController extends Controller
{

   
    function __construct(){
        $this->middleware('auth');
   }
    
    public function index()
    {
    $tankdata=Tank::where('is_active','=','1')->get();

    return view('tanks.index', compact('tankdata'));
    }

   
    public function create()
    {
        if(Auth::User()->can('create')){
        return view('tanks.create');}
        else{

            return redirect()->route('tanks.index') ;
        }
    }
    public function creategeneratortank($generator)
    {
        $g=Generator::where('id','=',$generator)->first();

        
        $tank=Tank::where('site_id','=',$g->site_id)->get();
       
        return view('tanks.addtanktogen',compact('generator','tank'));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'site_code'=>'required|exists:sites,site_code',
                'generator_id'=>'required|exists:generators,id',
                'installation_date'=>'required',
      
                'tank_index'=>'required',
                'tank_sharp'=>'required',
                'tank_dimension1'=>'required|numeric',
                'tank_dimension2'=>'required|numeric',
                'tank_dimension3'=>'required|numeric',
                'tank_diameter'=>'required|numeric',

            ]
        );
  
   

        $site=Site::query()->where('site_code',$request->get('site_code'))->first();
        $data =$request->all();

        $tank=new Tank($data);
        
        $tank->site()->associate( $site );
        $tank->save();
 
        $x=   new Generatortank();
    
        $x['generator_id']= $request->get('generator_id') ;
        $x['tank_id']=$tank->id;
        $x['installation_date']=$request->get('installation_date') ;
        $x['end_date']=$request->get('end_date') ;;
        $x->save();
        return redirect()->route('tanks.index')->with('success','Tank added successfully');

    }
   
  
    public function storegeneratortank(Request $request, $generator)
    {
        $request->validate(
            [
            
                'installation_date'=>'required',
              
                'tank_id'=>'required|exists:tanks,id',
            
            ]
        );
  
   
        $data =$request->all();

         $generatortank=new  Generatortank($data);
         $generatortank['generator_id']= $generator;
         $generatortank->save();
 
   
        return redirect()->route('tanks.index')->with('success','Tank added successfully');



    }


  



    public function edit(Tank $tank)
    {
        if(Auth()->User()->can('edit')){
        return view('tanks.edit',compact('tank'));}
        else{
            return redirect()->route('tanks.index');
        }
    }

    public function update(Request $request, Tank $tank)
    {
        $request->validate([
            'tank_index'=>'required|numeric',
            'tank_sharp'=>'required|numeric',
            'tank_dimension1'=>'required|numeric',
            'tank_dimension2'=>'required|numeric',
            'tank_dimension3'=>'required|numeric',
            'tank_diameter'=>'required|numeric',
        ]);
        $tank->update($request->all());
        return redirect()->route('tanks.index')->with('success','tank updated successfully');
    }

   
    public function destroy(Tank $tank)
    {
        if(Auth()->User()->can('delete')){
        
        $generatorTank=  $tank->generatortank()->get();
      $site_id=$tank->site_id;
     
        if ( $generatorTank){
         
            $tank->generatortank()->delete();
          $tank['is_active']='0';
          $tank['site_id']=null;
          $tank['end_date']=Carbon\Carbon::now();
          $tank->save();

        }
        else{

         
            $tank['is_active']='0';
            $tank['site_id']=null;
            $tank['end_date']=Carbon\Carbon::now();
            $tank->save();
      
     }

     $site=Site::where('id','=',$site_id)->first();

     $site->tank= $site->tank()->where('is_active','=','1')->count();

     $site->save();
     return redirect()->route('tanks.index')->with('success','Tank deleted successfully');
    
       
    }
     
    
    }
    function import_excel(){
        if(Auth::User()->can('import')){
        return view('tanks.import');}
        else{
            return redirect()->route('home');
        }
    }
    function import(Request $request){
        if($request->file('file')){
          $import =  Excel::import(new TankImport, request()->file('file'));
            $msg_success = "Data Uploaded Succesfully! ";
            $msg_danger = "Data Uploaded failed! ";
            if ($import) {
                return redirect('tanksimport')->with('u', strtoupper($msg_success));
            }else{
               return redirect('/tanksimport')->with('u', strtoupper($msg_danger));
            }
        }
        else{
            $msge = "please choose your file! ";
            return redirect('/tanksimport')->with('choose_file', strtoupper($msge));
        }

      /*  $import= Excel::toCollection(new TankImport,request()->file('file'))->first();
       foreach($import as $row){
          
     

        }
    
        $msg_success = "Data Uploaded Succesfully! ";
        $msg_danger = "Data Uploaded failed! ";
        if ($import) {
            return redirect('tanksimport')->with('u', strtoupper($msg_success));
        }else{
           return redirect('/tanksimport')->with('u', strtoupper($msg_danger));
        }
    }
    else{
        $msge = "please choose your file! ";
        return redirect('/tanksimport')->with('choose_file', strtoupper($msge));
    
    }*/}
 

    public function cheackTankGenerator(Request $request){

        $generatorTank=Generatortank::where('generator_id','=',$request->get('generator_id'))->where('tank_id','=',$request->get('id'))->first();

        if($generatorTank){
            return response()->json(['a'=>$generatorTank]);
        }
    }
}
