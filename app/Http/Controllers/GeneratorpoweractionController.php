<?php

namespace App\Http\Controllers;
use App\Models\Generatorpoweraction;
use App\Models\Poweraction;
use App\Models\Generator;
use App\Models\Battery;
use App\Models\Site;
use App\Models\Warehouse;
use App\Models\Audit;
use Illuminate\Http\Request;
use Carbon\Carbon;


class GeneratorpoweractionController extends Controller
{
    public function reshuffling()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','reshuffling')->first();
        $id=  $poweraction->id;
       
      $reshufflings=Generatorpoweraction::whereNotNull('generator_id')->where('action_id','=',$id)->get();
 
        return view('generatorpoweractions.reshuffling',compact('reshufflings'));
    }
   
    public function dismantle()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','dismantle')->first();
        $id=  $poweraction->id;
       
      $dismantles=Generatorpoweraction::whereNotNull('generator_id')->where('action_id','=',$id)->get();
 
        return view('generatorpoweractions.dismantle',compact('dismantles'));
    }


    public function batteryDismantle()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','dismantle')->first();
        $id=  $poweraction->id;
 
      $dismantles=Generatorpoweraction::whereNotNull('battery_id')->where('action_id','=',$id)->get();
 
        return view('batteries.dismantle',compact('dismantles'));
    }



    public function cabinetDismantle()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','dismantle')->first();
        $id=  $poweraction->id;
 
      $dismantles=Generatorpoweraction::whereNotNull('cabinet_id')->where('action_id','=',$id)->get();
 
        return view('cabinets.dismantle',compact('dismantles'));
    }

    public function solarDismantle()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','dismantle')->first();
        $id=  $poweraction->id;
 
      $dismantles=Generatorpoweraction::whereNotNull('solar_id')->where('action_id','=',$id)->get();
 
        return view('solars.dismantle',compact('dismantles'));
    }
    public function batteryReshuffling()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','reshuffling')->first();
        $id=  $poweraction->id;
       
      $reshufflings=Generatorpoweraction::whereNotNull('battery_id')->where('action_id','=',$id)->get();
 
        return view('batteries.reshuffling',compact('reshufflings'));
    }




    public function cabinetReshuffling()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','reshuffling')->first();
        $id=  $poweraction->id;
       
      $reshufflings=Generatorpoweraction::whereNotNull('cabinet_id')->where('action_id','=',$id)->get();
 
        return view('cabinets.reshuffling',compact('reshufflings'));
    }







    public function solarReshuffling()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','reshuffling')->first();
        $id=  $poweraction->id;
       
      $reshufflings=Generatorpoweraction::whereNotNull('solar_id')->where('action_id','=',$id)->get();
 
        return view('solars.reshuffling',compact('reshufflings'));
    }


    public function install()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','installation')->first();
        $id=  $poweraction->id;
       
      $installations=Generatorpoweraction::where('action_id','=',$id)->WhereNotNull('generator_id')->get();
 
        return view('generatorpoweractions.installation',compact('installations'));
    }



    public function cabinetInstall()
    { 
        $poweraction=Poweraction::query()->where('action_name','=','installation')->first();
        $id=  $poweraction->id;
       
      $installations=Generatorpoweraction::where('action_id','=',$id)->WhereNotNull('cabinet_id')->get();
 
        return view('cabinets.installation',compact('installations'));
    }




    public function create( $generator)
    {
        $p=Poweraction::get();
        return view('generatorpoweractions.create',compact('p','generator'));
    }

    
    public function store(Request $request)
    {


        $request->validate([

            'rm'=>'required',
            'generator_id'=>'required',
            'action_name'=>'required',
           'warehouse_name'=>'required|exists:warehouses,name'
                    ]);
       
                    $generatorpoweraction= new Generatorpoweraction($request->all());
                    $generatorpoweraction['user_id'] =  \Auth::user()->id;
                    $generatorpoweraction['user_name'] =  \Auth::user()->name;
                   
                    $poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
                    $generatorpoweraction->poweraction()->associate(  $poweraction );
                    $x= $request->generator_id;
                    
                    $generator=Generator::find($x);
    $id=$generator->site_id  ;
        $oldsite=Site::find($id);
     $oldsite->generator=$oldsite->generator()->whereNull('end_date')->where('is_active','=','1')->count() -1  ;

         
                    $warehouse = Warehouse::query()->where('name',$request->get('warehouse_name'))->first();
                $warehouse->generator =$warehouse->generator()->whereNull('end_date')->where('is_active','=','1')->count() +1 ;
               $generator->warehouse()->associate(  $warehouse )  ;

       $id= $generator->site_id;


       $oldSiteId=Site::find($id);
       $oldSiteCode=$oldSiteId->site_code;
        $oldSiteCode;
       $generatorpoweraction['old_site']=$oldSiteCode;


  
               $generator->site_id=null;
        
           
               
              $warehouse->generator=$warehouse->generator +1;
             $warehouse->save();
         $oldsite->save();
                    $generatorpoweraction->save();
                    $generator->save();

                        
                        return redirect()->route('generators.index')->with('success',  'u added successfully');
            
                       
           
    }


      
    public function batterystore(Request $request)
    {


        $request->validate([

            'rm'=>'required',
            'battery_id'=>'required',
            'action_name'=>'required',
           'site_code'=>'required|exists:sites,site_code'
                    ]);
             
                    $generatorpoweraction= new Generatorpoweraction($request->all());
                    $generatorpoweraction['user_id'] =  \Auth::user()->id;
                    $generatorpoweraction['user_name'] =  \Auth::user()->name;
                   
                    $poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
                    $generatorpoweraction->poweraction()->associate(  $poweraction );
               
                    $x= $request->battery_id;
                    
                    $battery=Battery::find($x);
    $id=$battery->site_id  ;
        $oldsite=Site::find($id);
         $oldsite->battery=$oldsite->battery -1;
         
                    $site = Site::query()->where('site_code',$request->get('site_code'))->first();
                   
               $battery->site()->associate(  $site )  ;
               $battery->save();
         $site->battery=$site->battery +1;
         $site->save();
         $oldsite->save();
                    $generatorpoweraction->save();


                        
                        return redirect()->route('batteries.index')->with('success',  'u added successfully');
            
                       
           
    }




    public function storee(Request $request)
    {
       
        $request->validate([

           'action_name'=>'required',
            
            'generator_id'=>'required',
         
            
            
                    ]);

                    $generatorpoweraction= new Generatorpoweraction($request->all());
                    $generatorpoweraction['user_id'] =  \Auth::user()->id;
                    $generatorpoweraction['user_name'] =  \Auth::user()->name;

             
$poweraction = Poweraction::query()->where('action_name',$request->get('action_name'))->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );




$generators= $generatorpoweraction->generator_id ;

$gene=Generator::find($generators);

$id= $gene->site_id;


$oldSiteId=Site::find($id);
$oldSiteCode=$oldSiteId->site_code;
$generatorpoweraction['old_site']=$oldSiteCode;
$x=$request->validate([

    'site_code'=>'required|exists:sites,site_code',
     

  
     
             ]);

$r= $x['site_code'];

if($r==$oldSiteCode){

    return redirect()->route('generators.index')->with('success',  'no site ');
}

else{

    $generatorpoweraction['new_site']=$r;

 $oldSiteId->generator =$oldSiteId->generator()->whereNull('end_date')->where('is_active','=','1')->count()-1;

    $oldSiteId->save();
            $site=Site::query()->where('site_code',$r)->first();
      
$gene->site()->associate( $site );

$gene->save();

$site->generator=$site->generator()->whereNull('end_date')->where('is_active','=','1')->count() ;


$site->save();
$generatorpoweraction->save();



                    return redirect()->route('generators.index')->with('success',  ' reshuffling successfully');
    }}

 
    public function edit(Generatorpoweraction $generatorpoweraction)
    {
        //
    }


    public function update(Request $request, Generatorpoweraction $generatorpoweraction)
    {
        //
    }

    public function destroy(Generatorpoweraction $generatorpoweraction)
    {
        //
    }
    public function show(Generator $generator,Generatorpoweraction $generatorpoweraction)
    {
        $audit=Audit::where('auditable_type','App\Models\Generator')->where('auditable_id','=','generator')->get();
      return view('generatorpoweractions.show',compact('generator','generatorpoweraction'));
    }

    /*public function reinstall(Request $request)
    {
        
      $data=  $request->validate([
            'site_code'=>'required',
            'pr'=>'required',
            'id'=>'required'
        ]);
        $id=$request['id'];
       
        $generator=Generator::find($id);

        $warehouse = Warehouse::query()->where('id',$generator ->warehouse_id)->first();
    $warehouse->generator=$warehouse->generator -1;
        



    $generator ->warehouse_id=NULL;
    $site=Site::query()->where('site_code',$request->get('site_code'))->first();
  $site->generator=$site->generator+1;
    $mytime = Carbon::now()->toDateString();
    $generator -> installation_date= $mytime;

$generator->site()->associate( $site );



$generatorpoweraction =new Generatorpoweraction();
$x="installation";
$generatorpoweraction['user_id'] =  \Auth::user()->id;
$generatorpoweraction['user_name'] =  \Auth::user()->name;
$generatorpoweraction['generator_id']=$request->get('id');;
$generatorpoweraction['pr']= $request->get('pr');
$poweraction = Poweraction::query()->where('action_name',$x)->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );





$generatorpoweraction->save() ;

      $generator -> update ($data); 
      $site->save();
      $warehouse->save();
      $generator->save();
      return redirect()->route('install');
    }
*/
}
