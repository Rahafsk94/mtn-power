<?php

namespace App\Imports;
use App\Models\Site;

use App\Models\Generator;

use App\Models\Generatorpoweraction;
use App\Models\Poweraction;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use App\Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Validation\Rule;

class GeneratorsImport implements ToModel , WithValidation,WithHeadingRow
{
    use Importable;
    public function model(array $row)
    {
    
        $generator =  new Generator([
            'site_code' => $row['site_code'],

            'generator_name' => $row['generator_name'],
            'generator_capacity' => $row['generator_capacity'],
            'engine_brand' => $row['engine_brand'],
            'owners' => $row['owners'],
            'installation_date' => $row['installation_date'],
           
            'site_code_gen_id' => $row['site_code_gen_id'],
            'runing_hour' => $row['runing_hour'],
            'pr' => $row['pr'],
      
        ]);
        $site = Site::query()->where('site_code', $row['site_code'])->first();
        $generator->site()->associate($site);
        $generator->save();
        $generatorpoweraction =new Generatorpoweraction();
$x="installation";
$generatorpoweraction['user_id'] =  \Auth::user()->id;
$generatorpoweraction['user_name'] =  \Auth::user()->name;
$generatorpoweraction['generator_id']= $generator->id;
$generatorpoweraction['pr']= $row['pr'];
$poweraction = Poweraction::query()->where('action_name',$x)->first();
$generatorpoweraction->poweraction()->associate(  $poweraction );

$generatorpoweraction->save() ;
   
    }

    public function rules(): array
    {
        return [
          
            'site_code' => 'required|exists:sites,site_code',
            'generator_name' => 'required',
             'generator_capacity' =>'required|numeric',
             'engine_brand' =>'required',
             'owners' =>'required',
             'installation_date' =>'required|date',
            
             'site_code_gen_id' =>'required',
             'runing_hour' =>'required|numeric',
             'pr' =>'required|numeric',

       ];
    }
    public function customValidationAttributes()
{
    return ['site_code' => 'Site Code'];
}
public function customValidationMessages()
{
    return [
       'site_code.required' => 'the site code cant be null.',
        //'1.numeric' => 'the site code has already been taken.'
    ];

}
}
