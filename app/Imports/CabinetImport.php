<?php

namespace App\Imports;
use App\Models\Site;
use App\Models\Cabinet;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use App\Models\Generatorpoweraction;
use Illuminate\Support\Str;
use App\Models\Poweraction;

class CabinetImport  implements ToModel , WithValidation,WithHeadingRow
{
    use Importable;
    public function model(array $row)
    {
      $cabinet=
       new Cabinet([
        'site_code' => $row['site_code'],


        'cabinet_name' => $row['cabinet_name'],
        'oracle_code' => $row['oracle_code'],
        'is_used' => $row['is_used'],
        'installation_date' => $row['installation_date'],
        'pr' => $row['pr'],
        ]);
        $site = Site::query()->where('site_code', $row['site_code'])->first();
        $cabinet['number']='1';
        $site->cabinet=  $site->cabinet()->count();
        $site->save();
        $cabinet->site()->associate($site);
        $cabinet['is_used']=Str::ucfirst($row['is_used']);
        $cabinet->cabinet_name =strtoupper($row['cabinet_name']);

       
        $cabinet->save();
        $generatorpoweraction =new Generatorpoweraction();
        $x="installation";
        $generatorpoweraction['user_id'] =  \Auth::user()->id;
        $generatorpoweraction['user_name'] =  \Auth::user()->name;
        $generatorpoweraction['cabinet_id']= $cabinet->id;
        $generatorpoweraction['pr']= $row['pr'];
        $poweraction = Poweraction::query()->where('action_name',$x)->first();
        $generatorpoweraction->poweraction()->associate(  $poweraction );
        
        $generatorpoweraction->save() ;
        
    }



    
    public function rules(): array
    {
        return [
          
            'site_code' => 'required|exists:sites,site_code',
            'cabinet_name' => 'required',
           
             'is_used' =>'required',
             'installation_date' =>'required|date',
             'oracle_code' =>'required|numeric',
        

       ];
    }
    public function customValidationAttributes()
{
    return ['site_code' => 'Site Code'];
}
public function customValidationMessages()
{
    return [
       'site_code.required' => 'the site code cant be null.',
        //'1.numeric' => 'the site code has already been taken.'
    ];

}
}
