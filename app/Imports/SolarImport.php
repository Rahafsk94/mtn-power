<?php

namespace App\Imports;

use App\Models\Solar;
use App\Models\Site;
use App\Models\Generatorpoweraction;
use App\Models\Poweraction;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;
class SolarImport implements ToModel,WithValidation,WithHeadingRow
{
    use Importable;

    public function model(array $row)
    {
       $solar= new Solar([
            'site_code' => $row['site_code'],
            'panel_brand' => $row['panel_brand'],
            'panel_capacity' => $row['panel_capacity'],
            'number_of_panel' => $row['number_of_panel'],
            'number_of_charger' => $row['number_of_charger'],
            'installation_date' => $row['installation_date'],
            'status' => $row['status'],
            'controller' => $row['controller'],
            'solar_project' => $row['solar_project'],
            'po' => $row['po'],
            'pr'=> $row['pr']
        ]);
        $site= Site::query()->where('site_code', $row['site_code'])->first();

        $site->solar= $site->solar + $row['number_of_panel'];

        $solar->site()->associate($site);
        $generatorpoweraction=new Generatorpoweraction();
        $solar->save();
        $site->save();
        $generatorpoweraction['solar_id']= $solar->id;
        $generatorpoweraction['pr']= $row['pr'];
        $poweraction= Poweraction::query()->where('action_name','=','installation')->first();
        $generatorpoweraction['action_id']=$poweraction->id;   
        $generatorpoweraction['user_id'] =  \Auth::user()->id;
        $generatorpoweraction['user_name'] =  \Auth::user()->name;
        $generatorpoweraction->save();


         
    }
    public function rules(): array
    {
        return [
          
            'site_code' =>   ['required','exists:sites,site_code'],
      

            'panel_brand' =>   ['required'],
            'panel_capacity' =>   ['required',  'numeric'], 
            'number_of_panel'=> ['required',  'numeric'], 
            'number_of_charger'=>['required',  'numeric'],
    
            'installation_date'=> ['required','date'],
          
            'status'=>'nullable',
            'controller'=>['required',  'numeric'],
            'solar_project'=>['required'],
            'po'=>['required',  'numeric'],
            'pr'=>['required', 'numeric'], 
           
       ];
    }
public function customValidationAttributes()
{
    return ['site_code' => 'Site Code',
    ];
}
public function customValidationMessages()
{
    return [
        
        'site_code.required'=>'the Site Code cant be null',

    ];

}

}
