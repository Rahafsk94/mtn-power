<?php

namespace App\Imports;

use App\Models\Generator;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

 use App\Carbon\Carbon;
 use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Validation\Rule;

class GeneratorsImport implements ToModel,WithValidation

{
    use Importable;
    public function model(array $row)
    {
        return new Generator([

            'site_id' => $row['0'],
            'generator_name' => $row['1'],
            'generator_capacity' => $row['2'],
            'engine_brand' => $row['3'],
            'owners' => $row['4'],
            'installation_date' => $row['5'],
            'activation_date' => $row['6'],
            'end_date' => $row['7'],
            'site_code_gen_id' => $row['8'],
            'runing_hour' => $row['9'],
      
        ]);
    }

    public function rules(): array
    {
        return [
          
             '0' =>'required|numeric',
             
        ];
    }
    public function customValidationAttributes()
{
    return ['0' => 'site id'];
}
public function customValidationMessages()
{
    return [
        '0.unique' => 'the site code has already been taken.',
    ];

}


}
