<?php

namespace App\Imports;
use  App\Rules\Rahaf;
use App\Models\Tank;
use App\Models\Generator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Site;

class TankImport implements ToCollection,WithHeadingRow
{
    public function collection(Collection $rows)
    {
        $rowIndex='0';

       foreach($rows as $row){
        $rowIndex=$rowIndex + 1;

   
         Validator::make($row->toArray(),
       [

              'site_code' =>['required'],
              'tank_index' => ['required','numeric'],                      
             'tank_sharp' => ['required','numeric'],
             'tank_dimension1' => ['required','numeric'],
             'tank_dimension2' => ['required','numeric'],
             'tank_dimension3' =>['required','numeric'],
             'generator_id' => ['required', function ($attribute, $value, $fail) use($row,$rowIndex) {
             
                $site=Site::where('site_code','=',$row['site_code'])->first();


                $generator = Generator::where('id', '=',$value)->first();
                
                if (!$generator) {
                    $fail('Generator ID is invalid please check the row '.$rowIndex);
                } else {
                    if ($generator->site_id!= $site->id) {
                        $fail('Generator location(Site) does not match tank location, please check the row '.($rowIndex+1));
                    }
                }
            }],
              'installation_date' => ['required','date'],



         ])->validate();
       }

        foreach ($rows as $row) {


           $tank= new Tank([
                'site_code' => $row['site_code'],
                'tank_index' => $row['tank_index'],
                'tank_sharp' => $row['tank_sharp'],
                'tank_dimension1' => $row['tank_dimension1'],
                'tank_dimension2' => $row['tank_dimension2'],
                'tank_dimension3' => $row['tank_dimension3'],
                'tank_diameter' => $row['tank_diameter'],
                'generator_id' => $row[ 'generator_id' ],
                'installation_date' => $row['installation_date'],

            ]);
            $site= Site::query()->where('site_code', $row['site_code'])->first();


            $tank->site()->associate($site);
            $tank->save();
            $tank->save();
        }
    }
}