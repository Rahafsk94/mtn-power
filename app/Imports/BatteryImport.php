<?php

namespace App\Imports;

use App\Models\Battery;
use App\Models\Site;
use App\Models\Cabinet;
use App\Models\Solar;
use App\Models\Generatorpoweraction;
use App\Models\Poweraction;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\Validator;

class BatteryImport implements ToCollection,WithHeadingRow
{

    use Importable;


        public function collection(Collection $rows)
        {
            $rowIndex='0';
    
           foreach($rows as $row){
            $rowIndex=$rowIndex + 1;
    
       

            Validator::make($row->toArray(),
            [
     
                   'site_code' =>['required'],
                   'cabinet_id' => ['numeric','nullable', function($attribute, $value, $fail)use($row,$rowIndex){
                    $site=Site::where('site_code','=',$row['site_code'])->first();
                    $battery_site=$site->id;
                    $cabinet=Cabinet::where('id','=',$value)->first();
                    if( !$cabinet){
                        $fail('Cabinet ID is invalid please check the row '.($rowIndex+1));
                    }
                    else
                   {
                    if($battery_site!=$cabinet->site_id){
                        $fail('Cabinet location(Site) does not match Batteries location, please check the row '.($rowIndex+1));
                    }}
                   }],                      
                  'solar' => ['numeric','nullable',function($attribute, $value, $fail)use($row,$rowIndex){
                    $site=Site::where('site_code','=',$row['site_code'])->first();
                    $battery_site=$site->id;
                    $solar=Solar::where('id','=',$value)->first();
                    if( !$solar){
                        $fail('Solar ID is invalid please check the row '.($rowIndex+1));
                    }
                    else{
                        if($battery_site!=$solar->site_id){
                            $fail('Solar location(Site) does not match Batteries location, please check the row '.($rowIndex+1));
                        }
                    }

                  }],
                  'battery_brand' => ['required'],
                  'capacity' => ['required','numeric'],
                  'numbers' =>['required','numeric'],
                  'pr' => ['required',/* function ($attribute, $value, $fail) use($row,$rowIndex) {
                  
                     $site=Site::where('site_code','=',$row['site_code'])->first();
     
     
                     $generator = Generator::where('id', '=',$value)->first();
                     
                     if (!$generator) {
                         $fail('Generator ID is invalid please check the row '.$rowIndex);
                     } else {
                         if ($generator->site_id!= $site->id) {
                             $fail('Generator location does not match tank location, please check the row '.($rowIndex+1));
                         }
                     }
                 }*/],
                 'po' => ['required','numeric'],
                 'status' => ['required'],
                 'oracle_code' => ['required','numeric'],
               
                   'installation_date' => ['required','date'],
     
     
     
              ])->validate();


          


        
        $battery = new Battery([


            'site_code' => $row['site_code'],
            'cabinet_id' => $row['cabinet_id'],
            'solar' => $row['solar'],
            'battery_brand' => $row['battery_brand'],
          
            'capacity' => $row['capacity'],
            'numbers' => $row['numbers'],
            'pr' => $row['pr'],
            'po' => $row['po'],
            'status' => $row['status'],
            'installation_date' => $row['installation_date'],
            'oracle_code' => $row['oracle_code'],
   
            
        ]);

        $site = Site::query()->where('site_code', $row['site_code'])->first();
        $battery->site()->associate($site);
        $battery->save();
        $site->battery=$site->battery+ $row['numbers'] ;
        $site->save();
        $generatorpoweraction =new Generatorpoweraction();
        $x="installation";
        $generatorpoweraction['user_id'] =  \Auth::user()->id;
        $generatorpoweraction['user_name'] =  \Auth::user()->name;
        $generatorpoweraction['battery_id']= $battery->id;
        $generatorpoweraction['pr']= $row['pr'];
        $poweraction = Poweraction::query()->where('action_name',$x)->first();
        $generatorpoweraction->poweraction()->associate(  $poweraction );
        
        $generatorpoweraction->save() ;

    }
    

}

}
