<?php

namespace App\Imports;

use App\Models\Ampere;
use App\Models\Site;
use Maatwebsite\Excel\Concerns\ToModel;


use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
class AmpereImport implements ToModel,WithValidation , WithHeadingRow
{

    use Importable;

    
    public function model(array $row)
    {
         $ampere= new Ampere([
            'site code'=> $row['site_code'],
            'ampere_capacity'=> $row['ampere_capacity'],
            'ampere_provider'=> $row['ampere_provider'],
            'payment_method'=> $row['payment_method'],
            'payment_type'=> $row['payment_type'],
            'agreed_rh'=> $row['agreed_rh'],
            'nots'=> $row['nots'],
            'ampere_capacity'=> $row['ampere_capacity'],
            'installation_date'=> $row['installation_date'],
  
            
        ]);

     
  $site = Site::query()->where('site_code',$row['site_code'])->first();
  $ampere->site()->associate( $site );
$ampere->save();
    }
    public function rules(): array
    {
        return [
          
            'site_code' => 'required|exists:sites,site_code',
            'ampere_provider' => 'required',
             'payment_method' =>'required',
             'payment_type' =>'required',
             'agreed_rh' =>'required|numeric',
             'nots',
             'installation_date' =>'required|date',
           
      
         

       ];
    }
    public function customValidationAttributes()
    {
        return ['site_code' => 'Site Code',
        'agreed_rh'=>'Agreed RH'
    ];
    }
    public function customValidationMessages()
    {
        return [
           'site_code.required' => 'the site code cant be null.',
            //'1.numeric' => 'the site code has already been taken.'
        ];
    
    }
}
