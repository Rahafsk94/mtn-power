<?php

namespace App\Imports;

use App\Models\Site;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

 use App\Carbon\Carbon;
 use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Illuminate\Validation\Rule;
use App\Rules\Sitecodename;
class SitesImport implements ToModel,WithValidation,WithHeadingRow

{

    use Importable;


    public function model(array $row)
    {
       $site= new Site([
            'site_code' => $row['site_code'],
            'finance_code' => $row['finance_code'],
            'site_name' => $row['site_name'],
            'priority' => $row['priority'],
            'area' => $row['area'],
            'location' => $row['location'],
            'sub_location' => $row['sub_location'],
            'oracle_location' => $row['oracle_location'],
            'province' => $row['province'],
            'tx_category' => $row['tx_category'],
             'installation_date'=> $row['installation_date'],
            'band'=> $row['band'],
            'zone' => $row['zone'],
            'c_r' => $row['c_r'],
            'supplier' => $row['supplier'],
            'coordinates_e' => $row['coordinates_e'],
            'coordinates_n' => $row['coordinates_n'],
            'rationning_hours' => $row['rationning_hours'],
            'on_off_air' => $row['on_off_air'],
        ]);
        $site->site_code =strtoupper($row['site_code']);
        $site->save();
    }
    
    
    public function rules(): array
    {
        return [
          
            'site_code' =>   ['required',  Rule::unique('sites', 'site_code'),new Sitecodename()],
      
            'finance_code' =>   ['required',  Rule::unique('sites', 'finance_code')],
            'site_name' =>   ['required'],
            'priority' =>   ['required',  'numeric'], 
            'area'=> ['required'], 
            'location'=>['required'],
            'sub_location'=>['required'],
            'oracle_location'=>['required'],
            'province'=>['required'],
            'tx_category'=>['required'],
            'installation_date'=>['required','date'],
          
            'band'=>['required'],
            'zone'=>['required'],
            'c_r'=>['required'],
            'supplier'=>['required'],
            'coordinates_e'=>['required'],
            'coordinates_n'=>['required'],
            'rationning_hours'=>['required','numeric'],
            'on_off_air'=>['required'],
       ];
    }
public function customValidationAttributes()
{
    return ['site_code' => 'Site Code',
    'finance_code' => 'Finance Code'];
}
public function customValidationMessages()
{
    return [
        'site_code.unique'=> 'the Site Code has  been taken.',
        'site_code.required'=>'the Site Code cant be null',
        'finance_code.unique' => 'the Finance Code has  alredy been taken.',
        'finance_code.required'=>'the Finance Code cant be null',
    ];

}

   
    }
    
  



