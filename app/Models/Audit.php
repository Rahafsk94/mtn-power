<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Site;
use App\Models\Solar;
use App\Models\Generator;
use App\Casts\Old_value;

class Audit extends Model
{

    public function auditable(){
        return $this->morphTo();
    }
    
 
    use HasFactory;
    
   

  
    protected $casts = [
        'old_values' =>'array',
        'new_values' =>'array',

    ]; 



    public function user()
    {
        return $this->belongsTo(User::class);
    }
 
    public function site()
    {
        return $this->belongsTo(Site::class);
    }
   
   
    public function solar()
    {
        return $this->belongsTo(Solar::class);
    }
   
}
