<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Site;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Ampere extends Model implements AuditableContract
{
    use \OwenIt\Auditing\Auditable;
    public function site()
    {
        return $this->belongsTo(Site::class);
    }


    protected $table = 'amperes';
    use HasFactory;

    public $timestamps = false;
    protected $fillable =['site_id','ampere_provider','ampere_capacity','payment_method','payment_type','agreed_rh','nots','installation_date'];


    public function __construct(array $attributes = [])
    {        
    
        $this->created([$this, 'onCreated']);

        


        parent::__construct($attributes);
    }


  
    public function onCreated(\App\Models\Ampere $amperes)
    {
        $amperes->site->editPowerAmpere();
        $amperes->site->save();

    }


    public function notActive( $ampere){
$ampere->site->updatePowerAmpere();
$ampere->site->save();
    }
}

