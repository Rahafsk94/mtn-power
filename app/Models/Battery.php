<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Site;
use App\Models\Warehouse;
use App\Models\Solar;
use App\Models\Cabinet;
use App\Models\Generatorpoweraction;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Battery extends Model implements AuditableContract
{

    use \OwenIt\Auditing\Auditable;
    use HasFactory;
    protected $casts = [
        'installation_date' => 'date',

    ];
  
    protected $table ="batteries";
    protected $fillable =['site_id','solar','cabinet_id','battery_brand','capacity','numbers','status','oracle_code','installation_date','shipment_date','po','is_active','solar','dismantle_date'];

 


    public function __construct(array $attributes = [])
    {        

      //  $this->created([$this, 'onCreated']);
        $this->deleted([$this, 'onDeleted']);
        


        parent::__construct($attributes);
    }




    public function solar()
    {
        return $this->belongsTo(Solar::class);
    }
    

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
    public function cabinet()
    {
        return $this->belongsTo(Cabinet::class);
    }
 
  
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
  
   /* public function onDeleted(\App\Models\Battery $batteries)
    {
        if($batteries->site){
        $batteries->site->updatePowerSolutions();
$batteries->site->save();}

    }*/
  
  
  
 /* 
  public function onCreated(\App\Models\Battery $batteries)
    {
        if($batteries->site){
         $x=$batteries->numbers;

 
  $batteries->site->updatePowerSolutions($x);

        }
    }*/
    public function generatorpoweraction(  )  
    {
        return $this->hasMany(Generatorpoweraction::class);
    }
}