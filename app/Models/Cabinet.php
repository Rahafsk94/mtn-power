<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Battery;
use App\Models\Warehouse;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Audit;
class Cabinet extends Model implements AuditableContract
{
    use HasFactory;


    use Auditable;

    protected $fillable =['cabinet_name','number','is_used','oracle_code','installation_date','is_active','shipment_date','dismantle_date'];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
        public function sitecabinet()  
        {
            return $this->hasMany(Sitecabinets::class, 'cabinet_id', 'id');
        }
 public function battery()  
        {
            return $this->hasMany(Battery::class, 'cabinet_id', 'id');
        }

        public function generatorpoweraction( )  
        {
            return $this->hasMany(Generatorpoweraction::class);
        }
    
      
    }