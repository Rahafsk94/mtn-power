<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Traits\HasPermissions;
use OwenIt\Auditing\Auditable;

use App\Models\Audit;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class User extends Authenticatable implements AuditableContract 
{

 


    use Auditable;

    use HasApiTokens, HasFactory, Notifiable,HasRoles,HasPermissions; 


    protected $fillable = [
        'name',
        'email',
        'password',

    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];

   
    protected $casts = [
        'email_verified_at' => 'datetime',
        'roles_name'=>'array'
    ];


    public function generators(){
        return $this->hasMany(User::class,'user_id','id');
    }
    public function audit(){
        return $this->hasMany(Audit::class);
    }

 

}
