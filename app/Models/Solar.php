<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Battery;
use App\Models\Warehouse;
use App\Models\Site;
use App\Models\Audit;
use App\Models\Generatorpoweraction;
use OwenIt\Auditing\Auditable;




use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Solar extends Model implements AuditableContract
{
  use \OwenIt\Auditing\Auditable;
    use HasFactory;
  protected $fillable=['po','installation_date','dismantle_date','site_id','shipment_date','controller','controller_brand','solar_project','panel_brand','panel_capacity','	number_of_panel','number_of_charger','number_of_panel'];
  
    public function battery(){
        return $this->hasMany(Battery::class, 'solar','id');
    }
    public function site(){
      return $this->belongsTo(Site::class);
    }


    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
      }
   /* public function __construct(array $attributes = [])
    {        
    
        $this->created([$this, 'onCreated']);
        $this->deleted([$this, 'onDeleted']);
        
        parent::__construct($attributes);
    }*/
  /*  public function onCreated(\App\Models\Solar $solar)
    {
      if($solar->site){
        $solar->site->updatePowerSolar();
        $solar->site->save();
      }
    }*/
   
    public function generatorpoweraction(  )  
    {
        return $this->hasMany(Generatorpoweraction::class);
    }
}
