<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tank;

class Generatortank extends Model
{
    protected $casts = [
        'installation_date' => 'date'
    ];
    public function tank()
    {
        return $this->belongsTo(Tank::class);
    }

    use HasFactory;

    protected $fillable=['tank_id','generator_id','installation_date','end_date'];
      
    
    
   

}
