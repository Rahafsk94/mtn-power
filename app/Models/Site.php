<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Generator;
use App\Models\Ampere;
use App\Models\Battery;
use App\Models\Audit;
use App\Models\Tank;
use App\Models\Solar;
use App\Models\Cabinet;

use Carbon\Carbon;

use OwenIt\Auditing\Auditable;




use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use DB;
class Site extends Model implements AuditableContract
{
    use \OwenIt\Auditing\Auditable;
    protected $table ="sites";
    protected $casts = [
        'installation_date' => 'date',
        'end_date' => 'date'
    ];
  
    public $timestamps = false;
    use HasFactory;
    

    protected $fillable =['site_code','finance_code','site_name','priority','area',
    'location','sub_location','oracle_location','province','tx_category','installation_date','end_date','band','zone','c_r','supplier','coordinates_e','coordinates_n','rationning_hours','generator','on_off_air'];
 

        public function generator()  
        {
            return $this->hasMany(Generator::class, 'site_id', 'id');
        }
      
        public function ampere()
        {
            return $this->hasMany(Ampere::class, 'site_id', 'id');
        }
        
        public function tank()
        {
            return $this->hasMany(Tank::class, 'site_id', 'id');
        }
        
        public function battery()  
        {
            return $this->hasMany(Battery::class, 'site_id', 'id');
        }
        
        public function cabinet()  
        {
            return $this->hasMany(Cabinet::class, 'site_id', 'id');
        }
      
        public function audit()
        {
            return $this->morphToMany(Audit::class,'auditable');
        }
       public function solar(){
        return $this->hasMany(Solar::class,'site_id','id');
       }
      

   /* public function hasGenerator(\App\Models\Site $generators){
        $count=DB::table('generators')->count();
if( $count>0){

$generators->setAttribute('priority', '1');
}

}*/





public function updatePowerSolution(){
  
    $count=$this->generator()->whereNull('end_date')->where('is_active','=','1')->count();
    
       
        $this->setAttribute('generator',$count );

    }



    
/*public function updatePowerSolutions($x){
    $this->battery =   $x + $this->battery -$x;
  $this->save();


    }*/


    public function updatePowerAmpere()
    {

        $count=$this->ampere()->whereNull('end_date')->where('is_active','=','1')->count() -1;
        

            $this->setAttribute('ampere',$count );

        }

        public function editPowerAmpere()
        {
    
            $count=$this->ampere()->whereNull('end_date')->where('is_active','=','1')->count() ;
            
    
                $this->setAttribute('ampere',$count );
    
            }
    
 

public function updatePowerSolar(){
  
    $count=$this->solar()->count();
    
       
        $this->setAttribute('solar',$count );

    }   
    public function updatePowerTank(){


        $count=$this->tank()->whereNotNull('site_id')->where('is_active','=','1')->count();
        
           
            $this->setAttribute('tank',$count );
    
        }

}


