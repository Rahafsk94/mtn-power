<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Generatorpoweraction;

class Poweraction extends Model
{
    use HasFactory;
    public function generatorpoweraction( )  
    {
        return $this->hasMany(Generatorpoweraction::class);
    }
    
}
