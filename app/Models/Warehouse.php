<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Generator;
use App\Models\Battery;
use App\Models\Solar;
use App\Models\Cabinet;
class Warehouse extends Model
{
    use HasFactory;
    protected $table ="warehouses";
    protected $fillable=['name','owner','location'];

    public function generator(){

        return $this->hasMany(Generator::class,'warehouse_id','id');
    }
    public function battery(){

        return $this->hasMany(Battery::class,'warehouse_id','id');
    }
    public function solar(){

        return $this->hasMany(Solar::class,'warehouse_id','id');
    }
    public function cabinet(){

        return $this->hasMany(Cabinet::class,'warehouse_id','id');
    }
    
}
