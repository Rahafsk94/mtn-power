<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Cabinet;
use App\Models\Site;

class Sitecabinets extends Model
{
    use HasFactory;
    protected $fillable =['site_id','cabinet_id','installation_date','numbers','oracle_code'];

      
    public function cabinet()
    {
        return $this->belongsTo(Cabinet::class);
    }

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
