<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Generator;
use App\Models\Battery;
use App\Models\Solar;
use App\Models\Poweraction;
class Generatorpoweraction extends Model
{
    use HasFactory;

    protected $fillable =['generator_id','pr','rm','trx','battery_id','user_name','action_id','cabinet_id'];


    public function generator()
    {
        return $this->belongsTo(Generator::class,'generator_id','id');
        
    }
    public function poweraction()
    {
        return $this->belongsTo(Poweraction::class,'action_id','id');
    }

    
    public function battery()
    {
        return $this->belongsTo(Battery::class,'battery_id','id');
        
    }
    public function solar()
    {
        return $this->belongsTo(Solar::class,'solar_id','id');
        
    }
      
    public function cabinet()
    {
        return $this->belongsTo(Cabinet::class,'cabinet_id','id');
        
    }
}
