<?php
namespace App\Models;
use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Generatorpoweraction;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use App\Models\Site;
use App\Models\Audit;
use App\Models\Tank;
use App\Models\Warehouse;

use App\Models\Poweraction;
class Generator extends Model implements AuditableContract

{
    use Auditable;
    protected $casts = [
        'installation_date' => 'date',
        'end_date' => 'date',
        'shipment_date' => 'date'
    ];
  
    public $timestamps = false;

    public function __construct(array $attributes = [])
    {        
        $this->creating([$this, 'onCreating']);
        $this->created([$this, 'onCreated']);
    // $this->updated([$this, 'onUpdated']);
     //  $this->updating([$this, 'onUpdating']);
        $this->deleted([$this, 'onDeleted']);
        


        parent::__construct($attributes);
    }
    public function onDeleted(\App\Models\Generator $generators){
        $generators->site->updatePowerSolution();
        $generators->site->save();
    }
    

    public function warehouse(){

        return $this->belongsTo(Warehouse::class);
    }
    public function onCreated(\App\Models\Generator $generators)
    {
        if (!\Auth::user()->id) {
            return false;
        }
        if( $generators->site){
      $generators->site->updatePowerSolution();
        $generators->site->save();
       
        }

   
    }
    


    public function onCreating(\App\Models\Generator $generators)
    {
        // Placeholder for catching any exceptions
        if (!\Auth::user()->id) {
            return false;
        }

        $generators->setAttribute('user_id', \Auth::user()->id);
    if( $generators->site) {     
           $generators->site->save();
        }

       
    //    $generators->setAttribute('generator_name', 'MTN');


    }
   


    use HasFactory;

    protected $table ="generators";
    protected $fillable =['site_id','updated_by','generator_name','generator_capacity','engine_brand','owners','installation_date','shipment_date','end_date','site_code_gen_id','runing_hour','warehouse_id','oracle_code','is_active','dismantle_date'];



    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    

  
    public function generatorpoweraction( )  
    {
        return $this->hasMany(Generatorpoweraction::class);
    }

    

    public function tanks(){
        
        return $this->belongsToMany(Tank::class,'generatortanks');
    }

    public function generatortank()  
    {
        return $this->hasMany(Generatortank::class, 'generator_id', 'id');
    }

}


