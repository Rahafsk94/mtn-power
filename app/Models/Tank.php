<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Generator;
use App\Models\Generatortank;
use App\Models\Site;
use OwenIt\Auditing\Auditable;

use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
class Tank extends Model implements AuditableContract
{
    use Auditable;
 
    use HasFactory;
    protected $table ="tanks";
    protected $fillable=['site_id','tank_index','tank_sharp','tank_dimension1','tank_dimension2','tank_dimension3','tank_diameter'];


    protected $casts = [
        'installation_date' => 'date',
        'end_date' => 'date',
    ];


    public function generatortank()  
        {
            return $this->hasMany(Generatortank::class, 'tank_id', 'id');
        }
    public function generators(){
        return $this->belongsToMany(Generator::class,'generatortanks');
    }
    public function site()
    {
        return $this->belongsTo(Site::class);
    }




    public function __construct(array $attributes = [])
    {        
    
        $this->created([$this, 'onCreated']);
   
       
        


        parent::__construct($attributes);
    }


  
    public function onCreated(\App\Models\Tank $tank)
    {
        $tank->site->updatePowerTank();
        $tank->site->save();

    }
   
   
    

}
