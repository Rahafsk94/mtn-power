<?php

namespace App\Rules;
use  App\models\Generator;
use  App\models\Site;
use Illuminate\Contracts\Validation\Rule;

class GeneratorWithTank implements Rule
{
 
    public function __construct()
    {
        
    }

  
    public function passes($attribute, $value)
    {

        $generator=Generator::find($value);
      

     return !is_null($generator->site_id);

     
    //return $generator->is_active =='1';
    }


 
    public function message()
    {
        return 'The Generator not on site.';
    }
}
