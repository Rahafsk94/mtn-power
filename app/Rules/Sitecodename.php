<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class Sitecodename implements Rule
{
    
    public function __construct()
    {
        //
    }

  
    public function passes($attribute, $value)

    {
       
        $sName=['ALP','DMR','DAM','DRA','DRZ','HMA','HMS','HSK','IDB','LTK','SWD','TRS'];
       $x=strtoupper(substr( $value,0,3));
  
  return Str::startsWith($x,$sName);
    }

    public function message()
    {
        return 'The site code is incorrect.';
    }
}
