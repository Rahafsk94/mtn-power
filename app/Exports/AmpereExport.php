<?php

namespace App\Exports;

use App\Models\Ampere;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;



class AmpereExport implements FromView
{


    public function view(): View
    {
        $ampere=Ampere::get();
        return view('ampere.export',compact('ampere')
       
        );
    }
}