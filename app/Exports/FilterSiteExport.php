<?php

namespace App\Exports;

use App\Models\Site;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;




class FilterSiteExport implements  FromView

    {
        public function view(): View
        {
            return view('sites.search', [
                'site' => Site::all()
            ]);
        }
}
