<?php

namespace App\Exports;

use App\Models\Generator;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class GeneratorsearchExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $searchs = $request->input('searchs');
              $generators = Generator::query()->whereHas('site',function($query) use ($searchs){
        return $query->where('site_code','LIKE', "{$searchs}");

      })->get();
            return view('generators.search', compact('generators'));
    }
}
