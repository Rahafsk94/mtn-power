<?php


namespace App\Exports;

use App\Models\Site;
use Maatwebsite\Excel\Concerns\FromCollection;


use Maatwebsite\Excel\Concerns\WithHeadings;

 
class SiteExport implements FromCollection,WithHeadings
{
   

    protected $casts = [
        'installation_date' => 'dd-mm-yyyy',
        'end_date' => 'date'
    ];

    public function headings():array{
        return[
            'id',
            'site_code',
            'finance_code',
            'site_name',
            'priority',
            'area',
            'location' ,
            'sub_location',
            'oracle_location',
            'province' ,
            'tx_category',
            'installation_date',
            'end_date' ,
            'is_active',
            'band',
            'zone',
            'c_r',
            'supplier',
            'coordinates_e',
            'coordinates_n',
            'rationning_hours',
            'on_off_air',
            'generator',
            'battery',
            'ampere',
            'solar',
            'tank',
            'cabinet'

        ];
    } 
    public function collection()
    {
        return Site::all()->where('is_active','=','1');
    }
}
