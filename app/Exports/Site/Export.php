<?php

namespace App\Exports\Site;

use App\Models\Site;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;

 
class StudentExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */ 
    public function headings():array{
        return[
            'site_code',
            'finance_code',
            'site_name',
            'priority',
            'area',
            'location' ,
            'sub_location',
            'oracle_location',
            'province' ,
            'tx_category',
            'installation_date',
            'end_date' ,
            'band',
            'zone',
            'c_r',
            'supplier',
            'coordinates_e',
            'coordinates_n',
            'rationning_hours',
            'on_off_air',
            'generator',
            'battery',
            'ampere',
            'solar',
            'tank',
            'cabinet'

            

        ];
    } 
    public function collection()
    {
        return Site::all();
    }
}
