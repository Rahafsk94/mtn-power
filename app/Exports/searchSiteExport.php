<?php

namespace App\Exports;

use App\Models\Site;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;

use Maatwebsite\Excel\Concerns\FromCollection;


use Maatwebsite\Excel\Concerns\WithHeadings;


class searchSiteExport implements FromCollection,WithHeadings
{


    
    
        use Exportable;
        public function __construct(String $sitesearch = null )
        {
            $this->sitesearch = $sitesearch;
        
        }

        public function headings():array{
            return[
                'id',
                'site_code',
                'finance_code',
                'site_name',
                'priority',
                'area',
                'location' ,
                'sub_location',
                'oracle_location',
                'province' ,
                'tx_category',
                'installation_date',
                'end_date' ,
                'band',
                'zone',
                'c_r',
                'supplier',
                'coordinates_e',
                'coordinates_n'
                
    
            ];
        } 
        public function collection()
        {
            return Site::query()
        ->where('site_code', 'LIKE', "{$this->sitesearch }")->get();
        }
    }

    

