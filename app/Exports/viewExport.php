<?php

namespace App\Exports;

use App\Models\Site;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class viewExport implements FromView
{

    public function __construct(String $sitesearch = null )
        {
            $this->sitesearch = $sitesearch;
        
        }

    public function view(): View
    {
        return view('test', [
            'site' =>Site::query()
            ->where('site_code', 'LIKE', "{$this->sitesearch}")->get()
        ]);
    }
}
