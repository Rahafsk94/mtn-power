<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->

    <title>
        @yield('title')
    </title>

    <!-- Scripts -->
    <!-- logout drop menu-->
   <!--   <script src="{{ asset('js/app.js') }}" defer></script>--> 
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css'> 
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    



 <!-- datatable -->
 
 <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
    <!--  <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>-->
  
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/datatables.min.css"/>
 
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.12.1/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/datatables.min.js"></script>
 <!--  import generator-->

 <meta name="csrf-token" content="{{ csrf_token() }}">
 
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


 <!-- side bar -->
 
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <!--  import site style-->

 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">




   <style>

.btn-group{

    display:flex;
    justify-content:space-around ;
}


#add_fields_placeholderValue {
    display: none;
}

#hideMe {
            -webkit-animation: seconds 1.0s forwards;
            -webkit-animation-iteration-count: 1;
            -webkit-animation-delay: 3s;
            animation: seconds 1.0s forwards;
            animation-iteration-count: 1;
            animation-delay: 3s;
            position: relative;
        }
        @-webkit-keyframes seconds {
            0% {
                opacity: 1;
            }
            100% {
                opacity: 0;
                left: -9999px;
                position: absolute;
            }
        }
        @keyframes seconds {
            0% {
                opacity: 1;
            }
            100% {
                opacity: 0;
                left: -9999px;
                position: absolute;
            }
        }

    .dropbtn {
  background-color: #F9D401;
  color: white;
  padding: 14px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

.btn.btn-link{
    color:#F9D401;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    padding-top: 4px;
  position: relative;
  display: inline-block;
}


/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    color: #00678f;
    font-family: cursive;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
  display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
  background-color: #F9D401;
  
}


.fa.fa-eye{
    margin-right:7px;
}

.bi.bi-trash {
  background: transparent;
  border: none;
  border-radius: 2em;
  color: #f00;
  display: inline-block;
  height: 20px;
  line-height: 2px;
  margin: 5px;
  padding: 0;
 

}
.columnn{
  float: left;
  width: 33.33%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

p{
    color: #00678f;
    font-family: cursive;
    font-size: 2ch;
}
h10{
    color: #555;
    font-family: cursive;
    font-size: 2ch;
}
.card{

    overflow-y: scroll;
margin :30px;
margin-top:5px;
}
.card-header{
    color: #F9D401;
  
    
}
.navbar {
    height: 80px;
    background-color: #F9D401;
    border: 1px solid red;
}

.navbar-nav.ms-auto{
    
    padding-right: 80px;
 
 }

 .btn.btn-success{
margin-left: 5px;
height: 50px;
    background-color:#F9D401;
    font-size: 18px;
     border-color: #F9D401;
     
}

.data-table{
    margin:3px;
}

.raw{
padding-left: 400px;
padding-right: 400px;
}
.column {
    float: left;

padding: 10px;

width: 50%;

}
.column2{
    float: left;
    padding: 10px;
padding-top: 80px;

width: 32%;

}

.button {
  background-color:#00678f;
  border: none;
  color: white;
width: 100px;
height: 50px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  padding: 4px;
  margin: 11px;
}


.row.mb-3{
    color: #F9D401;
}

.btn.btn-primary{
    background-color: #F9D401;
    border: #F9D401;
}
.btn.btn-link{
    color:#F9D401;
}
.form-control:focus{
    border-color:#F9D401;
    box-shadow: 0 0 0 0.2rem #F9D401;
}

.i{
    padding: 70px;
}


.header{
    text-align: center;
    margin :20px;
    color: #00678f;
}


.cont{
    margin:9px;
}


.content {
 
 max-height: 0;
 overflow: hidden;

}
/* side bar */

.sidebar-header {
    padding: 20px;

    background:   #F9D401;
    height: 80px;
}
.t li{
   
    padding-top:10px;
}
.t ul{
   
   padding-left:47px;
}
.close{
    background:   rgb(250,218,94);
 
  
}
/*الكارد ل صفحة تعديل الرول */
.roles-card{
    padding: 80px;
    padding-left:  500px;
width:1000px;
}
.center {
    display: flex;
    align-items: center;
    justify-content: center;
}

.center a {
    margin: 0 5px;
    padding: 0px;
}
   </style>

</head>
<body>
<div class="w3-sidebar w3-bar-block w3-card w3-animate-left" style="display:none" id="mySidebar">
<div class="close">
  <button class="w3-bar-item w3-button w3-large" 
  onclick="w3_close()"> &times;</button>
  </div>
  
  <div class="sidebar-header">
          <h3>    Dashboard</h3>
          
      </div>
    
 
  <a href="/home" class="w3-bar-item w3-button">  <i class="fa fa-fw fa-home"></i> Home</a>
  @can('import')
  <a href="{{route('import-file')}}" class="w3-bar-item w3-button">  <i class="fa fa-download" style='font-size:18px'> import</i> </a>
@endcan
  <a href="/sites" class="w3-bar-item w3-button">   <i class="material-icons" style="font-size:20px">router</i> Sites</a>

  <a href="/warehouses" class="w3-bar-item w3-button">    <i class="material-icons" style="font-size:20px">store</i> Warehouses</a>
  
  <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="w3-bar-item w3-button"><i class="material-icons"style="font-size:15px;">format_color_text</i> Actions</a>
              <ul class="collapse list-unstyled" id="homeSubmenu1">
                <div class="t">
                    <ul>
                  <li>
                      <a href="/poweractions">  generator Actions</a>
                  </li>
                  <li>
                      <a href="{{ route('batteries.index') }}">  Batteries</a>
                  </li>
                  <li>
                      <a href="{{ route('amperes.index') }}">Amperes</a>
                  </li>
                  <li>
                      <a href="tanks">Tanks</a>
                  </li>
                  </ul>
                  </div>
              </ul>













  <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="w3-bar-item w3-button"><i class="bi bi-power red-color"></i> Power</a>
              <ul class="collapse list-unstyled" id="homeSubmenu">
                <div class="t">
                    <ul>
                  <li>
                      <a href="/generators">  Generators</a>
                  </li>
                  <li>
                      <a href="{{ route('batteries.index') }}">  Batteries</a>
                  </li>
                  <li>
                      <a href="amperes">Amperes</a>
                  </li>
                  <li>
                      <a href="tanks">Tanks</a>
                  </li>
                  </ul>
                  </div>
              </ul>
              
              <a href="/powersolution" class="w3-bar-item w3-button">   <img src="/led.png" height="18"> power solutions</a>
@role('Management')

  <div class="w3-dropdown-hover">
    <button class="w3-button">Authentication
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="w3-dropdown-content w3-bar-block">
    <a class="nav-link" href="{{ route('users.index') }}"><i class="fa fa-fw fa-user"></i><span> Users</span></a>
    <a class="nav-link" href="{{('roles') }}"><i class="fa fa-key"></i><span> Roles</span></a>
    <a class="nav-link" href="{{('permission') }}"><i class="material-icons" style="font-size:14px">directions_run</i>Permissions</span></a></li>
    </div>
  </div> 
  
  @endrole

  </div> 
  
<div id="main">

<div class="w3-teal">

 
</div>

<nav class="navbar navbar-expand-lg ">

<button id="openNav"  style='background-color: #F9D401;border: none' onclick="w3_open()">&#9776; </button>
        <img src="/mtn.png" alt="MTN" width="120" height="65">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

     
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="btn btn-success" href="/home"><p>Home</p></a>
              

                
           <!--   <div class="dropdown">
  <button class="dropbtn"> <p>Import</p> </button>
  <div class="dropdown-content">
  <a href="siteimport"> <h6>Site Import</h6> </a>
  <a href="generatorimport"> <h6>generator import </h6> </a>
  <a href="ampereimport"> <h6>ampere import </h6> </a>
  <a href="batteryimport"> <h6>battery import </h6> </a>
  <a href="{{route('solarimport')}}"> <h6>Solar import </h6> </a>
  <a href="{{route('tanksimport')}}"> <h6>Tank import </h6> </a>
  <a href="{{route('cabinetimport')}}"> <h6>Cabinet import </h6> </a>
  </div>
  -->
        </div> 

        </li>         
               

            </ul>



                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>









                                    
                                </li>
                            @endif

                  
                        @else




                        <li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link " href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <i class="fa fa-bell"></i>
        <span class="badge badge-light bg-success badge-xs">{{auth()->user()->unreadNotifications->count()}}</span>
    </a>
    <ul class="dropdown-menu" >
                @if (auth()->user()->unreadNotifications)
                <li class="d-flex justify-content-end mx-1 my-2">
                <a href="{{route('mark-as-read')}}" class="btn btn-success btn-sm">Mark All as Read</a>
                </li>
                @endif

                @foreach (auth()->user()->unreadNotifications as $notification)
                @foreach ( $notification->data as $d)
                <a href="{{route('reset.password.get',$d) }}" class="text-success"><li class="p-1 text-success"> {{auth()->user()->name}}  Reset Password</li></a>
                @endforeach
                @endforeach
           
              
    </ul>
</li>



                        
                      
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                 {{ Auth::user()->name }}
                                </a>
                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('lo') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                  
                                    <form id="logout-form" action="{{ route('lo') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                    <a class="dropdown-item" href="{{route('editpassword',Auth::user()->id)}}">
                                    Change Password
                                    </a>
                                  
                                </div>
                            </li>

                            
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            
            @yield('content')
        
        </main>
    </div>
</body>


<script>
function w3_open() {
  document.getElementById("main").style.marginLeft = "25%";
  document.getElementById("mySidebar").style.width = "25%";
  document.getElementById("mySidebar").style.display = "block";
  document.getElementById("openNav").style.display = 'none';
}
function w3_close() {
  document.getElementById("main").style.marginLeft = "0%";
  document.getElementById("mySidebar").style.display = "none";
  document.getElementById("openNav").style.display = "inline-block";
}
</script>

</html>


















