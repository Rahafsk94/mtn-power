@extends('layouts.lay')
@section('content')
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="colorlib.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
  </head>
  <body>
    <div class="s010">
      <form>
        <div class="inner-form">
          <div class="basic-search">
            <div class="input-field">
              <input id="search" type="text" placeholder="Type Keywords" />

            </div>
          </div>
          <div class="advance-search">

            <span class="desc">Search by Running Hour</span>
            <form action="{{ route('yy') }}" method="GET">
            <div class="row">
                <input type="text" name="x" placeholder="Less than.." required/>
            </div>

            <div class="row second">
                <input type="text" name="y" placeholder="More than" required/>
            </div>
            <div class="row third">
              <div class="input-field">
                <div class="result-count">
                </div>
                <div class="group-btn">

                  <button class="btn-search" type="submit">SEARCH</button>
                </div>
              </div>
            </div>

          </div>
        </div>

    </div>

</html>


@endsection
