@extends('layouts.app')
@section('content')
<form action="{{route('name',$audit)}}" method="GET">
    @csrf
    <div class="container">
    <table class="table table-bordered">
        <tr>
          <th scope="col">Site Code</th>
          <th scope="col">Finance Code</th>
          <th scope="col">Site name</th>
          <th scope="col">Priority</th>
          <th scope="col">Area</th>
          <th scope="col">Location</th>
          <th scope="col">Sub location</th>
          <th scope="col">Oracle Location</th>
          <th scope="col">province</th>
          <th scope="col">TX Category</th>
          <th scope="col">Installation date</th>
          <th scope="col">End date</th>
          <th scope="col">Band</th>
          <th scope="col">Zone</th>

          <th scope="col">C_R</th>
          <th scope="col">Supplier</th>
          <th scope="col">Coordinates_E</th>
          <th scope="col">Coordinates_N</th>
        </tr>
        <tr>
               
                <td style="text-align:center">{{$audit->auditable_id}}</td>
         
<td style="text-align:center">{{$audit->event}}</td>

<td style="text-align:center">{{$audit->user->name}}</td>

<td style="text-align:center">

  @foreach ( $audit->old_values as $key=>$value)
  <p>{{$key}}</p><li>{{$value}} </li> 
  @endforeach
  </div> 
</td>

<td style="text-align:center">

  @foreach ( $audit->new_values as $key=>$value)
  <p>{{$key}}</p><li>{{$value}} </li> 
  @endforeach
  </div> 
</td>
</tr>

            </tr>
                </div>

                </form>
@endsection
