@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif

<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                         

</div>
                        </h4>
               
                        <h6>
                
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator id</span> </th>
  
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator name</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action name</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action time</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Craeted by mame</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Old Site</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >New Site</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >TRX</span> </th>
         </tr>
        <tbody>
            @foreach ($reshufflings as $reshuffling)
          
          
            <tr>
            <td style="text-align:center">{{$reshuffling->generator->id}}</td>
            <td style="text-align:center">{{$reshuffling->generator->generator_name}}</td>

            <td style="text-align:center">{{$reshuffling->poweraction->action_name}}   </td>
       
     

            <td style="text-align:center">{{date('d-m-y', strtotime($reshuffling->created_at))}}</td>
          
            <td style="text-align:center">{{$reshuffling->user_name}}</td>
            <td style="text-align:center">{{$reshuffling->old_site}}</td>
            <td style="text-align:center">{{$reshuffling->new_site}}</td>
            
            <td style="text-align:center">{{$reshuffling->trx}}</td>
 

          @endforeach
        </table>


 



        <script>
    $(document).ready(function() {



var table = $('#example').DataTable( {
       
   orderCellsTop: true,
   fixedHeader: true,
   dom: 'Bfrtip',       
   buttons: [
           
           {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},
                   
          ],

columns: [
    {data: 'id', name: 'id',"visible": true},
  
  {data: 'Generator Name', orderable: false},

  {data: 'from', orderable: false},
  {data: 'action date', orderable: false},
  {data: 'user', orderable: false},
  {data: 'old site', orderable: false},
  {data: 'new site', orderable: false},
  {data: 'trx', orderable: false,},


        ]
});
});


</script>



@endsection