@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
         <li>
 {{ $error }}</li>
       @endforeach
       </div>
   </ul>
</div>
@endif


<form action="{{route('sitecabinets.store')}}" method="POST">
    @csrf
    <div class="raw">
<div class="column">

            <div class="form-group">
                <strong>Site Id</strong>
                <input type="text" name="site_code" class="form-control" placeholder="Site Id">

            </div>
            <div class="form-group">
                <strong>Cabinet Id:</strong>
                <input type="text" name="cabinet_name" class="form-control" placeholder="Cabinet Id">

            </div>

            <div class="form-group">
                <strong> Numbers:</strong>
                <input type="text" name="numbers" class="form-control" placeholder="Numbers">

            </div>
            
            <div class="form-group">
                <strong> Installation Date:</strong>
                <input type="text" name=" installation_date" class="form-control" placeholder="Installation Date">

            </div>
            
           

            <div class="form-group">
                <strong> oracle_code:</strong>
                <input type="text" name="oracle_code" class="form-control" placeholder="oracle_code">

            </div>
            
            <select name="used" id="used">
  <option value="yes">yes</option>
  <option value="No">No</option>

</select>
<div class="pull-right">
    <button class="button">create</button>
   </div>
    </div>

</div>


</form>
</div>
</div>


@endsection
