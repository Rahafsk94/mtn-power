@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
        <h3 class="header">  <p> Sites with Cabinets Information</p></h3>

    </center>

    <div class="row">
 
 <div class="col-md-12">
     <div class="card">
         <div class="card-header">
       
             <h4>
             <div class="u">
                 <div class="pull-right">
                
                     <a href="{{route('sitecabinets.create')}}">    <button type="button" class="btn btn-outline-danger" >Add new cabinet on site</button></a>
                     <a href="cabinets">    <button  class="btn btn-outline-warning" type="submit">All type of cabinets</button></a>
                 

</div>
             </h4>

             <h6>      
       
       

             </h6>
      
         </div>
               
         <div class="card-body">
         <div class="cont">

<table  class="table table-bordered table-striped data-table" width="100%"  >
<thead>
 <tr>

     <th>Site Code</th>
     <th>Cabinet type</th>
     <th>Numbers</th>
     <th>Oracle Code</th>
     <th>Installation date</th>
     </tr>
        <tbody>
            @foreach ($sitecabinets as $v)
            <tr>
            
            
            <td style="text-align:center">{{$v->site->site_code}}</td>
            <td style="text-align:center">{{$v->cabinet->cabinet_name}}</td>
           
            <td style="text-align:center">{{$v->numbers}}</td>
            <td style="text-align:center">{{$v->oracle_code}}</td>
            <td style="text-align:center">{{$v->installation_date}}</td>
       
            @can('Admin')  
            <td style="text-align:center">   
            <div class="center">
             <a  href="{{route('batteries.edit',$v->id)}}"> <i class='fa fa-edit' style="color: #00678f"  style="font-size: 19px;   color: rgb(255, 0, 0);"></i>  </a>         
             <a href="{{route('d',$v->id)}}"> <i class="material-icons" style="font-size:19px;color:red;margin:3px;">remove_circle_outline</i></a> 
            <form action="{{route('sitecabinets.destroy',$v->id)}}" method="Post">

          @csrf
          @method('DELETE')     
        
                <button  type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" onclick="return confirm('Are you sure you want delete the battery?')" ></button>
</form>
</div>

         </td>
            @endcan

           
</tr>
            @endforeach
        </table>
</div>
</div>
@endsection