@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if(session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                    @endif
<center>
        <h3 class="header">Generators Information</h3>


    </center>
<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                            @can('create')
                                <a href="{{route('generators.create')}}">    <button type="button" class="btn btn-outline-success">Add new generator</button></a>
                                @endcan
                        @can('admin')
                                <a href="{{route('addaction')}}">    <button type="button" class="btn btn-outline-success">Add tanks and actions</button></a>
                                @endcan
                                <a href="{{route('generatorswarehouse')}}">    <button type="button" class="btn btn-outline-success">Generator in WareHouse</button></a>

                                @can('track')
                            <a href="{{ route('track') }}"><button type="button" class="btn btn-outline-success">Track</button></a>
                   @endcan
</div>
                        </h4>
               
                        <h6>

                        <div class="dropdown">
  <button class="btn btn-outline-danger" >Generator Actions</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="install">Installation</a>
    <a href="reshuffling">Reshuffling</a>
    <a href="dismantle">Dismantle</a>
  </div>
</div>

                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >ID</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site code</span> </th>
 
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator type</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator capacity</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Engine brand</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Owners</span> </th>

        <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site code Gen id</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Runing hour</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Oracle code</span> </th>
            @can('delete')
            <td style="text-align:center"style="text-align:center" nowrap ><span class="tableheadtext" >Delete </span></td>

            @endcan
         </tr>
        <tbody>
            @foreach ($generator as $v)
            <tr>
          
            <td style="text-align:center">{{$v->id}}</td>

            <td style="text-align:center">{{$v->site->site_code}}</td>
            <td style="text-align:center">{{$v->generator_name}}</td>
            <td style="text-align:center">{{$v->generator_capacity}}</td>
            <td style="text-align:center">{{$v->engine_brand}}</td>
            <td style="text-align:center">{{$v->owners}}</td>

    
            <td style="text-align:center">{{$v->site_code_gen_id}}</td>
            <td style="text-align:center">{{$v->runing_hour}}</td>
            <td style="text-align:center">{{$v->oracle_code}}</td>
          
            @can('delete')

            <td style="text-align:center">   
        
           <form action="{{route('generators.destroy',$v->id)}}" method="Post">
          
          @csrf
          @method('DELETE')

          <button type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" onclick="return confirm('Are you sure you want delete the generator?')" ></button>
          </form>
              
            
          
          @endcan
            @endforeach
        </table>
     
        
</form>


                    </div>
        
        

                
                </div>
               
            </div>
        </div>
    </div>


    <script>


$(document).ready(function() {



var table = $('#example').DataTable( {
       
   orderCellsTop: true,
   fixedHeader: true,
   dom: 'Bfrtip',       
   buttons: [
           
           {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'csv',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'pdf', 
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'copy',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       
                       'colvis'
          ],
          columnDefs: [ {
            targets: [-1],
            visible: false,
      
        } ],
columns: [
    {data: 'id', name: 'id'},
  {data: 'site_code', orderable: false,},

  {data: 'Generator Name', orderable: false},
  {data: 'Generator Capacity', orderable: false},
  {data: 'Engine Brand', orderable: false},
  {data: 'Owners', orderable: false},

  {data: 'site_code_gen_id', orderable: false},
  {data: 'Runing_hour', orderable: false},
  {data: 'oracle_code', orderable: false},
  @can('delete')
  {  data: 'x', orderable: false },
 
  @endcan

        ]
});
});



$(document).ready(function(){
    
    $('.editbtn').on('click', function(){

        $('#editmodal').modal('show');
        $tr =$(this).closest('tr');
        var data= $tr.children("td").map(function(){
          return $(this).text();
        }).get();

       
        $('#generator_id').val(data[0]);
     
      
        
    });
});







</script>



<script>
 
function test(a) {
var x = a.options[a.selectedIndex].label;
if(x=='reshuffling') { $("#add_fields_placeholderValue").show();}
else{
    $("#add_fields_placeholderValue").hide();
}
}


</script>
@endsection