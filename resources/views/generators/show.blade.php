
@extends('layouts.app')
@section('content')
<form action="{{route('generators.show',$generator->id)}}" method="GET">
@csrf
<table class="table table-bordered">

    <tr>

      <th scope="col">generator_id</th>
      <th scope="col">Site_Code</th>
      <th scope="col">generator_name</th>
      <th scope="col">generator_capacity</th>
      <th scope="col">engine_brand</th>
      <th scope="col">owners</th>
      <th scope="col">installation_date</th>
      <th scope="col">end_date</th>
      <th scope="col">site_code_gen_id</th>
      <th scope="col">Runing_hour</th>
    </tr>

  <tbody>
  <tr>
         <td>{{$generator->id}}</td>
          <td>{{$generator->site->site_code}}</td>
          <td>{{$generator->generator_name}}</td>
          <td>{{$generator->generator_capacity}}</td>
          <td>{{$generator->engine_brand}}</td>
          <td>{{$generator->owners}}</td>
          <td>{{$generator->installation_date}}</td>
          <td>{{$generator->end_date}}</td>
          <td>{{$generator->site_code_gen_id}}</td>
          <td>{{$generator->runing_hour}}</td>
          <td>    <form >


                <a class="btn btn-primary" href="{{route('generators.edit',$generator->id)}}">edit
                </a>


            </form>

        </td>
  </tr>
        </form>

@endsection
