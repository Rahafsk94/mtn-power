@extends('layouts.app')
@section('content')

@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif

<form action="{{route('generators.store')}}" method="POST"  id="form_todos">
    @csrf
    <div class="raw">
<div class="column">

        <div class="form-group">
            <strong>site code:</strong>


                <input type="text" name="site_code" id="site_code" class="form-control" placeholder="site code" required>
                <span id="error-message" style="color: red; display: none;">* the site is not available *</span>
        </div>


            <div class="form-group">
                <strong>Generator type:</strong>
                <input type="text" name="generator_name" class="form-control" placeholder="Generator type" required>

            </div>

            <div class="form-group">
            <strong>Generator capacity:</strong>
            <select class="form-control" name="generator_capacity"  placeholder="Generator type" required >
            <option value="" >Select capacity</option>

        <option >8</option>

        <option >15</option>
        <option >17</option>
        <option >20</option>
        <option >22</option>  
    </select>


    </div>

            <div class="form-group">
                <strong>Runing hour:</strong>
                <input type="number" name="runing_hour" class="form-control" placeholder="Runing hour" required>

            </div>

            <div class="form-group">
        <strong>Site code gen id:</strong>
        <input type="text" name="site_code_gen_id" class="form-control" placeholder="Site code gen id">

    </div>
 
</div>
<div class="column">
    <div class="form-group">
            <strong>Engine brand:</strong>
            <input type="text" name="engine_brand" class="form-control" placeholder="Engine brand" required>

        </div>
  




    


    <div class="form-group">
        <strong>Owners:</strong>
        <input type="text" name="owners" class="form-control" placeholder="Owners" id="display" required>

    </div>



    <div class="form-group">
        <strong>Installation date:</strong>
        <input type="date" name="installation_date" class="form-control" placeholder="Installation date" required>

    </div>



 

<div class="form-group">
        <strong>PR:</strong>
        <input type="number" name="pr"  id="pr" class="form-control" placeholder="PR " required>

</div>
<div class="form-group">
        <strong>Oracle code:</strong>
        <input type="number" name="oracle_code"   class="form-control" placeholder="oracle code ">

</div>

</div>




<div class="pull-right">

    <button class="button">create</button>
   </div>
    </div>

</div>


</form>
</div>
</div>
<script>

    $(document).ready(function(){

        $('#site_code').blur(function() {

            var siteCode = $(this).val();
            $.ajax({
            url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("site_code").value="";

              }
            
    },

        });
    });});
</script>
@endsection
