@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
        <h3 class="header">Generators Information</h3>

    </center>
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">

                            <a href="{{route('shipmentcreate')}}">    <button type="button" class="btn btn-outline-success" >Add new shipment</button></a>

                        </h4>

                        <h6>
                        <a href="{{route('generators.index')}}">    <button type="button" class="btn btn-outline-danger" >All Generators</button></a>


                        </h6>


                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >ID</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Warehouse</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator type</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator capacity</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Engine brand</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Owners</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site code/Gen_id</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Oracle code</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Shipment date</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Dismantle date</span> </th>
@can('admin')
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Install on site</span> </th>
            @endcan
</tr>
            <tbody>
            @foreach ($generator as $v)
            <tr>
            <td style="text-align:center">{{$v->id}}</td>

<td style="text-align:center">{{$v->warehouse->name}}</td>
<td style="text-align:center">{{$v->generator_name}}</td>
<td style="text-align:center">{{$v->generator_capacity}}</td>
<td style="text-align:center">{{$v->engine_brand}}</td>
<td style="text-align:center">{{$v->owners}}</td>


<td style="text-align:center">{{$v->site_code_gen_id}}</td>
<td style="text-align:center">{{$v->oracle_code}}</td>
<td style="text-align:center">@if($v->shipment_date)

{{date("d-m-Y", strtotime($v->shipment_date))}}
@endif</td>
<td style="text-align:center">@if($v->dismantle_date)

{{date("d-m-Y", strtotime($v->dismantle_date))}}
@endif</td>

            @can('admin')

          




                    <td style="text-align:center">


                                   <button type="button" id="edit_todo" data-id="{{$v->id }}" class="btn btn-outline-success">Install</button></td>

                @endcan


            @endforeach
        </table>


       <!-- The Modal -->
       <div class="modal" id="modal_todo">
            <div class="modal-dialog">
              <div class="modal-content">
                <form id="form_todo">
                    <div class="modal-header">
                      <h4 class="modal-title" id="modal_title"></h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                      <input type="hidden"  name="id" id="id">
                      <input type="text" name="site_code" id="name_todo" class="form-control" placeholder="Site code ..." required>
                      <span id="error-message" style="color: red; display: none;">* The site is not available *</span>
                      <input type="number" name="pr" id="name_todoo" class="form-control" placeholder="Pr..."required >
                      <input type="date" name="installation_date"  class="form-control" required >
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-info">Install</button>
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>





                <script type="text/javascript">
                  $(document).ready(function(){
                    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

                  });
      $("#add_todo").on('click',function(){
          $("#form_todo").trigger('reset');
          $("#modal_title").html('Add todo');
          $("#modal_todo").modal('show');
          $("#id").val("");
      });
                  $("body").on('click','#edit_todo',function(){
                      var id = $(this).data('id');
                      $.get('/genes/'+id+'/e',function(res){
                          $("#modal_title").html('Install on site');
                          $("#id").val(res.id);
                          $("#name_todo").val(res.site_code);
                          $("#name_todoo").val(res.pr);
                          $("#modal_todo").modal('show');
                      });
                  });
                  // Delete Todo

                  //save data

                  $("form").on('submit',function(e){
                      e.preventDefault();
                      $.ajax({
                          url:"/generator/s",
                          data: $("#form_todo").serialize(),
                          type:'POST'
                      }).done(function(res){
                          var row = '<tr id="row_todo_'+ res.id + '">';
                          row += '<td width="20">' + res.id + '</td>';
                          row += '<td>' + res.site_code + '</td>';
                          row += '<td width="150">' + '<button type="button" id="edit_todo" data-id="' + res.id +'" class="btn btn-info btn-sm mr-1">Edit</button>' + '<button type="button" id="delete_todo" data-id="' + res.id +'" class="btn btn-danger btn-sm">Delete</button>' + '</td>';
                          if($("#id").val()){
                              $("#row_todo_" + res.id).replaceWith(row);
                              setTimeout(function () { // wait 3 seconds and reload
    window.location.reload(true);
  }, 1000);
                          }else{
                              $("#list_todo").prepend(row);
                          }
                          $("#form_todo").trigger('reset');
                          $("#modal_todo").modal('hide');
                      });
                  });
              </script>

            </div>
        </div>
    </div>

  <script>


    $(document).ready(function() {





   var table = $('#example').DataTable( {
    orderCellsTop: true,
       fixedHeader: true,
       dom: 'Bfrtip',
buttons: [
    {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},'colvis'

],

columnDefs: [ {
            targets:[-2,-3] ,
            visible: false
        } ],
columns: [
{data: 'id', name: 'id'},
{data: 'type', orderable: false,},
{data: 'used', orderable: false},
{data: 'oracle_code', orderable: false},
{data: 'number', orderable: false},
{data: 'delete', orderable: false},
{data: 'edit', orderable: false},
{data: 'show', orderable: false},
{data: 'shipment', orderable: false},
{data: 'dismantle', orderable: false},
@can('admin')
{data: 'add one', orderable: false},

@endcan





]
   });
});




  </script>

        </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script>

    $(document).ready(function(){

        $('#name_todo').blur(function() {

            var siteCode = $(this).val();
            $.ajax({
            url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("name_todo").value="";

              }
            
    },

        });
    });});
</script>

@endsection

