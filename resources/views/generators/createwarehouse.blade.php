@extends('layouts.app')
@section('content')

@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif







<form action="{{route('shipment')}}" method="POST" id="myForm">
    @csrf
    <div class="raw">
<div class="column">

<div class="form-group">
    <strong>Warehouse :</strong>
    <input type="text" name="warehouse_name" class="form-control" id="warehouse_name" placeholder="warehouse " required>
    <span id="error-message" style="color: red; display: none;">* the warehose is not available *</span>
</div>



            <div class="form-group">
                <strong>Generator type:</strong>
                <input type="text" name="generator_name" id="generator_name"class="form-control" placeholder="Generator type" required>

            </div>
            

            <div class="form-group">
            <strong>Generator capacity:</strong>
            <select class="form-control" name="generator_capacity"  id="generator_capacity" placeholder="Generator type" required >
            <option value="" >select</option>

        <option >8</option>
        <option >15</option>
        <option >17</option>
        <option >20</option>
        <option >22</option>
    </select>


    </div>


            <div class="form-group">
        <strong>Site code gen id:</strong>
        <input type="text" name="site_code_gen_id" class="form-control" placeholder="Site code gen id" required>

    </div>

 

</div>
<div class="column">
    <div class="form-group">
            <strong>Engine brand:</strong>
            <input type="text" name="engine_brand" class="form-control" placeholder="Engine brand" required>

        </div>



        <div class="form-group">
        <strong>number:</strong>
        <input type="number" name="number" class="form-control" placeholder="number " required>

</div>





    <div class="form-group">
        <strong>Shipment date:</strong>
        <input type="date" name="activation_date" class="form-control" placeholder="Shipment date " required>

</div>





<div class="form-group">
    <strong>Oracle code :</strong>
    <input type="text" name="oracle_code" class="form-control" placeholder="Oracle code" required>

</div>

</div>




<div class="pull-right">

    <button class="button" type="submit" >Add</button>
   </div>
    </div>

</div>


</form>
</div>
</div>

<script>
    $("#warehouse_name").on('change',function(){

 var warehouseName= $(this).val();
 console.log(warehouseName);
 $.ajax({
    url: "{{ route('cheack_warehouse') }}",
    method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                warehouse_name:warehouseName
            },
            dataType:'json',
         
            success: function(response) {
        
              if(response.a){
               
                $('#error-message').hide();
              
              }
              if(response.b){
        
                $('#error-message').show();
      
            document.getElementById("warehouse_name").value="";

              }
            
    },
 });

    });
</script>

@endsection