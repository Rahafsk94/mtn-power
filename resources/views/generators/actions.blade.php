@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if(session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                    @endif
<center>
        <h3 class="header">Generators Information</h3>

    </center>
<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                     
                                <a href="{{route('generatorswarehouse')}}">    <button type="button" class="btn btn-outline-success">Generator in  WareHouse</button></a>
                             

</div>
                        </h4>
               
                        <h6>
                        <a href="{{route('generators.index')}}">    <button type="button" class="btn btn-outline-danger" >All Generators</button></a>
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >ID</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site code</span> </th>

           
            


            
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator Type</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Engine Brand</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site Code Gen Id</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Installation date</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Shipment date</span> </th>
   
         
  
            
            @can('admin')
            
            <th style="text-align:center"style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span></th>
            <th style="text-align:center"style="text-align:center" nowrap ><span class="tableheadtext" >Add new tank</span></th>
            <th style="text-align:center"style="text-align:center" nowrap ><span class="tableheadtext" >Reshuffling </span></th>

            <th style="text-align:center"style="text-align:center" nowrap ><span class="tableheadtext" >Dismantle </span></th>
            @endcan
         </tr>
        <tbody>
            @foreach ($generator as $v)
            <tr>
          
            <td style="text-align:center">{{$v->id}}</td>

            <td style="text-align:center">{{$v->site->site_code}}</td>
            <td style="text-align:center">{{$v->generator_name}}</td>

            <td style="text-align:center">{{$v->engine_brand}}</td>
            <td style="text-align:center">{{$v->site_code_gen_id}}</td>
            <td style="text-align:center">{{ date('d-m-y', strtotime($v->installation_date))}}</td>
            <td style="text-align:center"> @if($v->shipment_date) {{ date('d-m-y', strtotime($v->shipment_date))}}@endif</td>

          
       
        
            @can('admin')

           
            <td style="text-align:center">   
          
           <a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('generators.edit',$v->id)}}"></a> 
         
               
            
                </td>
                
            <td style="text-align:center"> 
                <a class='fa fa-plus red-color' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('newtankcreate',$v->id)}}"></a></td>
                <td style="text-align:center">   
                <button type="button" id="edit_todos" data-id="{{$v->id }}" class="btn btn-outline-success">	Reshuffling</button></td>
                  
                                    
                                    <td style="text-align:center">


<button type="button" id="edit_todo" data-id="{{$v->id }}" class="btn btn-outline-success">	dismantle</button></td>              
          @endcan
            @endforeach
        </table>
     
     
        
        

                
                </div>
               
            </div>

                  <!-- The Modal -->
       <div class="modal" id="modal_todos">
            <div class="modal-dialog">
              <div class="modal-content">
                <form id="form_todos">
                    <div class="modal-header">
                      <h4 class="modal-title" id="modals_title"></h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
               
                    <p>Enter the new site:</p>
                
                    <input type="hidden" id="id" name="id" class="form-control" placeholder="Number" required>
                    
                   <input type="number"  class="form-control" name="trx" id="name_todos"  placeholder="trx" required><br>
                   <input type="text"  class="form-control" name="site_code" id="name_todoos"  placeholder="Site code" required><br>
                   <span id="error-message" style="color: red; display: none;">* the site is not available *</span>
                   <input type="date" class="form-control" name="date"   required><br>
                   <input type="hidden"  name="action_name"  value="reshuffling" required>



                   
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button   id="demo" class="btn btn-info">Reshuffling</button>
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
       
               
            </div>
        </div>
       
                    </div>
        
        

                
                </div>
               
            </div>
        </div>

        


    </div>

    
      



    </div>





       <!-- The Modal -->
       <div class="modal" id="modal_todo">
            <div class="modal-dialog">
              <div class="modal-content">
                <form id="form_todo">
                    <div class="modal-header">
                      <h4 class="modal-title" id="modal_title"></h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">

                    <input type="hidden" name="action_name" class="form-control"  value="dismantle">

                    
                    <input type="hidden" id="g_id" name="id" class="form-control" placeholder="Number" required>
                   <input class="form-control" type="text" name="rm" id="name_todo"  placeholder="RM" required>
                   <input type="date" class="form-control" name="dismantle_date"   required>
               
<select  class="form-control" name="warehouse_name" id="name_todoo" required>
<option value="null" >  Select warehouse  </option>   
                    @foreach($warehouses as $warehouse)
                               
  <option value="{{ $warehouse->name}}" >{{$warehouse->name}}  </option>

  @endforeach
 
</select>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-info">Dismantle</button>
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>





                





                

                


                <script type="text/javascript">
                  $(document).ready(function(){
                    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

                  });
      $("#add_todo").on('click',function(){
          $("#form_todo").trigger('reset');
          $("#modal_title").html('Add todo');
          $("#modal_todo").modal('show');
          $("#id").val("");
      });
                  $("body").on('click','#edit_todo',function(){
                      var id = $(this).data('id');
                      $.get('/dismantle/'+id+'/e',function(res){
                          $("#modal_title").html('Return to warehouse');
                          $("#g_id").val(res.id);
                          $("#name_todo").val(res.rm);
                          $("#name_todoo").val(res.warehouse_name);
                          $("#modal_todo").modal('show');
                      });
                  });
                  // Delete Todo

                  //save data

                  $("form").on('submit',function(e){
                      e.preventDefault();
                      $.ajax({
                          url:"/dismantle/s",
                          data: $("#form_todo").serialize(),
                          type:'POST'
                      }).done(function(res){
                          var row = '<tr id="row_todo_'+ res.id + '">';
                          row += '<td width="20">' + res.id + '</td>';
                          row += '<td>' + res.site_code + '</td>';
                          row += '<td width="150">' + '<button type="button" id="edit_todo" data-id="' + res.id +'" class="btn btn-info btn-sm mr-1">Edit</button>' + '<button type="button" id="delete_todo" data-id="' + res.id +'" class="btn btn-danger btn-sm">Delete</button>' + '</td>';
                          if($("#id").val()){
                              $("#row_todo_" + res.id).replaceWith(row);
                              
                            


                          }else{
                              $("#list_todo").prepend(row);
                          }
                          $("#form_todo").trigger('reset');
                          $("#modal_todo").modal('hide');
                      });
                  });
              </script>





<script type="text/javascript">
                  $(document).ready(function(){
                    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

                  });
      $("#add_todo").on('click',function(){
          $("#form_todos").trigger('reset');
          $("#modals_title").html('Add todo');
          $("#modal_todos").modal('show');
          $("#id").val("");
      });
                  $("body").on('click','#edit_todos',function(){
                      var id = $(this).data('id');
                      $.get('/reshuffling/'+id+'/e',function(res){
                          $("#modals_title").html('Move to another site');
                          $("#id").val(res.id);
                          $("#name_todos").val(res.trx);
                          $("#name_todoos").val(res.site_code);
                          $("#modal_todos").modal('show');
                      });
                  });
                  // Delete Todo

                  //save data

                  document.getElementById("demo").addEventListener("click",function(e){
                      e.preventDefault();
                      $.ajax({
                          url:"/reshuffling/s",
                          data: $("#form_todos").serialize(),
                          type:'POST'
                      }).done(function(res){
                          var row = '<tr id="row_todo_'+ res.id + '">';
                          row += '<td width="20">' + res.id + '</td>';
                          row += '<td>' + res.site_code + '</td>';
                          row += '<td width="150">' + '<button type="button" id="edit_todos" data-id="' + res.id +'" class="btn btn-info btn-sm mr-1">Edit</button>' + '<button type="button" id="delete_todo" data-id="' + res.id +'" class="btn btn-danger btn-sm">Delete</button>' + '</td>';
                          if($("#id").val()){
                              $("#row_todo_" + res.id).replaceWith(row);
                              setTimeout(function () { // wait 3 seconds and reload
                                              window.location.reload(true);
                                          }, 2000);

                          }else{
                              $("#list_todo").prepend(row);
                          }
                          $("#form_todos").trigger('reset');
                          $("#modal_todos").modal('hide');
                      });
                  });
              </script>


            </div>
        </div>
    </div>










    <script>


$(document).ready(function() {



var table = $('#example').DataTable( {
       
   orderCellsTop: true,
   fixedHeader: true,
   dom: 'Bfrtip',       
   buttons: [
           'colvis',
           {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'csv',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'pdf', 
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'copy',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       
                     
          ],
          

columns: [
    {data: 'id', name: 'id',"visible": true},
  {data: 'site_code', orderable: false,},
  {data: 'Generator Name', orderable: false},
  {data: 'Engine Brand', orderable: false},
  {data: 'Installation date', orderable: false},
  {data: 'Activation date', orderable: false},

  {data: 'site code gen id', orderable: false},
  @can('admin')
  {  data: 'x', orderable: false },
  {  data: 'r', orderable: false },

  {  data: 'z', orderable: false },
  {  data: 'n', orderable: false },

  @endcan

        ]
});
});


</script>




<script>

    $(document).ready(function(){

        $('#name_todoos').blur(function() {

            var siteCode = $(this).val();
            $.ajax({
            url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("name_todoos").value="";

              }
            
    },

        });
    });});
</script>
@endsection