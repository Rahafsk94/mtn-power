<table>
    <thead>
    <tr>
        <th>Generator id</th>
        <th>site code</th>
        <th>generator_name	</th>
        <th>generator_capacity</th>
        <th>engine_brand</th>
        <th>owners</th>
        <th>installation_date</th>
        <th>activation_date	</th>
        <th>end_date</th>
        <th>site_code_gen_id</th>
        <th>Runing_hour</th>
    </tr>
    </thead>
    <tbody>
    @foreach($generator as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->site->site_code}}</td>
            <td>{{ $item->generator_name	}}</td>
            <td>{{ $item->generator_capacity	}}</td>
            <td>{{ $item->engine_brand	}}</td>
            <td>{{$item->owners	}}</td>
            <td>{{$item->installation_date}}</td>
            <td>{{ $item->activation_date}}</td>
            <td>{{ $item->end_date}}</td>
            <td>{{ $item->site_code_gen_id}}</td>
            <td>{{ $item->runing_hour}}</td>
        </tr>
    @endforeach
    </tbody>
</table>


<table>
         <tr>

          <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >event</span> </th>

</tr>

@foreach($generator as $v)
@foreach($v->Audits as $generator)
<tr>
<td style="text-align:center">{{$generator->event}}</td>
<td style="text-align:center">{{$generator->user->name}}</td>

@foreach($generator->old_values as $g )
<ul>
    <li>{{$g}}</li>
</ul>

  
</tr>
@endforeach
   @endforeach
@endforeach

</table>

