
@extends('generators.layout')
@section('content')
@if($gen->isNotEmpty())
<table class="table table-bordered">

    <tr>

      <th scope="col">generator_id</th>
      <th scope="col">Site_Code</th>
      <th scope="col">generator_name</th>
      <th scope="col">generator_capacity</th>
      <th scope="col">engine_brand</th>
      <th scope="col">owners</th>
      <th scope="col">installation_date</th>
      <th scope="col">end_date</th>
      <th scope="col">site_code_gen_id</th>
      <th scope="col">Runing_hour</th>
    </tr>

  <tbody>


    @foreach ($gen as $item)

    <tr>

        <td>{{$item->id}}</td>
        <td>{{$item->->site->site_code}}</td>
        <td>{{$item->generator_name}}</td>
        <td>{{$item->generator_capacity}}</td>
        <td>{{$item->engine_brand}}</td>
        <td>{{$item->owners}}</td>
        <td>{{$item->installation_date}}</td>
        <td>{{$item->end_date}}</td>
        <td>{{$item->site_code_gen_id}}</td>
        <td>{{$item->runing_hour}}</td>
        <td>
         <form action="{{route('generators.destroy',$item->generator_id)}}" method="Post">
        <a class="btn btn-info" href="{{route('generators.edit',$item->generator_id)}}">Edit</a>
        <a class="btn btn-primary" href="{{route('generators.show',$item->generator_id)}}">show</a>
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Delete</button>
        </form>
        </td>
    @endforeach
    @else
    <div>
        <center>
        <h2>No generator found</h2></center>
    </div>
        @endif
@endsection

