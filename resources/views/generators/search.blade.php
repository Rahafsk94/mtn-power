@extends('layouts.app')
@section('content')
@if($generators->isNotEmpty())
<center>
        <h3 class="header">Generators Information</h3>

    </center>
<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
         

                            <div class="pull-right">
                            <a >       <button type="button" id="export" class="btn btn-outline-secondary">export</button></a>
                          
                            <a href="{{ route('searchform') }}"> <button type="button" class="btn btn-outline-success">Runing hour search</button></a>
                                <a href="/generators"><button   class="btn btn-outline-warning" type="submit">All Generators</button></a>
                                <a href="{{route('generators.create')}}">    <button type="button" class="btn btn-outline-danger" >Add new generator</button></a>
                     
                    

</div>
                        </h4>
                        <h6>
                        <form action="{{ route('searchs') }}" method="GET">
                <input type="text" name="searchs" placeholder="Search by Site Code.." required/>
                <button type="submit">Search</button>
            </form>
                        </h6>
                       
                    </div>
                    <div class="card-body">

                    <table id="customer_data" class="table table-bordered table-striped">
                            <thead>
                                <tr>

      <th scope="col">generator_id</th>
      <th scope="col">Site_Code</th>
      <th scope="col">finance_Code</th>
      <th scope="col">generator_name</th>
      <th scope="col">generator_capacity</th>
      <th scope="col">engine_brand</th>
      <th scope="col">owners</th>
      <th scope="col">installation_date</th>
      <th scope="col">activation_date</th>
      <th scope="col">end_date</th>
      <th scope="col">site_code_gen_id</th>
      <th scope="col">Runing_hour</th>
      <th scope="col">edit action</th>
    </tr>
</thead>
<tbody>
  @foreach ($generators as $item)
     <tr>
        <td>{{$item->id}}</td>
        <td>{{$item->site->site_code}}</td>
        <td>{{$item->site->finance_code}}</td>
        <td>{{$item->generator_name}}</td>
        <td>{{$item->generator_capacity}}</td>
        <td>{{$item->engine_brand}}</td>
        <td>{{$item->owners}}</td>
        <td>{{date('d-m-y', strtotime($item->installation_date))}}</td>
        <td>{{date('d-m-y', strtotime($item->activation_date))}}</td>
        <td>{{date('d-m-y', strtotime($item->end_date))}}</td>
        <td>{{$item->site_code_gen_id}}</td>
        <td>{{$item->runing_hour}}</td>   
        <td>edit action</td> 


    </tr>
    @endforeach
</tbody>
    </table>
@else
    <div>
        <center>
        <h2 class="header">No generator found</h2></center>
    </div>
        @endif
        
<script>

$(document).ready(function() {
    $('#customer_data').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'excel', 'csv', 'pdf', 'copy'
   ],
        
      
    } );
} );


</script>


@endsection