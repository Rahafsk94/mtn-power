@extends('layouts.app')
@section('content')


<form action="{{route('generators.update',$generator->id)}}" method="POST">
    @csrf
    @method('PUT')
    @if ($errors->any())


    <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
       </ul>
    </div>

    @endif
<div class="raw">
    <div class="column">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Generator type:</strong>
                <input type="text" name="generator_name"

                value="{{$generator->generator_name}}"
                class="form-control" placeholder="Generator Name" required>

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Generator capacity:</strong>
                <input type="text" name="generator_capacity"
                value="{{$generator->generator_capacity}}"
                class="form-control" placeholder="Generator capacity" required>

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Engine brand:</strong>
                <input type="text" name="engine_brand"
                value="{{$generator->engine_brand}}"
                class="form-control" placeholder="Engine brand" required>

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Owners:</strong>
                <input type="text" name="owners"
                value="{{$generator->owners}}"
                class="form-control" placeholder="Owners" required>

            </div>
        </div>
    </div>
<div class="column">
    
    


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Site code gen id:</strong>
            <input type="text" name="site_code_gen_id"
            value="{{$generator->site_code_gen_id}}"
            class="form-control" placeholder="Site code gen id" required>

        </div>
    </div>
    

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Runing hour:</strong>
            <input type="text" name="runing_hour"
            value="{{$generator->runing_hour}}"
            class="form-control" placeholder="Runing hour"required>

        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Oracle code:</strong>
                <input type="number" name="oracle_code"
                value="{{$generator->oracle_code}}"
                class="form-control" placeholder="Oracle code" required>

            </div>
        </div>
      
        
</div>
<div class="pull-right">
<button class="button" type="submit" >Update</button>
</div>
</form>
</div>


    @endsection
