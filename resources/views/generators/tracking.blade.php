
@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
  <p>tracking info</p>
</center>

<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                    </div>
                    <div class="card-body">

<table class="table table-bordered table-striped" id="example" width="100%">
<thead>
<tr>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >ID</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator Type</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >User</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Old value</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >New value</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action time</span> </th>
</tr>
</thead>
<tbody>
            @foreach ($generator as $v)
            @foreach (  $v->audits as $t)
            <tr>
            <td style="text-align:center">{{$v->id}}</td>
            <td style="text-align:center">{{$v->generator_name}}</td>
    
            
            <td style="text-align:center">{{$t->event}}</td>
            <td style="text-align:center">{{$t->user->name}}</td>
               
            <td style="text-align:center">
<button class="collapsible">Open Collapsible</button>
<div class="content">
  @foreach ( $t->old_values as $key=>$value)
  <p>{{$key}}</p><li>{{$value}} </li> 
  @endforeach
  </div> 
</td>
         




            <td style="text-align:center">
<button class="collapsible">Open Collapsible</button>
<div class="content">
  @foreach ( $t->new_values as $key=>$value)
  <p>{{$key}}</p><li>{{$value}} </li> 
  @endforeach
  </div> 
</td>
<td style="text-align:center">{{$t->created_at}}</td>
            @endforeach

            @endforeach
</tbody>
</table>
</div>
</div>
<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "3px";
    } 
  });
}         
            
</script>
<script>
$(document).ready(function() {
  var table = $('#example').DataTable({
    
buttons: [
    'excel'

],
   

  });});
</script>
@endsection
