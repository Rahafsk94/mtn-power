@extends('layouts.app')
@section('content')
<center>

    <div class="card" style="width: 40rem;">
        <div class="card-header pb-0">
            <h5 class="card-title">Search By Running hour</h5>
        </div>

            <div class="card-body">

                <form action="{{ route('searchform') }}" method="GET">
                     <input type="text" name="x" placeholder="greater or equal to...." required/>

          <button class="btn-btn-primary" type="submit">Search</button>
                </form>
               
            
                <form action="{{ route('searchform') }}" method="GET">
                    <input type="text" name="y" placeholder="Less than or equal to...." required/>

         <button class="btn-btn-primary" type="submit">Search</button>
               </form>
        <form action="{{ route('searchform') }}" method="GET">
                <input type="text" name="u" placeholder="more than.." required/>
                <input type="text" name="z" placeholder="Less than.." required/>

     <button class="btn-btn-primary" type="submit">Search</button>

           </form>
           <div class=" pull-right">


</div>
        
</div>

</div>
<div class="card">
                    <div class="card-header">
                    <h4>
                     
                            <div class="pull-right">
                    <button id="export" class="btn btn-outline-secondary" >Export Now</button>
           
</h4>
                        </div>
                        
                        <div class="card-body">
        <table  id="tx"  class="table table-bordered table-striped">
 
            <tr>

              <th scope="col">Generator id</th>
              <th scope="col">Site Code</th>
              <th scope="col">Generator name</th>
              <th scope="col">Generator capacity</th>
              <th scope="col">Engine brand</th>
              <th scope="col">Owners</th>
              <th scope="col">Installation date</th>
              <th scope="col">activation date</th>
              <th scope="col">End date</th>
              <th scope="col">site_code_gen_id</th>
              <th scope="col">Runing_hour</th>
      

            </tr>

          <tbody>

            @if($gen)
            @foreach ($gen as $item)

         <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->site->site_code}}</td>
            <td>{{$item->generator_name}}</td>
            <td>{{$item->generator_capacity}}</td>
                <td>{{$item->engine_brand}}</td>
                <td>{{$item->owners}}</td>
                <td>{{$item->installation_date}}</td>
                <td>{{$item->activation_date}}</td>
                <td>{{$item->end_date}}</td>
                <td>{{$item->site_code_gen_id}}</td>
                <td>{{$item->runing_hour}}</td>
                
            <td style="text-align:center">
<form action="{{route('generators.destroy',$item->id)}}" method="Post">
        @csrf
        @method('DELETE')
        <button type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" ></button>
        </form>

</td>

<td style="text-align:center">
<a class='fa fa-edit' style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('generators.edit',$item->id)}}"></a> 
</td>
<td style="text-align:center">
        <a class='fa fa-eye'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('generators.show',$item->id)}}"></a>
    </td>

            @endforeach

                @endif
      </div>

</table>
</div>
</center>

<script>
 
 document.getElementById('export').onclick=function(){
        var tableData= document.getElementById('tx').id;
        htmlTableToExcel(tableData, filename = '');
    }


   var htmlTableToExcel= function(tableData, fileName = ''){

    var excelFileName='excel_table_data';
    var TableDataType = 'application/vnd.ms-excel';
    var selectTable = document.getElementById(tableData);
    var htmlTable = selectTable.outerHTML.replace(/ /g, '%20');
    
    filename = filename?filename+'.xlsx':'excel_data.xlsx';
    var excelFileURL = document.createElement("a");
    document.body.appendChild(excelFileURL);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', htmlTable], {
            type: TableDataType
        });
        navigator.msSaveOrOpenBlob( blob, fileName);
    }else{
        
        excelFileURL.href = 'data:' + TableDataType + ', ' + htmlTable;
        excelFileURL.download = fileName;
        excelFileURL.click();
    }
}
</script>



@endsection

