@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif

<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                         

</div>
                        </h4>
               
                        <h6>
                
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Cabinet id</span> </th>
  
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Cabinet type</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action name</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Form site</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action time</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Craeted by mame</span> </th>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >RM</span> </th>
            

         </tr>
        <tbody>
            @foreach ($dismantles as $dismantle)
          
          
            <tr>
            <td style="text-align:center">{{$dismantle->cabinet->id}}</td>
            <td style="text-align:center">{{$dismantle->cabinet->cabinet_name}}</td>

            <td style="text-align:center">{{$dismantle->poweraction->action_name}}   </td>
       
            <td style="text-align:center">{{$dismantle->old_site}}</td>
    
            <td style="text-align:center">{{date('d-m-y', strtotime($dismantle->created_at))}}</td>
          
            <td style="text-align:center">{{$dismantle->user_name}}</td>
            
            
            <td style="text-align:center">{{$dismantle->rm}}</td>

          @endforeach
        </table>


        

                
                </div>
               
            </div>
        </div>
    </div>


    </div>

<script>

    var table =$('#example').DataTable({
        orderCellsTop: true,
       fixedHeader: true,
       dom: 'Bfrtip',       
buttons: ['excel']
    });
</script>



@endsection