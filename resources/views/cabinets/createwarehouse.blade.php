@extends('layouts.app')
@section('content')

@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif







<form action="{{route('csh')}}" method="POST" id="myForm">
    @csrf
    <div class="raw">
<div class="column">




            <div class="form-group">
                <strong>Warehouse name:</strong>
                <input type="text" name="warehouse_name"  id="warehouse_name"class="form-control" placeholder="Warehouse" required>
                <span id="error-message" style="color: red; display: none;">* the warehouse is not available *</span>
            </div>
            

            


            <div class="form-group">
        <strong>Numbers:</strong>
        <input type="text" name="number" class="form-control" placeholder="number" required>

    </div>

  
    <div class="form-group">
        <strong>Shipment date:</strong>
        <input type="date" name="shipment_date" class="form-control" placeholder="Shipment date" required>

    </div>
</div>
<div class="column">

<div class="form-group">
        <strong>Status:</strong>
        <select class="form-control" name="is_used"   placeholder="Is uesed!" required >
            <option value="">Select status</option>

        <option value="used">Uesed</option>
        <option value="new" >New</option>
  
    </select>

</div>



<div class="form-group">
    <strong>Oracle code :</strong>
    <input type="number" name="oracle_code" class="form-control" placeholder="Oracle code" required>

</div>



<div class="form-group">
    <strong>Cabinet name :</strong>
    <input type="text" name="cabinet_name" class="form-control" placeholder="Cabinet name" required>

</div>

</div>





<div class="pull-right">

    <button class="button" type="submit" >Add</button>
   </div>
    </div>

</div>


</form>
</div>
</div>

<script>
    $("#warehouse_name").on('change',function(){

 var warehouseName= $(this).val();
 console.log(warehouseName);
 $.ajax({
    url: "{{ route('cheack_warehouse') }}",
    method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                warehouse_name:warehouseName
            },
            dataType:'json',
         
            success: function(response) {
        
              if(response.a){
               
                $('#error-message').hide();
              
              }
              if(response.b){
        
                $('#error-message').show();
      
            document.getElementById("warehouse_name").value="";

              }
            
    },
 });

    });
</script>
@endsection