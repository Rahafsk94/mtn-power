
@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if(session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                    @endif
<center>
        <h3 class="header">Cabinets Information</h3>

    </center>
<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                         
                   
                  

                             

</div>
                        </h4>
               
                        <h6>
                        <a href="{{route('cabinets.index')}}">    <button type="button" class="btn btn-outline-danger" >Cabinets in site</button></a>
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >ID</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site code</span> </th>

           
            


            
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Type</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Numbers </span> </th>

            
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Status</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Installation date</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Shipment date</span> </th>
    
         
  
            
            @can('edit')
            <th style="text-align:center"style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span></th>
            @endcan
            @can('admin')
            <th style="text-align:center"style="text-align:center" nowrap ><span class="tableheadtext" >Reshuffling </span></th>

            <th style="text-align:center"style="text-align:center" nowrap ><span class="tableheadtext" >dismantle </span></th>
            @endcan
         </tr>
        <tbody>
            @foreach ($cabinets as $v)
            <tr>
          
            <td style="text-align:center">{{$v->id}}</td>

            <td style="text-align:center">{{$v->site->site_code}}</td>
            <td style="text-align:center">{{$v->cabinet_name}}</td>
            <td style="text-align:center">{{$v->number}}</td>

            <td style="text-align:center">{{$v->is_used}}</td>
            <td style="text-align:center">{{ date('d-m-y', strtotime($v->installation_date))}}</td>
            <td style="text-align:center">@if ( $v->shipment_date){{date('d-m-y', strtotime($v->shipment_date)) }}@else{{ $v->shipment_date}}@endif</td>
      
          
       
        
            @can('edit')

          
            <td style="text-align:center">   
          
           <a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('cabinets.edit',$v->id)}}"></a> 
         
               
            
                </td>
                @endcan
                @can('admin')

                <td style="text-align:center">   
                <button type="button" id="edit_todos" data-id="{{$v->id }}" class="btn btn-outline-success" >Reshuffling</button></td>
                  
                                    
                                    <td style="text-align:center">


<button type="button" id="edit_todo" data-id="{{$v->id }}" class="btn btn-outline-success" >dismantle</button></td>              
          @endcan
            @endforeach
        </table>
     
     
        
        

                
                </div>
               
            </div>

                  <!-- The Modal -->
       <div class="modal" id="modal_todos">
            <div class="modal-dialog">
              <div class="modal-content">
                <form id="form_todos">
                    <div class="modal-header">
                      <h4 class="modal-title" id="modals_title"></h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
               
                    <p>Enter the new site:</p>
                 
                    <input type="hidden" id="id" name="id" class="form-control" placeholder="Number" required>
                   <input type="number" name="trx" id="name_todos"   class="form-control" placeholder="TRX" required>
                   <input type="text" name="site_code" id="name_todoos"  class="form-control"  placeholder="Site code" required>
                   <span id="error-message" style="color: red; display: none;">* the site is not available *</span>
                 
                   <input type="hidden"  name="action_name"  value="reshuffling" required>

                   <input type="date" name="installation_date"    class="form-control" placeholder="installation_date" required>

                   
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button   id="demo" class="btn btn-info">Reshuffling</button>
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
       
               
            </div>
        </div>
       
                    </div>
        
        

                
                </div>
               
            </div>
        </div>

        


    </div>

    
      



    </div>





       <!-- The Modal -->
       <div class="modal" id="modal_todo">
            <div class="modal-dialog">
              <div class="modal-content">
                <form id="form_todo">
                    <div class="modal-header">
                      <h4 class="modal-title" id="modal_title"></h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
               
                 
                    <input type="hidden" name="action_name" class="form-control"  value="dismantle">

                    
                    <input type="hidden" id="g_id" name="id" class="form-control" placeholder="Number" required>
                   <input type="text" name="rm" id="name_todo" class="form-control" placeholder="RM" required>
                   <input type="date" name="dismantle_date"  class="form-control" required>
              
                   
<select name="warehouse_name"   class="form-control" id="name_todoo" required>
<option value="" >  Select warehouse  </option>   
                    @foreach($warehouses as $warehouse)
                               
  <option value="{{ $warehouse->name}}" >{{$warehouse->name}}  </option>

  @endforeach
 
</select>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-info">Dismantle</button>
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>





                





                

                


                <script type="text/javascript">
                  $(document).ready(function(){
                    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

                  });
      $("#add_todo").on('click',function(){
          $("#form_todo").trigger('reset');
          $("#modal_title").html('Add todo');
          $("#modal_todo").modal('show');
          $("#id").val("");
      });
                  $("body").on('click','#edit_todo',function(){
                      var id = $(this).data('id');
                      $.get('/cabinetdismantle/'+id+'/e',function(res){
                          $("#modal_title").html('Return to warehouse');
                          $("#g_id").val(res.id);
                          $("#name_todo").val(res.rm);
                          $("#name_todoo").val(res.warehouse_name);
                          $("#modal_todo").modal('show');
                      });
                  });
                  // Delete Todo

                  //save data

                  $("form").on('submit',function(e){
                      e.preventDefault();
                      $.ajax({
                          url:"/cabinetdismantle/s",
                          data: $("#form_todo").serialize(),
                          type:'POST'
                      }).done(function(res){
                          var row = '<tr id="row_todo_'+ res.id + '">';
                          row += '<td width="20">' + res.id + '</td>';
                          row += '<td>' + res.site_code + '</td>';
                          row += '<td width="150">' + '<button type="button" id="edit_todo" data-id="' + res.id +'" class="btn btn-info btn-sm mr-1">Edit</button>' + '<button type="button" id="delete_todo" data-id="' + res.id +'" class="btn btn-danger btn-sm">Delete</button>' + '</td>';
                          if($("#id").val()){
                              $("#row_todo_" + res.id).replaceWith(row);

                              setTimeout(function () { // wait 2 seconds and reload
                                              window.location.reload(true);
                                          }, 2000);

                              
                          }else{
                              $("#list_todo").prepend(row);
                          }
                          $("#form_todo").trigger('reset');
                          $("#modal_todo").modal('hide');
                      });
                  });
              </script>





<script type="text/javascript">
                  $(document).ready(function(){
                    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

                  });
      $("#add_todo").on('click',function(){
          $("#form_todos").trigger('reset');
          $("#modals_title").html('Add todo');
          $("#modal_todos").modal('show');
          $("#id").val("");
      });
                  $("body").on('click','#edit_todos',function(){
                      var id = $(this).data('id');
                      $.get('/cabinetreshuffling/'+id+'/e',function(res){
                          $("#modals_title").html('Move the cabinet');
                          $("#id").val(res.id);
                          $("#name_todos").val(res.trx);
                          $("#name_todoos").val(res.site_code);
                          $("#modal_todos").modal('show');
                      });
                  });
                  // Delete Todo

                  //save data

                  document.getElementById("demo").addEventListener("click",function(e){
                      e.preventDefault();
                      $.ajax({
                          url:"/cabinetreshuffling/s",
                          data: $("#form_todos").serialize(),
                          type:'POST'
                      }).done(function(res){
                          var row = '<tr id="row_todo_'+ res.id + '">';
                          row += '<td width="20">' + res.id + '</td>';
                          row += '<td>' + res.site_code + '</td>';
                          row += '<td width="150">' + '<button type="button" id="edit_todos" data-id="' + res.id +'" class="btn btn-info btn-sm mr-1">Edit</button>' + '<button type="button" id="delete_todo" data-id="' + res.id +'" class="btn btn-danger btn-sm">Delete</button>' + '</td>';
                          if($("#id").val()){
                              $("#row_todo_" + res.id).replaceWith(row);
                              setTimeout(function () { // wait 3 seconds and reload
                                              window.location.reload(true);
                                          }, 2000);
                          }else{
                              $("#list_todo").prepend(row);
                          }
                          $("#form_todos").trigger('reset');
                          $("#modal_todos").modal('hide');
                      });
                  });
              </script>


            </div>
        </div>
    </div>










    <script>


$(document).ready(function() {


    


var table = $('#example').DataTable( {
       
   orderCellsTop: true,
   fixedHeader: true,
   dom: 'Bfrtip',       
   buttons: [
           
           {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'csv',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'pdf', 
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'copy',
                       exportOptions: {
                           columns: ':visible'
                       }},
                      'colvis' ,
                     
          ],
     
columns: [
    {data: 'id', name: 'id',"visible": true},
  {data: 'site_code', orderable: false,},
  {data: 'Generator Name', orderable: false},
  {data: 'Engine Brand', orderable: false},
  {data: 'numbers', orderable: false},
  {data: 'solar', orderable: false},
  {data: 'Installation date', orderable: false},


  @can('edit')
  {  data: 'x', orderable: false },
  @endcan
  @can('admin')
  {  data: 'r', orderable: false },
  {  data: 'y', orderable: false },


  @endcan

        ]
});
});







</script>

<script>

    $(document).ready(function(){

        $('#name_todoos').blur(function() {

            var siteCode = $(this).val();
            $.ajax({
            url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("name_todoos").value="";

              }
            
    },

        });
    });});
</script>
<script>


$('#name_todoos').on('change', function(){
        var siteCode = $(this).val();
        var option = '<option value="">Select</option>';
        $('#cabinet_id').html(option);
        $('#solar').html(option);
        $.ajax({
            
    url: "{{ route('cheack_cabinet') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
                var option = '<option value="">Select cabinet</option>';


           
           for (var i=0;i<response.length;i++){

      
    option += '<option value="'+ response[i].id + '">' + response[i].id + '</option>';
       $('#cabinet_id').html(option);
    }
  
   

              
            
    },

        });

        $.ajax({
            
            url: "{{ route('cheack_solar') }}",
                    method: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        site_code: siteCode
                    },
                    dataType:'json',
                 
                    success: function(response) {
                        var option = '<option value="">Select solar</option>';
                
        
                   
                   for (var i=0;i<response.length;i++){
        
              
            option += '<option value="'+ response[i].id + '">' + response[i].id + '</option>';
               $('#solar').html(option);
            }
          
           
        
                      
                    
            },
        
                });
        





        });
</script>
@endsection