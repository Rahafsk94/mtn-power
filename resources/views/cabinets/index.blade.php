@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
        <h3 class="header">Cabinets Information</h3>
      
    </center>
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                         @can('create')
                            <a href="{{route('cabinets.create')}}">    <button type="button" class="btn btn-outline-success" >Add new cabinet</button></a>
                            @endcan
                            @can('track')
                            <a href="{{route('cabinet_track')}}">    <button type="button" class="btn btn-outline-success" >Track</button></a>
                            @endcan
                            @can('admin')
                            <a href="{{route('cabinetactions')}}">    <button type="button" class="btn btn-outline-success" >Action</button></a>
                            @endcan
                            <a href="{{route('cabinetwarehouse')}}">    <button type="button" class="btn btn-outline-success" >Cabinet in warehouse</button></a>
                        </h4>
               
                        <h6>
   
                          
                        <div class="dropdown">
  <button class="btn btn-outline-danger" >Cabinet Actions</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="/cabinet/install">Installation</a>
    <a href="/cabinet/reshuffling">Reshuffling</a>
    <a href="/cabinet/dismantle">Dismantle</a>
  </div>
</div>
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >ID</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site code</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Type</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Status</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Oracle code</span> </th>
            

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Installation date</span> </th>
            @can('delete')
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Delete</span> </th>
            @endcan
            @can('edit')
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span> </th>
            @endcan
            <tbody>
            @foreach ($cabinets as $cabinet)
            <tr>
            <td style="text-align:center">{{$cabinet->id}}</td>
            <td style="text-align:center">{{$cabinet->site->site_code}}</td>
            <td style="text-align:center">{{$cabinet->cabinet_name}}</td>
            <td style="text-align:center">{{$cabinet->is_used}}</td>
            <td style="text-align:center">{{$cabinet->oracle_code}}</td>
   
            <td style="text-align:center">{{$cabinet->installation_date}}</td>

            @can('delete')

            <td style="text-align:center">

            <form action="{{route('cabinets.destroy',$cabinet->id)}}" method="Post">
          
                    @csrf
                    @method('DELETE')
                    <button type="submit" onclick="return confirm('Are you sure you want to delete this item?');" class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" ></button>
                    </form>

</td>

@endcan
@can('edit')  

<td style="text-align:center">
          <a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('cabinets.edit',$cabinet->id)}}"></a> 
        </td>
        @endcan
                    
                
                
                


            @endforeach
        </table>
     






             <script>


    $(document).ready(function() {
   

        
   

   var table = $('#example').DataTable( {
           
       orderCellsTop: true,
       fixedHeader: true,
       dom: 'Bfrtip',       
buttons: [


    {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }},
    'colvis'

],

columns: [
{data: 'id', name: 'id'},
{data: 'site_code', orderable: false,},
{data: 'type', orderable: false,},
{data: 'used', orderable: false},
{data: 'oracle_code', orderable: false},

{data: 'date', orderable: false},
@can('delete')
{data: 'Delete', orderable: false},
@endcan
@can('edit')
{data: 'Edit', orderable: false},
@endcan


]
   });
});




  </script>
 
 
        </div>
                </div>
            </div>
        </div>
    </div>


</div>


@endsection