@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif

<form action="{{route('cabinets.store')}}" method="POST">
    @csrf
    <div class="raw">
    <div class="column">

    <div class="form-group">
                <strong>Site code:</strong>
                <input type="text" name="site_code" class="form-control" placeholder="Site code" id="site_code" required>
                <span id="error-message" style="color: red; display: none;">* the site is not available *</span>
            </div>
<div class="form-group">
            <strong>Cabinet type:</strong>
            <input type="text" name="cabinet_name" class="form-control" placeholder="Cabinet Name" required>

        </div>
    


            <strong>Status</strong> <br> 
        <select name="is_used"  class="form-control" required>
        <option value="">Select status</option>  
  <option value="used">Used</option>
  <option value="new">New</option>

</select>
            </div>
            
            <div class="column">
            <div class="form-group">
            <strong>Oracle Code  :</strong>
            <input type="text" name="oracle_code" class="form-control" placeholder="Oracle code" required>

        </div>
        <div class="form-group">
                <strong>Installation date:</strong>
                <input type="date" name="installation_date" class="form-control"  required>

            </div>
            <div class="form-group">
                <strong>PR:</strong>
                <input type="number" name="pr" class="form-control"  required>

            </div>

</div>
<div class="pull-right">
    <button class="button">create</button>
   </div>
</div>




<script>

        $(document).ready(function(){

                $("#site_code").on('change', function(){
        var siteCode = $(this).val();

$.ajax({
        url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("site_code").value="";

              }
            
    },
});

});
        });
</script>
      
   

   

     

@endsection