@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<form action="{{route('cabinets.update',$cabinet->id)}}" method="POST">
    @csrf
    @method('PUT')
    @if ($errors->any())


    <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
       </ul>
    </div>

    @endif
<div class="raw">
    <div class="column">

      


        
            <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                <strong>Cabinet name:</strong>
              
                <input type="text" name="cabinet_name" 

value="{{$cabinet->cabinet_name}}"
class="form-control" required>
            </div>
            </div>
       
           
            </div>
            <div class="column">
     
            <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                <strong>Oracle code:</strong>
              
                <input type="text" name="oracle_code"

value="{{$cabinet->oracle_code}}"
class="form-control" required>
            </div>
            </div>
          

       
        
</div>

<div class="pull-right">
<button class="button" type="submit" >Update</button>
</div>
</form>
@endsection