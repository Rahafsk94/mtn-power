<button Type="submit" >  <a href="siteexport"></a>export</button>
<table class="table table-bordered">

    <tr>

      <th scope="col">Site Code</th>
      <th scope="col">Finance Code</th>
      <th scope="col">Site name</th>
      <th scope="col">Priority</th>
      <th scope="col">Area</th>
      <th scope="col">Location</th>
      <th scope="col">Sub location</th>
      <th scope="col">Oracle Location</th>
      <th scope="col">province</th>
      <th scope="col">TX Category</th>
      <th scope="col">Installation date</th>
      <th scope="col">End date</th>
      <th scope="col">Band</th>
      <th scope="col">Zone</th>
      <th scope="col">C_R</th>
      <th scope="col">Supplier</th>
      <th scope="col">Coordinates_E</th>
      <th scope="col">Coordinates_N</th>
    </tr>
    <tbody>
  
    @foreach ($site as $item)
    <tr>

        <td>{{$item->site_code}}</td>
        <td>{{$item->finance_code}}</td>
        <td>{{$item->site_name}}</td>
        <td>{{$item->priority}}</td>
        <td>{{$item->area}}</td>
        <td>{{$item->location}}</td>
        <td>{{$item->sub_location}}</td>
        <td>{{$item->oracle_location}}</td>
        <td>{{$item->province}}</td>
        <td>{{$item->tx_category}}</td>
        <td>{{$item->installation_date}}</td>
        <td>{{$item->end_date}}</td>
        <td>{{$item->band}}</td>
        <td>{{$item->zone}}</td>
        <td>{{$item->c_r}}</td>
        <td>{{$item->supplier}}</td>
        <td>{{$item->coordinates_e}}</td>
        <td>{{$item->coordinates_n}}</td>
        @endforeach
    </tbody>
</table>