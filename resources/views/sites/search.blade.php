@extends('layouts.app')
@section('content')


<form action="{{route('sitesearch')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="col-md-2">
                        <input type="text" class="form-control input-sm"name="sitesearch">
                        </div>
                        <div class="col-md-4">
                        <button type="submit" class="btn btn-primary btn-sm" name="exportExcel" >export</button>
                        </div>
</div>
</div>
        
</form>
<div class="card">
<div class="card-header">
    <h4>

        <div class="pull-right">
<button class="btn btn-outline-secondary"  id="export" >Export Now</button>


</h4>
</div>

@if($site->isNotEmpty())
<div class="card-body">

<table id="tableData"  class="table table-bordered table-striped">
    <thead>
        <tr>




      <th scope="col">Site Code</th>
      <th scope="col">Finance Code</th>
      <th scope="col">Site name</th>
      <th scope="col">Priority</th>
      <th scope="col">Area</th>
      <th scope="col">Location</th>
      <th scope="col">Sub location</th>
      <th scope="col">Oracle Location</th>
      <th scope="col">province</th>
      <th scope="col">TX Category</th>
      <th scope="col">Installation date</th>
      <th scope="col">End date</th>
      <th scope="col">Band</th>
      <th scope="col">Zone</th>
      <th scope="col">C_R</th>
      <th scope="col">Supplier</th>
      <th scope="col">Coordinates_E</th>
      <th scope="col">Coordinates_N</th>
    </tr>

  <tbody>
    @foreach ($site as $item)
    <tr>

        <td>{{$item->site_code}}</td>
        <td>{{$item->finance_code}}</td>
        <td>{{$item->site_name}}</td>
        <td>{{$item->priority}}</td>
        <td>{{$item->area}}</td>
        <td>{{$item->location}}</td>
        <td>{{$item->sub_location}}</td>
        <td>{{$item->oracle_location}}</td>
        <td>{{$item->province}}</td>
        <td>{{$item->tx_category}}</td>
        <td>{{$item->installation_date}}</td>
        <td>{{$item->end_date}}</td>
        <td>{{$item->band}}</td>
        <td>{{$item->zone}}</td>
        <td>{{$item->c_r}}</td>
        <td>{{$item->supplier}}</td>
        <td>{{$item->coordinates_e}}</td>
        <td>{{$item->coordinates_n}}</td>
        
        <td style="text-align:center">

<form action="{{route('sites.destroy',$item->id)}}" method="Post">

        @csrf
        @method('DELETE')
        <button type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" ></button>
        </form>

</td>

<td style="text-align:center">
<a class='fa fa-edit' style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('sites.edit',$item->id)}}"></a> 
</td>
<td style="text-align:center">
        <a class='fa fa-eye'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('sites.show',$item->id)}}"></a>
    </td>
        @endforeach

    @else
        <div>
            <center>
            <h2>No sites found</h2></center>
        </div>

        @endif
      
</table>
</div>
<script>
 
 document.getElementById('export').onclick=function(){
        var tableData= document.getElementById('tableData').id;
        htmlTableToExcel(tableData, filename = '');
    }


   var htmlTableToExcel= function(tableData, fileName = ''){

    var excelFileName='excel_table_data';
    var TableDataType = 'application/vnd.ms-excel';
    var selectTable = document.getElementById(tableData);
    var htmlTable = selectTable.outerHTML.replace(/ /g, '%20');
    
    filename = filename?filename+'.xlsx':'excel_data.xlsx';
    var excelFileURL = document.createElement("a");
    document.body.appendChild(excelFileURL);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', htmlTable], {
            type: TableDataType
        });
        navigator.msSaveOrOpenBlob( blob, fileName);
    }else{
        
        excelFileURL.href = 'data:' + TableDataType + ', ' + htmlTable;
        excelFileURL.download = fileName;
        excelFileURL.click();
    }
}
</script>




</body>
@endsection

