<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('title')
    </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script>
<script src="public\cusom.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/jquery.table2excel.min.js"></script>
<script src="./node_modules/jquery/dist/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2014-11-29/FileSaver.min.js"></script>
    <script type="text/javascript" src="../dist/tableToExcel.js"></script>
    <link href="./node_modules/@grapecity/spread-sheets/styles/gc.spread.sheets.excel2013white.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="./node_modules/@grapecity/spread-sheets/dist/gc.spread.sheets.all.min.js"></script>
    <script type="text/javascript" src="./node_modules/@grapecity/spread-excelio/dist/gc.spread.excelio.min.js"></script>    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css'>  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="colorlib.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />



   <style>












.navbar {
    height: 80px;
    background-color: #F9D401;
    border: 1px solid red;
}


p{
    color: #00678f;
    font-family: cursive;
    font-size: 2ch;
}
table {
    border: 3px solid gray;
   
}
th,td{
    border:  1px solid gray;
    height: 1px;
 
  
}
.row.mb-3{
    color: #F9D401;
}
.card-header{
    color: #F9D401;
}
.btn.btn-primary{
    background-color: #F9D401;
    border: #F9D401;

}
.header{
    text-align: center;
    margin :20px;
    color: #00678f;
}
.btn.btn-link{
    color:#F9D401;
}
.form-control:focus{
    border-color:#F9D401;
    box-shadow: 0 0 0 0.2rem #F9D401;
}

.i{
    padding: 100px;
}
.c{
    margin-RIGHT:40PX;
    margin-left:30px;
    width:400px;
}

.dropdown-item{
    color:#F9D401;
box-shadow: #F9D401;

}
.btn.btn-success{
margin-left: 5px;
height: 50px;
    background-color:#F9D401;
    font-size: 18px;
     border-color: #F9D401;
}

.bi.bi-trash {
  background: transparent;
  border: none;
  border-radius: 2em;
  color: #f00;
  display: inline-block;
  height: 20px;
  line-height: 2px;
  margin: 5px;
  padding: 0;
 

}


.btn-btn-primary{

    margin-top: 3px;
}
.column {
    float: left;

padding: 10px;

width: 50%;

}
.card{
    margin-top: 30px;
    margin-left: 7%;

}
.create{
    margin-left: 400px;
    margin-right: 400px;
}
.button {
  background-color:#00678f;
  border: none;
  color: white;
width: 100px;
height: 50px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  padding: 4px;
  margin: 11px;
}

.form-group{
    padding: 7px;
}
.export {
margin-right:27px;
margin-top:33px;
float: right;
}
.con{
   
margin-right:50px;
margin-top:40px;
float: right;
}
.cont{
    width:100px;
}

.cont.collapsible {
  background-color: #777;
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 10px;;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
}

.cont.active, .collapsible:hover {
  background-color: #555;
}
.cont{
    width:100%;
}
.cont.collapsible:after {
  content: '\002B';
  color: white;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.cont.active:after {
  content: "\2212";
}

.content {
 
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
  background-color: #f1f1f1;
}
.


   </style>

</head>
<body>
    <nav class="navbar navbar-expand-lg ">
        <img src="/mtn.png" alt="MTN" width="120" height="65">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="btn btn-success" href="/home"><p>Home</p></a>
        </li>         <li><a class="btn btn-success" href="/sites"><p>All Sites</p></a></li>
                <li> <a class="btn btn-success" href="{{route('sites.create')}}"><p>Create new Sites</p></a>
                </li>

            </ul>


        </div>

            <div class="col-sm-3 col-md-3 pull-right">

                <form action="{{ route('sitesearch') }}" method="GET">

                   <input type="text" placeholder="Search by site.." name="sitesearch">
                    <button type="submit">Search</button>
</form>

                </div>


            </div>

        </div>
    </div>
</div>
</nav>

<div class="container1">
    
<h3 class="header">Sites Informations <br><br>
<a href="generator/export">       <button type="button" class="btn btn-warning">export</button></a>
</h3>


</div>
                
<div class="dropdown">
  <button class="dropbtn">Dropdown</button>
  <div class="dropdown-content">
  <a href="sites">Link 1</a>
  <a href="#">Link 2</a>
  <a href="#">Link 3</a>
  </div>
</div>
        
@yield('content')
</body>

</html>



