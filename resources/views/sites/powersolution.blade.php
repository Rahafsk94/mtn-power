@extends('layouts.app')
@section('content')


<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            
All sites with its power solution
                            <div class="pull-right">

</div>
                        </h4>
               
                        <h6>
                    
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site Code</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Battery </span> </th>
           
            


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Ampere</span> </th>


            
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Solar</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Tank</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Cabinet</span> </th>
         </tr>
        <tbody>
            @foreach ($solution as $v)
            <tr>
          
            
            <td style="text-align:center">{{$v->site_code}}</td>
            <td style="text-align:center">{{$v->generator}}</td>
            <td style="text-align:center">{{$v->battery}}</td>
            <td style="text-align:center">{{$v->ampere}}</td>
            <td style="text-align:center">{{$v->solar}}</td>
            <td style="text-align:center">{{$v->tank}}</td>
            <td style="text-align:center">{{$v->cabinet}}</td>
            @endforeach
        </table>
     
<script>

    
$(document).ready(function() {


    


var table = $('#example').DataTable( {
       
   orderCellsTop: true,
   fixedHeader: true,
   dom: 'Bfrtip',       
   buttons: [
           
           {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'csv',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'pdf', 
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'copy',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       
                       'colvis'
          ],

columns: [
  
  {data: 'site_code', orderable: false,},
  {data: 'generator', name: 'generator'},
  {data: 'battery', orderable: false},
  {data: 'ampere', orderable: false},
  {data: 'solar', orderable: false},
  {data: 'tank', orderable: false},
  {data: 'cabinet', orderable: false},

        ]
});
});

</script>
@endsection




