@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<span id="messageSpan"></span>

<center>
        <h3 class="header">  <p> Sites Information</p></h3>
        
      
    </center>

<div class="row">
 
            <div class="col-md-12">
                <div class="card" >
                    <div class="card-header">
                  
                        <h4>
                        <div class="u">
                            <div class="pull-right">
                                @can('admin')
                            <a href="siteexport">       <button type="button" class="btn btn-outline-success">Export</button></a>
                            @endcan
                            @can('track')
                            <a href="site/track">    <button  class="btn btn-outline-success" type="submit">Site Actions</button></a>
                            @endcan
                            @can('create')
                                <a href="{{route('sites.create')}}">    <button type="button" class="btn btn-outline-success" >Add New Site</button></a>
                                @endcan
                                <a href="powersolution">    <button  class="btn btn-outline-success" type="submit">Power Solution</button></a>
                              
                    

</div>
                        </h4>

                        <h6>      
                  
      
        
        
                        </h6>
                 
                    </div>
                          
                    <div class="card-body">
                    <div class="cont">
    
    <table  class="table table-bordered table-striped data-table" width="100%">
        <thead>
            <tr>
                
                <th>Site Code</th>
            
                <th>Site Name</th>
                <th>Priority</th>
                <th>Area</th>

                
               
                <th>On/Off/Air</th>
                <th>Rationning hours</th>
   
          
                <th>Supplier</th>
                
                <th>Tx category</th>
                
                
             
                <th>Province</th>
                <th>Zone</th>
                <th>Finance Code</th>
                <th>Band</th>	
                <th>Location</th>
                <th>Oracle location</th>
                <th>Sub location</th>
                <th>Coordinates E</th>
                <th>Coordinates N</th>
                <th>C R</th>
                <th>Installation date</th>
                @can('admin')    
                <td>  <p>Edit</p> </td>
                @endcan
                @can('delete')  
                <td>  <p>Delete</p> </td>
                @endcan
            </tr>

        </thead>
        <tbody>
       
        </tbody>
    </table>
</div>            
    
</div>  

<script>
   $(document).ready(function() {
   
            $('.data-table thead tr').clone(true).appendTo( '.data-table thead' );
            $('.data-table thead tr:eq(1) th').each( function (i) {

                var title = $(this).text();

                $(this).html( '<input type="text" placeholder=" Search '+title+'" />' );
              
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                         .column(i)
                            .search( this.value )
                            .draw();
                    }
                });
            });
   
            var table = $('.data-table').DataTable( {
        
                orderCellsTop: true,
                fixedHeader: true,
       
                dom: 'Bfrtip',       
        buttons: [
           
    {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'csv',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'pdf', 
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'copy',
                exportOptions: {
                    columns: ':visible'
                }},
                
                'colvis'
   ],
   columnDefs: [ {
            targets: [-1,-2,-3,-4,-5,-6,-7,-8,-9,-10],
            visible: false,
      
        } ],
        
        ajax: "{{ route('sites.index') }}",
        columns: [
          
            {data: 'site_code', name: 'site_code', orderable: false},
            
            {data: 'site_name', name: 'site_name', orderable: false},
            {data: 'priority', name: 'priority', orderable: false},
            {data: 'area', name: 'area', orderable: false},
           
            {data: 'on_off_air', name: 'on_off_air', orderable: false},   
            {data: 'rationning_hours', name: 'rationning_hours', orderable: false},   
            
        
            {data: 'supplier', name: 'supplier', orderable: false},
           
            {data: 'tx_category', name: 'tx_category', orderable: false},      
          
          
       
            {data: 'province', name: 'province', orderable: false},
            {data: 'zone', name: 'zone', orderable: false},
            {data: 'finance_code', name: 'finance_code', orderable: false},
            {data: 'band', name: 'band', orderable: false},
            {data: 'location', name: 'location', orderable: false},
            {data: 'oracle_location', name: 'oracle_location', orderable: false}, 
            {data: 'sub_location', name: 'sub_location', orderable: false},
            {data: 'coordinates_e', name: 'coordinates_e', orderable: false},     
            {data: 'coordinates_n', name: 'coordinates_n', orderable: false}, 
            {data: 'c_r', name: 'c_r', orderable: false},      
            {data: 'installation_date', name: 'installation_date', orderable: false, searchable: false},   
            @can('admin')
            {data: 'action', name: 'action', orderable: false, searchable: false},
            @endcan
            @can('delete')
            {data: 'delete', name: 'action', orderable: false, searchable: false},
            @endcan
        ]
    
 
            });




            
            
        });
        
        
    </script>
   <script>

function myFunction($id) {



var id =$id
$.ajax({
    
    url: '/site/destroy/'+$id,
            type: 'POST',
            data:{"id": id,
                
                "_token": "{{ csrf_token() }}"},
            dataType:'json',
         
            success: function(response) {
          
                document.getElementById('messageSpan').innerHTML = "<div class='container'><div class='alert alert-danger' role='alert'>The Site has been deleted.</div></div>"; 
                setTimeout(function () { // wait 3 seconds and reload
    window.location.reload(true);
  }, 4000);
    },

        });

}
   </script>
@endsection
