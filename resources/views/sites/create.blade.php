@extends('layouts.app')
@section('content')
@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif

    <form action="{{route('sites.store')}}" method="POST">
        @csrf
    <div class="raw">
        <div class="column">

                <div class="form-group">
                    <strong>Site Code:</strong>
                    <input type="text" name="site_code" class="form-control" placeholder="Site Code" required id="site_code">
                    <span id="error-message" style="color: red; display: none;">* the Site Code is incorrect*</span>
                </div>

                <div class="form-group">
                    <strong>Site Name:</strong>
                   <input type="text" name="site_name" class="form-control" placeholder="Site Name" id="site_name" required>
           
                </div>
     
                <div class="form-group">
                    <strong>Area:</strong>
                     <input type="text" name="area" class="form-control" placeholder="Area" required>
                    </div>
                    <div class="form-group">
                        <strong>Oracle Location:</strong>
                      <input type="text" name="oracle_location" class="form-control" placeholder="Oracle Location" required>

                    </div>
                    <div class="form-group">
                        <strong>province:</strong>
                       <input type="text" name="province" class="form-control" placeholder="Province"required>
                    </div>


    <div class="form-group">
        <strong>Band:</strong>
       <input type="text" name="band" class="form-control" placeholder="Band" required>
        </div>

        <div class="form-group">
            <strong>C_R:</strong>
           <input type="text" name="c_r" class="form-control" placeholder="C_R" required>

    </div>
    <div class="form-group">
        <strong>Coordinates E:</strong>
       <input type="text" name="coordinates_e" class="form-control" placeholder="Coordinates E" required>

</div>
<div class="form-group">
   <strong>Is site on air?</strong> 
    <select class="form-control" name="on_off_air" id="on_off_air" required>
        <option value="">Select</option>
        <option value="1" >True</option>
        <option value="0">False</option>
    </select>

</div>
</div>

                <div class="column">

                    <div class="form-group">
                        <strong>Finance Code:</strong>
                        <input type="text" name="finance_code" class="form-control" placeholder="Finance Code" required>

                    </div>
  <div class="form-group">
     <strong>Priority:</strong>
     <input type="number" name="priority" class="form-control" placeholder="Priority" required>
                </div>

                <div class="form-group">
                    <strong>Location:</strong>
                     <input type="text" name="location" class="form-control" placeholder="Location" required>

        </div>

        <div class="form-group">
            <strong>Sub Location:</strong>
               <input type="text" name="sub_location" class="form-control" placeholder="Sub Location" required>

    </div>

    <div class="form-group">
        <strong>TX Category:</strong>
       <input type="text" name="tx_category" class="form-control" placeholder="TX Category" required>
    </div>



    <div class="form-group">
        <strong>Rationning hours:</strong>
       <input type="number" name="rationning_hours" class="form-control" placeholder="rationning hours" required>
    </div>

    <div class="form-group">
        <strong>Zone:</strong>
       <input type="text" name="zone" class="form-control" placeholder="Zone" required>

</div>
<div class="form-group">
    <strong>Supplier:</strong>
   <input type="text" name="supplier" class="form-control" placeholder="Supplier" required>
</div>



<div class="form-group">
    <strong>Coordinates N:</strong>
   <input type="text" name="coordinates_n" class="form-control" placeholder="Coordinates N" required>

</div>

</div>

<div class="pull-right">
<br><br><br>
    <button class="button">create</button>
</div>
</form>
</div>
</div>
<script>
  $("#site_code").on('change', function(){
        var siteName = $(this).val();
       
$.ajax({
    url: '/sitecodename',
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_name: siteName
            },
            dataType:'json',
         
            success: function(response) {
           if(response){
            
           }
              if(response.a){
                $('#error-message').show();
                      
            document.getElementById("site_code").value="";
              }
              else{
             
                $('#error-message').hide();


              }
            
    },

        });});

</script>
@endsection
