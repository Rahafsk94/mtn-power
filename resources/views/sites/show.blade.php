@extends('sites.layout')
@section('content')
<form action="{{route('sites.show',$Site->id)}}" method="GET">
    @csrf
    <div class="container">
    <table class="table table-bordered">
        <tr>
          <th scope="col">Site Code</th>
          <th scope="col">Finance Code</th>
          <th scope="col">Site name</th>
          <th scope="col">Priority</th>
          <th scope="col">Area</th>
          <th scope="col">Location</th>
          <th scope="col">Sub location</th>
          <th scope="col">Oracle Location</th>
          <th scope="col">province</th>
          <th scope="col">TX Category</th>
          <th scope="col">Installation date</th>
          <th scope="col">End date</th>
          <th scope="col">Band</th>
          <th scope="col">Zone</th>

          <th scope="col">C_R</th>
          <th scope="col">Supplier</th>
          <th scope="col">Coordinates_E</th>
          <th scope="col">Coordinates_N</th>
        </tr>
        <tr>
                <td>{{$Site->site_code}}</td>
                <td>{{$Site->finance_code}}</td>
                <td>{{$Site->site_name}}</td>
                <td>{{$Site->priority}}</td>
                <td>{{$Site->area}}</td>
                <td>{{$Site->location}}</td>
                <td>{{$Site->sub_location}}</td>
                <td>{{$Site->oracle_location}}</td>
                <td>{{$Site->province}}</td>
                <td>{{$Site->tx_category}}</td>
                <td>{{$Site->installation_date}}</td>
                <td>{{$Site->end_date}}</td>

                <td>{{$Site->band}}</td>
                <td>{{$Site->zone}}</td>
                <td>{{$Site->c_r}}</td>
                <td>{{$Site->supplier}}</td>
                <td>{{$Site->coordinates_e}}</td>
                <td>{{$Site->coordinates_n}}</td>
            </tr>
                </div>
</form>
@endsection
