@extends('layouts.app')
@section('content')


<form action="{{route('sites.update',$site->id)}}" method="POST">
    @csrf
    @method('PUT')
    @if ($errors->any())


    <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
       </ul>
    </div>

    @endif
<div class="raw">
    <div class="column">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Site code:</strong>
              
                <input type="text" name="site_code" id="site_code"

value="{{$site->site_code}}"
class="form-control" placeholder="Site code">
<span id="error-message" style="color: red; display: none;">* the Site Code is incorrect*</span>
            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Finance code:</strong>
                <input type="text" name="finance_code"

                value="{{$site->finance_code}}"
                class="form-control" placeholder="Generator Name">

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Site name:</strong>
                <input type="text" name="site_name"
                value="{{$site->site_name}}"
                class="form-control" placeholder="Generator capacity">

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>priority:</strong>
                <input type="number" name="priority"
                value="{{$site->priority}}"
                class="form-control" placeholder="priority">

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Area:</strong>
                <input type="text" name="area"
                value="{{$site->area}}"
                class="form-control" placeholder="Area">

            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Rationning hours:</strong>
            <input type="number" name="rationning_hours"
            value="{{$site->rationning_hours}}"
            class="form-control" placeholder="rationning_hours">

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">

    <div class="form-group">

    <strong>on_off_air:</strong>
    <select name="on_off_air"   class="form-control">
    <option  >  {{$site->on_off_air}} </option>

  <option value="1" >Yes</option>

  <option value="0">No</option>
</select>


            </div>


        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>location:</strong>
                <input type="text" name="location"

                value="{{$site->location}}"
                class="form-control" placeholder="location">

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong> Sub location:</strong>
                <input type="text" name="sub_location"

                value="{{$site->sub_location}}"
                class="form-control" placeholder="location">

            </div>


        </div>
    </div>
<div class="column">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Coordinates e:</strong>
            <input type="text" name="coordinates_e"
            value="{{$site->coordinates_e}}"
            class="form-control" placeholder="coordinates_e">

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Coordinates n:</strong>
            <input type="text" name="coordinates_n"
            value="{{$site->coordinates_n}}"
            class="form-control" placeholder="coordinates_n">

        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Band:</strong>
            <input type="text" name="band"
            value="{{$site->band}}"
            class="form-control" placeholder="Band">

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>zone:</strong>
            <input type="text" name="zone" class="form-control"  value="{{$site->zone}}">

        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>CR:</strong>
            <input type="text" name="c_r"
            value="{{$site->c_r}}"
            class="form-control" placeholder="c_r">

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Supplier:</strong>
                <input type="text" name="supplier"

                value="{{$site->supplier}}"
                class="form-control" placeholder="Supplier">

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Province:</strong>
                <input type="text" name="province"

                value="{{$site->province}}"
                class="form-control" placeholder="province">

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>tx_category:</strong>
            <input type="text" name="tx_category"
            value="{{$site->tx_category}}"
            class="form-control" placeholder="tx_category">

        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong> Oracle location:</strong>
                <input type="text" name="oracle_location"

                value="{{$site->oracle_location}}"
                class="form-control" placeholder="oracle_location">

            </div>


        </div>
</div>
<div class="pull-right">
<button class="button" type="submit" >Update</button>
</div>
</form>
</div>

<script>
  $("#site_code").on('change', function(){
        var siteName = $(this).val();
       
$.ajax({
    url: '/sitecodename',
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_name: siteName
            },
            dataType:'json',
         
            success: function(response) {
           if(response){
            
           }
              if(response.a){
                $('#error-message').show();
                      
            document.getElementById("site_code").value="";
              }
              else{
             
                $('#error-message').hide();


              }
            
    },

        });});

</script>
    @endsection
