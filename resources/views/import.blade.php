
@extends('layouts.app')
@section('content')
<br><br>


        
        <section class="s-pt-30 s-pb-3 service-item2 ls animate" id="services" data-animation="fadeInUp">
    <div class="container">
        <section class="s-pt-30 s-pb-3 service-item2 ls animate" id="services" data-animation="fadeInUp">
            <div class="container">
                <div class="i">

                <div class="row c-mb-80 c-mb-md-60">
                    <div class="d-none d-lg-block divider-20"></div>

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="vertical-item text-center">
                            <div class="item-media">
                            <img src="install.png" width="40" height="40" alt="">
                            </div>
                            <div class="item-content">
                                <h6>
                                <a href="{{route('generatorimport')}}"> <h10>Generator import</h10> </a>
                                         </a>
                                </h6>

                                <p>
                             Import the generator file. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- .col-* -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="vertical-item text-center">
                            <div class="item-media">
                            <img src="install.png" width="40" height="40" alt="">
                            </div>
                            <div class="item-content">
                                <h6>
                                <a href="{{route('batteryimport')}}"> <h10>Battery import</h10> </a>
                                         </a>
                                </h6>

                                <p>
                             Import the battery file. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- .col-* -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="vertical-item text-center">
                            <div class="item-media">
                            <img src="install.png" width="40" height="40" alt="">
                            </div>
                            <div class="item-content">
                                <h6>
                                <a href="{{route('siteimport')}}"> <h10>Site import</h10> </a>
                                         </a>
                                </h6>

                                <p>
                             Import the site file. 
                                </p>
                            </div>
                        </div>
                    </div>
        
                       
                </div>
              
            </div>
        
            </div>
            
        </section>
        <hr width="50%" color="green" size="50px" />

        
        <section class="s-pt-30 s-pb-3 service-item2 ls animate" id="services" data-animation="fadeInUp">
    <div class="container">
        <section class="s-pt-30 s-pb-3 service-item2 ls animate" id="services" data-animation="fadeInUp">
            <div class="container">
                <div class="i">

                <div class="row c-mb-80 c-mb-md-60">
                    <div class="d-none d-lg-block divider-20"></div>

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="vertical-item text-center">
                            <div class="item-media">
                            <img src="install.png" width="40" height="40" alt="">
                            </div>
                            <div class="item-content">
                                <h6>
                                <a href="{{route('ampereimport')}}"> <h10>Ampere import</h10> </a>
                                         </a>
                                </h6>

                                <p>
                             Import the ampere file. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- .col-* -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="vertical-item text-center">
                            <div class="item-media">
                            <img src="install.png" width="40" height="40" alt="">
                            </div>
                            <div class="item-content">
                                <h6>
                                <a href="{{route('solarimport')}}"> <h10>Solar import</h10> </a>
                                         </a>
                                </h6>

                                <p>
                             Import the solar file. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- .col-* -->
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="vertical-item text-center">
                            <div class="item-media">
                            <img src="install.png" width="40" height="40" alt="">
                            </div>
                            <div class="item-content">
                                <h6>
                                <a href="{{route('cabinetimport')}}"> <h10>Cabinet import</h10> </a>
                                         </a>
                                </h6>

                                <p>
                             Import the cabinet file. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- .col-* -->
            
                 
                    <!-- .col-* -->
                   

                    <!-- .col-* -->
             
                    
                    <!-- .col-* -->  
                  
                   
                   
                </div>
              
            </div>
        
            </div>
            
        </section>

        <hr width="50%" color="green" size="50px" />


        
    <div class="container">
        <section class="s-pt-30 s-pb-3 service-item2 ls animate" id="services" data-animation="fadeInUp">
            <div class="container">
                <div class="i">

                <div class="row c-mb-80 c-mb-md-60">
                    <div class="d-none d-lg-block divider-20"></div>

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="vertical-item text-center">
                         
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="vertical-item text-center">
                            <div class="item-media">
                            <img src="install.png" width="40" height="40" alt="">
                            </div>
                            <div class="item-content">
                                <h6>
                                <a href="{{route('tanksimport')}}"> <h10>Tank import</h10> </a>
                                         </a>
                                </h6>

                                <p>
                             Import the tank file. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- .col-* -->
            
                 
                    <!-- .col-* -->
                   

                    <!-- .col-* -->
             
                    
                    <!-- .col-* -->  
                  
                   
                   
                </div>
              
            </div>
        
            </div>
            
        </section>
@endsection