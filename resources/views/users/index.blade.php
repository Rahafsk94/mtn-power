@extends('layouts.app')

@section('content')



@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if(session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                    @endif
<center>
        <h3 class="header">Users Information</h3>


    </center>
<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">

                   
                               
@if (Route::has('register'))
                            <a class="btn btn-outline-success" href="{{ route('register') }}">{{ __('Register') }}</a>    
                            @endif
                        
                               
</div>
                        </h4>
               
                        <h6>

                       

                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Name</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Email</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Join date</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Role</span> </th>
            @can('edit')
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span> </th>
@endcan
@can('delete')
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Delete</span> </th>
@endcan
          
           
         </tr>
        <tbody>
            @foreach ($data as $user)
            <tr>
          
            <td style="text-align:center">{{$user->name}}</td>

            <td style="text-align:center">{{$user->email}}</td>
            <td style="text-align:center">{{$user->created_at}}</td>
            <td style="text-align:center">




@if(!empty($user->getRoleNames()))
        @foreach($user->getRoleNames() as $v)
        <label >{{ $v }}</label>
        @endforeach
        @endif
    



</td>
  @can('edit')
 <td style="text-align:center">
                <a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);"
                    href="{{ route('users.edit', $user->id) }}"></a>
</td>
            @endcan


 @can('delete')


<td style="text-align:center">   

<form action="{{route('users.destroy',$user->id)}}" method="Post">

@csrf
@method('DELETE')

<button type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" onclick="return confirm('Delete user?')" ></button>
</form>
  


@endcan
      
          
            @endforeach
        </table>
     
        
</form>


                    </div>
        
        

                
                </div>
               
            </div>
        </div>
    </div>


    <script>


$(document).ready(function() {


    


var table = $('#example').DataTable( {
       
   orderCellsTop: true,
   fixedHeader: true,
   dom: 'Bfrtip',       
   buttons: [
           
           {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'csv',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'pdf', 
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'copy',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       
                       'colvis'
          ],

columns: [
    {data: 'name', name: 'name'},
  {data: 'email', orderable: false,},
  {data: 'date', orderable: false,},
  {data: 'role', orderable: false},
@can('edit')
 {data: 'edit', orderable: false,},
 @endcan
 @can('delete')
  {data: 'delete', orderable: false},
  @endcan

        ]
});
});










</script>







@endsection




