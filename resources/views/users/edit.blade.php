@extends('layouts.app')
@section('content')

<form action="{{route('users.update',$user->id)}}" method="POST">

@csrf
    @method('PUT')

    @if ($errors->any())


<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
       @endforeach
   </ul>
</div>

@endif




<div class="raw">
  
  <div class="columnn">
  
      <div class="form-group">
          <strong>User name:</strong>
          <input type="text" name="name" value="{{$user->name}}" class="form-control" required>
  
      </div>
  </div>
  <div class="columnn">
      <div class="form-group">
              <strong>Email:</strong>
  
  
              <input type="email" name="email" value="{{$user->email}}" class="form-control"  required>
  
          </div>
          </div>
          <div class="columnn">
      <div class="form-group">
      

      <strong>User role:</strong>
<select class="form-control" name="roles" id="roles" required>

@foreach($userRole as $link)


    <option value="{{$user->getRoleNames() }}">{{$link}}</option>
    @endforeach
    @foreach($roles as $v)
      <option value="{{ $v}}" >{{ $v}}</option>

       
       @endforeach
    </select>









  
          </div>
          <div class="pull-right">
      <button class="button">Update</button>
     </div>
      </div>
  
  </div>
</form>
@endsection