@extends('layouts.app')
@section('content')

<form action="{{route('solars.update',$solar->id)}}" method="POST">
    @csrf
    @method('PUT')
    @if ($errors->any())


    <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
       </ul>
    </div>

    @endif
<div class="raw">
    <div class="column">

        
      
   
    <div class="col-xs-12 col-sm-12 col-md-12">

  


        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Panel capacity:</strong>
                <input type="number" name="panel_capacity"

                value="{{$solar->panel_capacity}}"
                class="form-control" required>

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>PO:</strong>
            <input type="number" name="po"
            value="{{$solar->po}}"
            class="form-control" required >

        </div>



        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Number of charger:</strong>
            <input type="number" name="number_of_charger"
            value="{{$solar->number_of_charger}}"
            class="form-control" required >

        </div>



        </div>
     
    </div>
<div class="column">

 
<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Panel brand:</strong>
                <input type="text" name="panel_brand"
                value="{{$solar->panel_brand}}"
                class="form-control" placeholder="Generator capacity" required>

            </div>


        </div>
<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Controller:</strong>
                <input type="text" name="controller"

                value="{{$solar->controller}}"
                class="form-control" placeholder="Controller" required>

            </div>


        </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Solar project:</strong>
            <input type="text" name="solar_project"
            value="{{$solar->solar_project}}"
            class="form-control" required>

        </div>
    </div>

 

</div>
<div class="pull-right">
<button class="button" type="submit" >Update</button>
</div>
</form>
</div>

          
    @endsection
