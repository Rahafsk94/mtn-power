@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
        <h3 class="header">Solars Information</h3>

    </center>
<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                        <h4>
                            <div class="pull-right">
                         
                            <a href="{{route('solars.create')}}">    <button type="button" class="btn btn-outline-success">Add new solar</button></a>
@can('admin')
                            <a href="{{route('solaractions')}}">    <button type="button" class="btn btn-outline-success">Add actions</button></a>
                            @endcan
                            @can('track')
                            <a href="{{route('solar_track')}}">    <button type="button" class="btn btn-outline-success">Track</button></a>
                            @endcan
                            <a href="{{route('solarswarehouse')}}">    <button type="button" class="btn btn-outline-success" >Warehouse</button></a>
                    </div>   

                        </h4>

                        <h6>
   
                        <div class="dropdown">
  <button class="btn btn-outline-danger" >Solar actions</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="{{route('solarinstall')}}">Installation</a>
    <a href="{{route('solarre')}}">Reshuffling</a>
    <a href="{{route('solardismantle')}}">Dismantle</a>
  </div>
</div>
 
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>
                                
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Solar ID</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site Code</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Panel numbers</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Panel brand</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Panel capacity</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Controller</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Battery</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Number of charger</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Status</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >PO</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Solar project</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" > Installation date</span> </th>
            @can('edit')
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span> </th>
            @endcan
            @can('delete')
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Delete</span> </th>
            @endcan
         </tr>
        <tbody>
            @foreach ($solar as $v)
            <tr>
            <td style="text-align:center">{{$v->id}}</td>
            <td style="text-align:center">{{$v->site->site_code}}</td>
            <td style="text-align:center">{{$v->number_of_panel}}</td>
            <td style="text-align:center">{{$v->panel_brand}}</td>
            <td style="text-align:center">{{$v->panel_capacity}}</td>
            <td style="text-align:center">{{$v->controller}}</td>
       
            <td style="text-align:center"> @foreach ($v->Battery as $y) <li> id ({{$y->id}})  numbers ({{$y->numbers}} )</li> 
            @endforeach </td>
               <td style="text-align:center">{{$v->number_of_charger}}</td> 
               <td style="text-align:center">{{$v->status}}</td>    
            <td style="text-align:center">{{$v->po}}</td> 
            <td style="text-align:center">{{$v->solar_project}}</td> 
       
            <td style="text-align:center">{{$v->installation_date}}</td>
            @can('edit')
            
            <td style="text-align:center"><a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('solars.edit',$v->id)}}"></a></td>
            @endcan
            @can('delete')
            <td style="text-align:center">
        
        <form action="{{route('solars.destroy',$v->id)}}" method="POST">
            @csrf
            @method('DELETE')

<button type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" onclick="return confirm('Are you sure you want delete the solar?')" ></button>
        </form>
        </td>
            @endcan
            @endforeach
        </table>
     

       

 
        </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script>
   $(document).ready(function() {
   
            $('#example thead tr').clone(true).appendTo( '#example thead' );
            $('#example thead tr:eq(1) th').each( function (i) {

                var title = $(this).text();

                $(this).html( '<input type="text" placeholder=" Search '+title+'" />' );
              
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                         .column(i)
                            .search( this.value )
                            .draw();
                    }
                });
            });
   
            var table = $('#example').DataTable( {
                    
                orderCellsTop: true,
                fixedHeader: true,
                dom: 'Bfrtip',       
        buttons: [
           
    {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'csv',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'pdf', 
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'copy',
                exportOptions: {
                    columns: ':visible'
                }},
                
                'colvis'
   ],
   columnDefs: [ {
            targets:[-1,-2,-3,-4,-5,-6] ,
            visible: false
        } ],
   columns: [
    {data: 'id', name: 'id'},
  {data: 'site_code', orderable: false,},
  {data: 'controller', orderable: false,},
  {data: 'panel brand', orderable: false,},
  {data: 'panel_capacity', orderable: false,},
  {data: 'Number of charger', orderable: false},
  {data: 'Last Updated by', orderable: false},
  

  {data: 'po', orderable: false},

  {data: 'solar project', orderable: false},


  {data: 'Added by', orderable: false},
  {data: 'status', orderable: false},
  {data: 'battery', orderable: false},
  @can('edit')
  {data: 'action', orderable: false},
  @endcan
  @can('delete')
  {data: 'delete', orderable: false},
@endcan
        ]
            });
        });
        
    </script>
 
 
@endsection