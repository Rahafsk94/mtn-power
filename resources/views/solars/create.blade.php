@extends('layouts.app')
@section('content')
@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif
<center>
    <p>Create new solar</p>
</center>
<form action="{{route('solars.store')}}" method="POST">
    @csrf

    
    <div class="raw">
<div class="column">
<div class="form-group">
                <strong>Site code:</strong>
                <input type="text" name="site_code" class="form-control" placeholder="Site code" id="site_code" required>
                <span id="error-message" style="color: red; display: none;">* the site is not available *</span>

        </div>



    <div class="form-group">
        <strong> Number of panel:</strong>
        <input type="number" name="number_of_panel" class="form-control" placeholder="Number of panel" required>

    </div>
 

    <div class="form-group">
                <strong>Po:</strong>
                
          <input type="number" name="po" class="form-control" placeholder="po" required>
        </div>
        
  <div class="form-group">
                <strong>Solar project:</strong>
                <input type="text" name="solar_project" class="form-control" placeholder="Solar project" required>


        </div>
        
        <div class="form-group">
                <strong> Installation date:</strong>
                <input type="date" name="installation_date" class="form-control" placeholder="Installation date" required>


        </div>
        <div class="form-group">
                <strong> PR:</strong>
                <input type="number" name="pr" class="form-control" placeholder="PR" required>


        </div>
    </div>

    <div class="column">
    <div class="form-group">
        <strong>Controller:</strong>
        <input type="number" name="controller" class="form-control" placeholder="Controller" required>

    </div>
    <div class="form-group">
                <strong> Panel capacity:</strong>
                <input type="number" name="panel_capacity" class="form-control" placeholder="Panel capacity" required>


        </div>
        
      

        <div class="form-group">
                <strong>Panel brand:</strong>
                <input type="text" name="panel_brand" class="form-control" placeholder="Panel brand" required>


        </div>
        <div class="form-group">
                <strong>Number of charger:</strong>
                <input type="text" name="number_of_charger" class="form-control" placeholder="Number of charger" required>


        </div>
        <div class="form-group">
                <strong>Status:</strong>
                <select type="text" name="status" class="form-control" required>

                <option value="">Select solar status</option>
<option value="used">Usesd</option>

<option value="new">New</option>
</select>
        </div>
      
        </div>



<div class="pull-right">
    <button class="button">create</button>
   </div>
    </div>

</div>


</form>
</div>
</div>
<script>

        $(document).ready(function(){

                $("#site_code").on('change', function(){
        var siteCode = $(this).val();

$.ajax({
        url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("site_code").value="";

              }
            
    },
});

});
        });
</script>

@endsection