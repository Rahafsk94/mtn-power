@extends('layouts.app')
@section('content')

@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif







<form action="{{route('solarshipment')}}" method="POST" id="myForm">
    @csrf
    <div class="raw">
<div class="column">




  <div class="form-group">
     <strong>Warehouse  name:</strong>
      <input type="text" name="warehouse_name" id="warehouse_name"class="form-control" placeholder="Warehouse" required>
      <span id="error-message" style="color: red; display: none;">* the warehouse is not available *</span>
  </div>
            

          


            <div class="form-group">
        <strong>PO:</strong>
        <input type="number" name="po" class="form-control" placeholder="po" required>

    </div>

  
    <div class="form-group">
        <strong>Shipment date:</strong>
        <input type="date" name="shipment_date" class="form-control" placeholder="Shipment date" required>

    </div>

    <div class="form-group">
        <strong>Panel brand:</strong>
        <input type="text" name="panel_brand" class="form-control" placeholder="Panel brand" required>

    </div>
    <div class="form-group">
        <strong>Number of panel:</strong>
        <input type="number" name="number_of_panel" class="form-control" placeholder="Number of panel" required>

    </div>
</div>
<div class="column">





<div class="form-group">
<strong>Controller:</strong>
    <input type="number" name="controller" class="form-control" placeholder="Controller" required>

</div>



<div class="form-group">
    <strong>Solar project :</strong>
    <input type="text" name="solar_project" class="form-control" placeholder="Solar project..." required>

</div>

<div class="form-group">
        <strong>Panel capacity:</strong>
        <input type="text" name="panel_capacity" class="form-control" placeholder="Panel capacity" required>

    </div>
    
<div class="form-group">
        <strong>Number of charger:</strong>
        <input type="number" name="number_of_charger" class="form-control" placeholder="Number of charger" required>

    </div>

</div>





<div class="pull-right">

    <button class="button" type="submit" >Add</button>
   </div>
    </div>

</div>


</form>
</div>
</div>
<script>
    $("#warehouse_name").on('change',function(){

 var warehouseName= $(this).val();
 console.log(warehouseName);
 $.ajax({
    url: "{{ route('cheack_warehouse') }}",
    method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                warehouse_name:warehouseName
            },
            dataType:'json',
         
            success: function(response) {
        
              if(response.a){
               
                $('#error-message').hide();
              
              }
              if(response.b){
        
                $('#error-message').show();
      
            document.getElementById("warehouse_name").value="";

              }
            
    },
 });

    });
</script>

@endsection