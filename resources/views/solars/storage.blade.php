@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
        <h3 class="header">Solars in warehouse</h3>

    </center>
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                        <a href="{{route('solars.index')}}">    <button type="button" class="btn btn-outline-danger" >Solar in site</button></a>
                            <div class="pull-right">

                            <a href="{{route('solarshipmentcreate')}}">    <button type="button" class="btn btn-outline-success" >Add new shipment</button></a>
                          
                        </h4>

                        <h6>

            </form>


                        </h6>


                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Solar ID</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Warehouse name</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Number of panel</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >PO</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Controller</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Status</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Solar project</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Panel brand</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Panel capacity</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Number of charger</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Shipment date</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Dismantle date</span> </th>
@can('admin')
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Install on site</span> </th>
            @endcan
</tr>
            <tbody>
            @foreach ($solars as $v)
            <tr>
            <td style="text-align:center">{{$v->id}}</td>

<td style="text-align:center">{{$v->warehouse->name}}</td>
<td style="text-align:center">{{$v->number_of_panel}}</td>
<td style="text-align:center">{{$v->po}}</td>
<td style="text-align:center">{{$v->controller}}</td>
<td style="text-align:center">{{$v->status}}</td>
<td style="text-align:center">{{$v->solar_project}}</td>
<td style="text-align:center">{{$v->panel_brand}}</td>

<td style="text-align:center">{{$v->panel_capacity}}</td>
<td style="text-align:center">{{$v->number_of_charger}}</td>

<td style="text-align:center">{{$v->shipment_date}}</td>
<td style="text-align:center">{{$v->dismantle_date}}</td>
            @can('admin')

          




                    <td style="text-align:center">


               <button type="button" id="edit_todo" data-id="{{$v->id }}" class="btn btn-outline-success">Install</button></td>

                @endcan


            @endforeach
        </table>


       <!-- The Modal -->
       <div class="modal" id="modal_todo">
            <div class="modal-dialog">
              <div class="modal-content">
                <form id="form_todo">
                    <div class="modal-header">
                      <h4 class="modal-title" id="modal_title"></h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                      <input type="hidden"  name="id" id="id">
                      <input  type="hidden" id="number" name="number_of_panel" >
                      <input  id="num" name="number" class="form-control" placeholder="Number">
                      <input type="text" name="site_code" id="name_todo" class="form-control" placeholder="Site code ..." required>
                      <span id="error-message" style="color: red; display: none;">* the site is not available *</span>
                      <input type="number" name="pr" id="name_todoo" class="form-control" placeholder="Pr..."required >
                      <input type="date" name="installation_date"  class="form-control" placeholder="Installation date"required >
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-info">Install</button>
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>





                <script type="text/javascript">
                  $(document).ready(function(){
                    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

                  });
      $("#add_todo").on('click',function(){
          $("#form_todo").trigger('reset');
          $("#modal_title").html('Add todo');
          $("#modal_todo").modal('show');
          $("#id").val("");
          $("#number").val("");
      });
                  $("body").on('click','#edit_todo',function(){
                      var id = $(this).data('id');
                      $.get('/solars/'+id+'/e',function(res){
                       
                          $("#modal_title").html('Install on site');
                          $("#id").val(res.id);
                          $("#number").val(res.number_of_panel);
                          $("#name_todo").val(res.site_code);
                          $("#name_todoo").val(res.pr);
                          $("#modal_todo").modal('show');
                      });
                  });
                  // Delete Todo

                  //save data

                  $("form").on('submit',function(e){
                    if(   $("#number").val() - $("#num").val() < 0 ){

                        alert('You dont have enought panel');
                           }
                             else {
                      e.preventDefault();
                      $.ajax({
                          url:"/solar/s",
                          data: $("#form_todo").serialize(),
                          type:'POST'
                      }).done(function(res){
                          var row = '<tr id="row_todo_'+ res.id + '">';
                          row += '<td width="20">' + res.id + '</td>';
                          row += '<td>' + res.site_code + '</td>';
                          row += '<td width="150">' + '<button type="button" id="edit_todo" data-id="' + res.id +'" class="btn btn-info btn-sm mr-1">Edit</button>' + '<button type="button" id="delete_todo" data-id="' + res.id +'" class="btn btn-danger btn-sm">Delete</button>' + '</td>';
                          if($("#id").val()){
                              $("#row_todo_" + res.id).replaceWith(row);
                              setTimeout(function () { // wait 3 seconds and reload
    window.location.reload(true);
  }, 1000);
                          }else{
                              $("#list_todo").prepend(row);
                          }
                          $("#form_todo").trigger('reset');
                          $("#modal_todo").modal('hide');
                      });}
                  });
              </script>

            </div>
        </div>
    </div>

  <script>


    $(document).ready(function() {





   var table = $('#example').DataTable( {
    orderCellsTop: true,
       fixedHeader: true,
       dom: 'Bfrtip',
buttons: [
  {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }},'colvis'

],
columnDefs: [ {
            targets:[-2,-3] ,
            visible: false
        } ],

columns: [
{data: 'id', name: 'id'},
{data: 'type', orderable: false,},
{data: 'used', orderable: false},
{data: 'oracle_code', orderable: false},
{data: 'panel', orderable: false},
{data: 'number', orderable: false},
{data: 'date', orderable: false},
{data: 'dismantle_date', orderable: false},
{data: 'delete', orderable: false},
{data: 'edit', orderable: false},
{data: 'show', orderable: false},
{data: 'add one', orderable: false},


{data: 'install', orderable: false},




]
   });
});




  </script>
<script>
  $("#name_todo").on('change', function(){

    var siteCode= $(this).val();
   
   $.ajax({
    url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("name_todo").value="";

              }
            
    },
});


   
  });
</script>
        </div>
                </div>
            </div>
        </div>
    </div>


</div>


@endsection

