@extends('layouts.app')
@section('content')
@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif
<form action="{{route('amperes.store')}}" method="POST">
    @csrf


    
        <div class="raw">
      <div class="column">
            <div class="form-group">
                <strong>Site code:</strong>
                       <input type="text" name="site_code" class="form-control" placeholder="Site code" id="site_code" required>
                <span id="error-message" style="color: red; display: none;">* the site is not available *</span>
            </div>
<div class="form-group">
 <strong>Ampere Provider:</strong>
<input type="text" name="ampere_provider" class="form-control" placeholder="Ampere Provider" required>
</div>
<div class="form-group">
    <strong>Payment method:</strong>
   <input type="text" name="payment_method" class="form-control" placeholder="Payment method" required>
   </div>
   <div class="form-group">
    <strong>Payment type:</strong>
   <input type="text" name="payment_type" class="form-control" placeholder="Payment type" required>
   </div>
   </div>
   <div class="column">
   <div class="form-group">
    <strong>Agreed RH:</strong>
   <input type="number" name="agreed_rh" class="form-control" placeholder="Agreed RH" required>
   </div>

   <div class="form-group">
    <strong>Notes:</strong>
   <input type="text" name="nots" class="form-control" placeholder="Notes">
   </div>

   <div class="form-group">
    <strong>Ampere capacity:</strong>
   <input type="number" name="ampere_capacity" class="form-control" placeholder="Capacity" required>
   </div>

   <div class="form-group">
    <strong>Installation date:</strong>
   <input type="date" name="installation_date" class="form-control" placeholder="Installation date" required>
   </div>

   
</div>

<div class="pull-right">
    <button class="button">create</button>
   </div>

</div>

</form>
<script>
$(document).ready(function() {
    $('#site_code').blur(function() {
        var siteCode = $(this).val();

        $.ajax({
            url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("site_code").value="";

              }
            
    },

        });
    });
});
  </script>
@endsection
