@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
        <h3 class="header">Amperes Information</h3>

    </center>
<div class="card">
<div class="card-header">
<h4>
                            <div class="pull-right">

                            @can('track')
                            <a href="ampere/track">       <button type="button" class="btn btn-outline-success">Track</button></a>
                            @endcan
                            @can('create')
                                <a href="{{route('amperes.create')}}">    <button type="button" class="btn btn-outline-success" >Add New Ampere</button></a>
           
@endcan
</div>
                        </h4>
</div>
<div class="card-body">

        <table class="table table-bordered table-striped" id="datatable" width="100%">
        <thead>
                                <tr>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >ID</span> </th>

                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site code</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Ampere capacity</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Ampere Provider</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Payment Method</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Payment Type</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Agreed RH</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Notes</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Installation Date</span> </th>
@can('edit')
                                <td style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span> </td>
                                @endcan
                               @can('delete')
                                <td style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Delete</span> </td>
                                @endcan
                    </tr>
                    <tbody>
@foreach ( $ampere as  $item)




                    <tr>
                    <td style="text-align:center">{{$item->id}}</td>
                    
                    <td style="text-align:center">{{$item->site->site_code}}</td>
                    <td style="text-align:center">{{$item->ampere_capacity}}</td>
                    <td style="text-align:center">{{$item->ampere_provider}}</td>
                    <td style="text-align:center">{{$item->payment_method}}</td>
                    <td style="text-align:center">{{$item->payment_type}}</td>
                    <td style="text-align:center">{{$item->agreed_rh}}</td>
                    <td style="text-align:center">{{$item->nots}}</td>
                    <td style="text-align:center">{{ date('d-m-y', strtotime($item->installation_date))}}</td>
        
                    @can('admin')
            <td style="text-align:center">   
            <a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('amperes.edit',$item->id)}}"></a>         
</td>
          
<td style="text-align:center">   
           <form action="{{route('amperes.destroy',$item->id)}}" method="Post">
          
          @csrf
          @method('DELETE')
          <button type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0); " onclick="return confirm('delete ??')" ></button>
          </form>
          </td>
                 @endcan            
                           @endforeach
</tr>
</tbody>
</div>

</form>
</table>

</div>

<script>
   $(document).ready(function() {
   
            $('#datatable thead tr').clone(true).appendTo( '#datatable thead' );
            $('#datatable thead tr:eq(1) th').each( function (i) {

                var title = $(this).text();

                $(this).html( '<input type="text" placeholder=" Search '+title+'" />' );
              
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                         .column(i)
                            .search( this.value )
                            .draw();
                    }
                });
            });
   
            var table = $('#datatable').DataTable( {
 //                   "stateSave": true,
                orderCellsTop: true,
                fixedHeader: true,
                dom: 'Bfrtip',       
        buttons: [
           
    {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'csv',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'pdf', 
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'copy',
                exportOptions: {
                    columns: ':visible'
                }},
                
                'colvis'
   ],
   columnDefs: [ {
            targets:[-1,-2,-3,-4,-5,-6,-7] ,
            visible: false
        } ],
        columns: [
    {data: 'id', name: 'id'},

    {data: 'site_code', name: 'site_code'},
    {data: 'ampere_provider', name: 'ampere_provider',searchable:false},
    {data: 'ampere_capacity', name: 'ampere_capacity',searchable:false},
    {data: 'payment_method', name: 'payment_method',searchable:false},
    {data: 'payment_type', name: 'payment_type',searchable:false},
    {data: 'agreed_rh', name: 'agreed_rh',searchable:false},
    {data: 'nots', name: 'nots',searchable:false},
    {data: 'installation_date', name: 'installation_date',searchable:false},

    @can('edit')
    {data: 'edit',searchable:false},
    @endcan
    @can('delete')
    {data: 'delete',searchable:false},
    @endcan
        ]
            });
        });
        
    </script>





@endsection
