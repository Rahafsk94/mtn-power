@extends('layouts.app')
@section('content')

<form action="{{route('amperes.update',$ampere->id)}}" method="POST">
    @csrf
    @method('PUT')
    @if ($errors->any())
    <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
       </ul>
    </div>

    @endif
    <div class="raw">
    <div class="column">
           



                <div class="form-group">
                    <strong>Ampere Provider:</strong>
                    <input type="text" name="ampere_provider" value="{{$ampere->ampere_provider}}" class="form-control" placeholder="Ampere Provider" required>

                </div>
                <div class="form-group">
                    <strong>Payment method:</strong>
                    <input type="text" name="payment_method" value="{{$ampere->payment_method}}" class="form-control" placeholder="Payment method" required>

                </div>


                <div class="form-group">
                    <strong>Payment type:</strong>
                    <input type="text" name="payment_type" value="{{$ampere->payment_type}}" class="form-control" placeholder="Payment type" required>

                </div>
</div>
<div class="column">
                <div class="form-group">
                    <strong>Agreed RH:</strong>
                    <input type="number" name="agreed_rh" value="{{$ampere->agreed_rh}}" class="form-control" placeholder="Agreed RH" required>

                </div>

                <div class="form-group">
                    <strong>Notes:</strong>
                    <input type="text" name="notes" value="{{$ampere->nots}}" class="form-control" placeholder="Notes" >

                </div>


                <div class="form-group">
                    <strong>Ampere capacity:</strong>
                    <input type="number" name="ampere_capacity" value="{{$ampere->ampere_capacity}}" class="form-control" placeholder="Capacity" required>

                </div>
              
                
                <div class="pull-right">
<button class="button" type="submit" >Update</button>
</div>

</form>
@endsection
