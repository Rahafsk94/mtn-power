@extends('ampere.layout')
@section('content')
@if($Ampere->isNotEmpty())
<table class="table table-bordered">

    <tr>

      <th scope="col">Ampere_id</th>
      <th scope="col">Site_Code</th>
      <th scope="col">Ampere_Provider</th>
      <th scope="col">Payment_method</th>
      <th scope="col">Payment_type</th>
      <th scope="col">agreed_RH</th>
      <th scope="col">Nots</th>
      <th scope="col">installation_date</th>
      <th scope="col">end_date</th>

    </tr>

<tbody>


    @foreach ($Ampere as $item)
    <tr>

    <td>{{$item->Ampere_id}}</td>
    <td>{{$item->Site_Code}}</td>
    <td>{{$item->Ampere_Provider}}</td>
    <td>{{$item->Payment_method}}</td>
    <td>{{$item->Payment_type}}</td>
    <td>{{$item->agreed_RH}}</td>
    <td>{{$item->Nots}}</td>
    <td>{{$item->installation_date}}</td>
    <td>{{$item->end_date}}</td>
    @endforeach

    @else
    <div>
        <center>
        <h2>No Ampere found</h2></center>
    </div>
        @endif
@endsection
