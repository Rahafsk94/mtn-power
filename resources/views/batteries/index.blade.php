@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
        <h3 class="header">  <p> Batteries in site</p></h3>

    </center>

    <div class="row">
 
 <div class="col-md-12">
     <div class="card">
         <div class="card-header">
       
             <h4>
             <div class="u">
                 <div class="pull-right">
                    @can('track')
                 <a href="{{route('battery-track')}}">       <button type="button" class="btn btn-outline-success">Track</button></a>
                 @endcan
                 @can('create')
                     <a href="{{route('batteries.create')}}">    <button type="button" class="btn btn-outline-success" >Add new Battery</button></a>
                    @endcan
                     <a href="{{route('batterywarehouse')}}">    <button  class="btn btn-outline-success" type="submit">Warehouse</button></a>
                     <a href="{{route('bactions')}}">    <button  class="btn btn-outline-success" type="submit">Add action</button></a>

</div>
             </h4>

             <h6>      
             <div class="dropdown">
  <button class="btn btn-outline-danger" >Batteries actions</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="{{route('batteryinstall')}}">Installation</a>
    <a href="{{route('bre')}}">Reshuffling</a>
    <a href="{{route('bdismantle')}}">Dismantle</a>
  </div>
</div>


             </h6>
            
         </div>
               
         <div class="card-body">
         <div class="cont">

<table  class="table table-bordered table-striped data-table" width="100%"  >
<thead>
 <tr>
 <th>ID</th>
    <th>Site code</th>
    <th>Battery brand</th>
    <th>Capacity</th>
    <th>Battery numbers</th>
     <th>Cabinet type</th>

     <th>Solar ID</th>
  
    
     <th>PO</th>
    

   
    
   
     @can('edit')
     <td> <p>Edit</p> </td>
    
     @endcan
     @can('delete')
     <td> <p>Delete</p> </td>
     @endcan
     </tr>
        <tbody>
            @foreach ($battery as $v)
            <tr>
            <td style="text-align:center">{{$v->id}}</td>
            <td style="text-align:center">{{$v->site->site_code}}</td>
            <td style="text-align:center">{{$v->battery_brand}}</td>
            <td style="text-align:center">{{$v->capacity}}</td>
            <td style="text-align:center">{{$v->numbers}}</td>
            <td style="text-align:center"> @if($v->cabinet){{$v->cabinet->cabinet_name}}@endif</td>
            
        
            <td style="text-align:center">{{$v->solar}}</td>
          
           
            <td style="text-align:center">{{$v->po}}</td>
          
      


            @can('edit')  
            <td style="text-align:center">   
            
            <a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('batteries.edit',$v->id)}}"></a>
            </td>
            @endcan
            
            @can('delete')  
            <td style="text-align:center">  
            <form action="{{route('batteries.destroy',$v->id)}}" method="Post">
              
          @csrf
          @method('DELETE')     
        
                <button  type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" onclick="return confirm('Are you sure you want delete the battery?')" ></button>
</form>
         </td>
         @endcan
</tr>
            @endforeach
        </table>
</div>
</div>



<script>
   $(document).ready(function() {
    
   

            $('.data-table thead tr').clone(true).appendTo( '.data-table thead' );
            
            $('.data-table thead tr:eq(1) th').each( function (i) {

                var title = $(this).text();

                $(this).html( '<input type="text" placeholder=" Search '+title+'" />' );
              
                $( 'input', this ).on( 'keyup change', function () {
                    if ( table.column(i).search() !== this.value ) {
                        table
                         .column(i)
                            .search( this.value )
                            .draw();
                    }
                });
            });
   
            var table = $('.data-table').DataTable( {
                 
                orderCellsTop: true,
                fixedHeader: true,
                dom: 'Bfrtip',       
                
        buttons: [
           
    {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'csv',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'pdf', 
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'copy',
                exportOptions: {
                    columns: ':visible'
                }},
                
                'colvis'
   ],
   columnDefs: [ {
            targets:[-1,-2,-3,-10],
            visible: false
        } ],
        columns: [
            {data: 'id', name: 'id'},
    {data: 'site_code', name: 'site_code'},
    {data: 'battery_brand', name: 'battery_brand'},
    {data: 'capacity', name: 'capacity'},
    {data: 'numbers', name: 'numbers'},
    {data: 'cabinet_name', name: 'cabinet_name'},
    {data: 'solar', name: 'solar'},
  

    {data: 'po', name: 'po'},
  


   
    @can('edit')  
    {data: 'edit',   name: 'actions'},
@endcan
   
@can('delete')  
{data: 'delete',   name: 'actions'},
@endcan
        ]
            });
        });
        
    </script>


@endsection