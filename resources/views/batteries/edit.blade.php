@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<form action="{{route('batteries.update',$battery->id)}}" method="POST">
    @csrf
    @method('PUT')
    @if ($errors->any())


    <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
       </ul>
    </div>

    @endif
<div class="raw">
    <div class="column">

      


        
            <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                <strong>Battery brand:</strong>
              
                <input type="text" name="battery_brand"

value="{{$battery->battery_brand}}"
class="form-control" placeholder="Battery Brand" required>
            </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
                <strong>Capacity:</strong>
              
                <input type="number" name="capacity"

value="{{$battery->capacity}}"
class="form-control" placeholder="Capacity"  required>
            </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Cabinet ID:</strong>
                @if($battery->cabinet_id !='') 
                <select type="number" name="cabinet_id" class="form-control" value="{{$battery->cabinet_id}}">

                @foreach($cabinet as $cab)
                <option value="{{$cab->id}}">{{$cab->id}}</option>
                @endforeach
                <option value="">Null</option>
                </select>
@else
<select type="number" name="cabinet_id" class="form-control" >
<option value="">Null</option>
                @foreach($cabinet as $cab)
                <option value="{{$cab->id}}">{{$cab->id}}</option>
                @endforeach
                
                </select>
                @endif
            </div>
            </div>
            </div>
            <div class="column">
          
            <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Oracle code:</strong>
              
                <input type="number" name="oracle_code"

value="{{$battery->oracle_code}}"
class="form-control" placeholder="Oracle Code" required>
            </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Solar ID:</strong>
                @if($battery->solar =='')      
                <select type="number" name="solar" class="form-control">
                <option value="">Null</option>
                @foreach($solar as $sol)
                <option value="{{$sol->id}}">{{$sol->id}}</option>
                @endforeach
                
                </select>
                @else

                <select type="number" name="solar" class="form-control"  value="{{$battery->solar}}">
          
                @foreach($solar as $sol)
                <option value="{{$sol->id}}">{{$sol->id}}</option>
                
                @endforeach
                <option value="">Null</option>
                </select>
@endif
            </div>
            </div>
</div>

<div class="pull-right">
<button class="button" type="submit" >Update</button>
</div>
</form>
@endsection