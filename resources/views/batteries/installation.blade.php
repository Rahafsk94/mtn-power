@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif

<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                         

</div>
                        </h4>
               
                        <h6>
                
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Battery ID</span> </th>
  
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Battery brand</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action name</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action time</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Craeted by user</span> </th>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >PR</span> </th>
         </tr>
        <tbody>
            @foreach ($installations as $reshuffling)
          
          
            <tr>
            <td style="text-align:center">{{$reshuffling->battery->id}}</td>
            <td style="text-align:center">{{$reshuffling->battery->battery_brand}}</td>

            <td style="text-align:center">{{$reshuffling->poweraction->action_name}}   </td>
       
     

            <td style="text-align:center">{{date('d-m-y', strtotime($reshuffling->created_at))}}</td>
          
            <td style="text-align:center">{{$reshuffling->user_name}}</td>
            
            
            <td style="text-align:center">{{$reshuffling->pr}}</td>
 

          @endforeach
        </table>


 


<script>

    

$(document).ready(function() {

var table= $('#example').DataTable( {
    orderCellsTop: true,
   fixedHeader: true,
   dom: 'Bfrtip',       
   buttons: [
           
           {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},
                   
          ],

});

});
</script>



@endsection 