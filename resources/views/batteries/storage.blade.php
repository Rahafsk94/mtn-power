@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<center>
        <h3 class="header">Batteries in warehose</h3>

    </center>
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            
                            <div class="pull-right">
@can('create')
                            <a href="{{route('batteryshipmentcreate')}}">    <button type="button" class="btn btn-outline-success" >Add new shipment</button></a>
@endcan
                        </h4>

                        <h6>
                        <a href="{{route('batteries.index')}}">    <button type="button" class="btn btn-outline-danger" > Batteries in site</button></a>
            </form>


                        </h6>


                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" > ID</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Warehose name</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Battery brand</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Capacity</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Numbers</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Oracle code</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Status</span> </th>

<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Process order</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Shipment date</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Dismantle date</span> </th>
@can('admin')
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Install on site</span> </th>
            @endcan
</tr>
            <tbody>
            @foreach ($batteries as $v)
            <tr>
            <td style="text-align:center">{{$v->id}}</td>

<td style="text-align:center">{{$v->warehouse->name}}</td>
<td style="text-align:center">{{$v->battery_brand}}</td>
<td style="text-align:center">{{$v->capacity}}</td>
<td style="text-align:center" >{{$v->numbers}}</td>
<td style="text-align:center">{{$v->oracle_code}}</td>


<td style="text-align:center">{{$v->status}}</td>

<td style="text-align:center">{{$v->po}}</td>
<td style="text-align:center">{{$v->shipment_date}}</td>
<td style="text-align:center">{{$v->dismantle_date}}</td>
            @can('admin')

          




                    <td style="text-align:center">


                                   <button type="button" id="edit_todo" data-id="{{$v->id }}" class="btn btn-outline-success">Install</button></td>

                @endcan


            @endforeach
        </table>


       <!-- The Modal -->
       <div class="modal" id="modal_todo">
            <div class="modal-dialog">
              <div class="modal-content">
                <form id="form_todo">
                    <div class="modal-header">
                      <h4 class="modal-title" id="modal_title"></h4>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                      <input type="hidden"  name="id" id="id">
                
                      <input type="text" name="site_code" id="name_todo" class="form-control" placeholder="Site code ..." required>
                      <span id="error-message" style="color: red; display: none;">* The site is not available *</span>
                      <input type="number" name="pr" id="name_todoo" class="form-control" placeholder="Pr..."required >
                      <input type="hidden" name="numbers" id="idd"class="form-control" placeholder="numbers..."required >
                      <input type="number" name="number" id="iddd" class="form-control" placeholder="Numbers..."required >
                      <select type="text" name="cabinet_id" id="cabinet" class="form-control" placeholder="Cabinet id ..." >
<option value="">Select cabinet</option>
</select>
                      <select type="number" name="solar" id="solar" class="form-control" placeholder="Solar id...">
                      <option value="">Select solar</option>
</select>
                      <input type="date" name="installation_date" class="form-control" placeholder="Installation_date..."required>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-info">Install</button>
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>





                <script type="text/javascript">


                  $(document).ready(function(){
                    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

                  });
      $("#add_todo").on('click',function(){
          $("#form_todo").trigger('reset');
          $("#modal_title").html('Add todo');
          $("#modal_todo").modal('show');
          $("#id").val("");
          $("#idd").val("");
    
      });
                  $("body").on('click','#edit_todo',function(){
                      var id = $(this).data('id');
                      var idd = $(this).data('idd');
                      $.get('/batts/'+id+'/e',function(res){
                          $("#modal_title").html('Install on site');
                          $("#id").val(res.id);
                          $("#idd").val(res.numbers);
                        
                          $("#name_todo").val(res.site_code);
                          $("#name_todoo").val(res.pr);
                          $("#modal_todo").modal('show');
                      });
                  });
                  // Delete Todo

                  //save data

                      
                  $("form").on('submit',function(e){
                    if($("#iddd").val() - $("#idd").val() >0){
                     
      alert('you dont have enoght battery');
      setTimeout(function () { // wait 3 seconds and reload
    window.location.reload(true);
  }, 3000);
  
                          }
                          else{
                      e.preventDefault();
                      $.ajax({
                          url:"/battery/s",
                          data: $("#form_todo").serialize(),
                          type:'POST'
                      }).done(function(res){
             
            
                        
                          var row = '<tr id="row_todo_'+ res.id + '">';
                          row += '<td width="20">' + res.id + '</td>';
                          row += '<td>' + res.site_code + '</td>';
                          row += '<td width="150">' + '<button type="button" id="edit_todo" data-id="' + res.id +'" class="btn btn-info btn-sm mr-1">Edit</button>' + '<button type="button" id="delete_todo" data-id="' + res.id +'" class="btn btn-danger btn-sm">Delete</button>' + '</td>';
                          if($("#id").val()){
                              $("#row_todo_" + res.id).replaceWith(row);


                              setTimeout(function () { // wait 3 seconds and reload
                                              window.location.reload(true);
                                          }, 2000);

                          }else{
                              $("#list_todo").prepend(row);
                          }

                          $("#form_todo").trigger('reset');
                          $("#modal_todo").modal('hide');
                      });}
            
                  });
              </script>

            </div>
        </div>
    </div>

  <script>


    $(document).ready(function() {





   var table = $('#example').DataTable( {
    orderCellsTop: true,
       fixedHeader: true,
       dom: 'Bfrtip',
buttons: [
    {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }}
                       
                       ,     'colvis' ,

],
columnDefs: [ {
            targets: [-1,-2],
            visible: false,
      
        } ],
columns: [
{data: 'id', name: 'id'},
{data: 'warehouse', orderable: false,},
{data: 'battery_brand', orderable: false},
{data: 'capacity', orderable: false},
{data: 'number', orderable: false},
{data: 'oracle_code', orderable: false},
{data: 'status', orderable: false},
{data: 'po', orderable: false},
{data: 'date', orderable: false},
{data: 'disdate', orderable: false},
@can('admin')
{data: 'install', orderable: false},
@endcan







]
   });
});




  </script>

<script>

$(document).ready(function(){

    $('#name_todo').blur(function() {

        var siteCode = $(this).val();
        $.ajax({
        url: "{{ route('cheack_site') }}",
        method: 'POST',
        data: {
            _token: "{{ csrf_token() }}",
            site_code: siteCode
        },
        dataType:'json',
     
        success: function(response) {
       if(response.b){
        
       }
          if(response.a){
            $('#error-message').hide();
          }
          else{
            $('#error-message').show();
  
        document.getElementById("name_todo").value="";

          }
        
},

    });
});});
</script>


<script>

$(document).ready(function(){

    $('#name_todo').blur(function() {
       
        
        var site=$(name_todo).val();
 
        $.ajax({
        url: "{{ route('cheack_solar') }}",
        method: 'POST',
        data: {
            _token: "{{ csrf_token() }}",
      
        site_code:site
        },
        dataType:'json',
     
        success: function(response) {
            var option = '<option value="">Select solar</option>';
       

           if(response.length==0){


       $('#solar').html(option);
           }
           else{
           for (var i=0;i<response.length;i++){

      
    option += '<option value="'+ response[i].id + '">' + response[i].id + '</option>';
       $('#solar').html(option);

           }
    }
  
   

              
        
},

    });
});});
</script>





<script>

$(document).ready(function(){

    $('#name_todo').blur(function() {

        
        var site=$(name_todo).val();

        $.ajax({
        url: "{{ route('cheack_cabinet') }}",
        method: 'POST',
        data: {
            _token: "{{ csrf_token() }}",
      
        site_code:site
        },
        dataType:'json',
     
        success: function(response) {
            var option = '<option value="">Select cabinet</option>';
          
            if(response.length==0){


$('#cabinet').html(option);
    }
    else{
           
           for (var i=0;i<response.length;i++){

      
    option += '<option value="'+ response[i].id + '">' + response[i].id + '</option>';
       $('#cabinet').html(option);
    }}
  
   

              
        
},

    });
});});
</script>

        </div>
                </div>
            </div>
        </div>
    </div>


</div>


@endsection

