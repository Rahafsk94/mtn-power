@extends('layouts.app')
@section('content')
@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
         

       <li>   {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif

<form action="{{route('batteries.store')}}" method="POST">
    @csrf
    <div class="raw">
<div class="column">

        <div class="form-group">
            <strong>Site Code:</strong>


                <input type="text" name="site_code" class="form-control" id="site_code" placeholder="Site code" required>
                <span id="error-message" style="color: red; display: none;">* the site is not available *</span>
        </div>

        <div class="form-group">
        <strong>Cabinet ID:</strong>

        <select name="cabinet_id" class="form-control" id="cabinet_id" placeholder="Cabinet id">
        <option value="">Select</option>
        
</select>

    </div>
    
            <div class="form-group">

                <strong>Battery brand:</strong>
                <input type="text" name="battery_brand" class="form-control" placeholder="Battery Brand" required>


            </div>


            <div class="form-group">
                <strong>Capacity:</strong>
                <input type="number" name="capacity" class="form-control" placeholder="Capacity" required>


        </div>
   
            <div class="form-group">
                <strong>Numbers:</strong>
                <input type="number" name="numbers" class="form-control" placeholder="Numbers" required>

            </div>
       

    
            <div class="form-group">
                <strong>PO:</strong>
                <input type="number" name="po" class="form-control" placeholder="po" required>

            </div>

    </div>



    <div class="column">




    <div class="form-group">
    <strong>Status:</strong>
            <select type="text" name="status" class="form-control" placeholder="status" required >
            <option value="">Select</option>

        <option >Used</option>
        <option >New</option>

    </select>


    </div>



      
        <div class="form-group">
        <strong>Solar ID:</strong>
        <Select type="name" name="solar" class="form-control" placeholder="solar id" id ="solar_id">
            <option value="">Select</option>
</Select>

    </div>

    <div class="form-group">
        <strong>Installation date:</strong>
        <input type="date" name="installation_date" class="form-control" placeholder="installation_date" required>

    </div>


    <div class="form-group">
        <strong>PR:</strong>
        <input type="number" name="pr" class="form-control" placeholder="PR " required>

    </div>




    <div class="form-group">
                <strong>Oracle Code:</strong>
                <input type="text" name="oracle_code" class="form-control" placeholder="Oracle Code" required>

            </div>


<div class="pull-right">
    <button class="button">create</button>
   </div>
    </div>

</div>


</form>
</div>
</div>
<script>

    $(document).ready(function(){

        $('#site_code').blur(function() {

            var siteCode = $(this).val();
            $.ajax({
            url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("site_code").value="";

              }
            
    },

        });

   
    });});
</script>
<script>


$("#site_code").on('change', function(){
        var siteCode = $(this).val();
        var option = '<option value="">Select</option>';
        $('#cabinet_id').html(option);
        $('#solar_id').html(option);
        $.ajax({
            
    url: "{{ route('cheack_cabinet') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
                var option = '<option value="">Select</option>';
           console.log(response );

           
           for (var i=0;i<response.length;i++){

      
    option += '<option value="'+ response[i].id + '">' + response[i].id + '</option>';
       $('#cabinet_id').html(option);
    }
  
   

              
            
    },

        });

        $.ajax({
            
            url: "{{ route('cheack_solar') }}",
                    method: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        site_code: siteCode
                    },
                    dataType:'json',
                 
                    success: function(response) {
                        var option = '<option value="">Select</option>';
                   console.log(response );
        
                   
                   for (var i=0;i<response.length;i++){
        
              
            option += '<option value="'+ response[i].id + '">' + response[i].id + '</option>';
               $('#solar_id').html(option);
            }
          
           
        
                      
                    
            },
        
                });
        





        });
</script>
@endsection