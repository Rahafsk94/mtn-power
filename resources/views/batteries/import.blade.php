@extends('layouts.app')
@section('content')
<div class="container mt-5">
 
   
  @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
  @endif
 
  <div class="card">
 
    <div class="card-header font-weight-bold">
      <h2 class="float-left">  <p>Import the Battery file</p> </h2>
   
    </div>
 
    <div class="card-body">
 
        <form id="excel-csv-import-form" method="POST"  action="{{ url('import-battery') }}" accept-charset="utf-8" enctype="multipart/form-data">
 
          @csrf
                   


          @if (count($errors) > 0)
                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                      <div class="alert alert-danger alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">okay!!</button>
                          <h4><i class="icon fa fa-ban"></i> Sorry!</h4>
                          @foreach($errors->all() as $error)
                          {{ $error }} <br>
                          @endforeach      
                      </div>
                    </div>
                </div>
                @endif



            <div class="row">
 
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="file" name="file" placeholder="Choose file">
                    </div>
                    @error('file')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                    @enderror
                </div>              
  
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="submit"> Import </button>
                </div>
            </div>     
        </form>
 
    </div>
 
  </div>
 
</div>  

@endsection