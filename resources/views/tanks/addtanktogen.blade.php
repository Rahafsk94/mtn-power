@extends('layouts.app')
@section('content')
@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif

<form action="{{route('newtankcreate',$generator)}}" method="POST">
    @csrf
    <div class="raw">
  
<div class="column">

    <div class="form-group">
        <strong>Installation Date:</strong>
        <input type="date" name="installation_date" class="form-control" placeholder="Installation Date" required>

    </div>
</div>
<div class="column">
    <div class="form-group">
            <strong>Tank Id :</strong>


                <select type="text" name="tank_id"  id="tank_id"  class="form-control" placeholder="Tank Id" required>
                <option value="">Select tank id</option>
                    @foreach($tank as $t)
                    
                    <option value="{{$t->id}}">{{$t->id}}</option>
                    @endforeach
</select>

        </div>
       
        <div class="pull-right">
    <button class="button">create</button>
   </div>
    </div>

</div>
<script>

    $(document).ready(function(){
        var generator= {!! json_encode($generator) !!};
        console.log(generator);
        $('#tank_id').blur(function() {

            var tankId = $(this).val();
           
          

            $.ajax({
            url: "{{ route('cheack_tankgenerator') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                id: tankId ,
                generator_id:generator
            },
            dataType:'json',
         
            success: function(response) {
                if(response.a){

                    alert('Tank already exist');
                    document.getElementById("tank_id").value="";
                }
                

            }});


              






             
            
        });

       
    });
</script>
    @endsection