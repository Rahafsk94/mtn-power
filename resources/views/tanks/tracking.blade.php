
@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                    </div>
                    <div class="card-body">

<table  class="table table-bordered table-striped" id="example" width="100%" >
<thead>
<tr>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Tank id</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Event</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Created by user</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Old values</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >New values</span> </th>
<th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Action time</span> </th>
</tr>
</thead>

@foreach ( $tank as $t)
<tbody>
<tr>
<td style="text-align:center">{{$t->auditable_id}}</td>

<td style="text-align:center">{{$t->event}}</td>

<td style="text-align:center">{{$t->user->name}}</td>

<td style="text-align:center">
<button class="collapsible">Open Collapsible</button>
<div class="content">
  @foreach ( $t->old_values as $key=>$value)
  <p>{{$key}}</p><li>{{$value}} </li> 
  @endforeach
  </div> 
</td>
<td style="text-align:center">
<button class="collapsible">Open Collapsible</button>
<div class="content">
  @foreach ( $t->new_values as $key=>$value)
  <p>{{$key}}</p><li>{{$value}} </li> 
  @endforeach
  </div> 
</td>
<td style="text-align:center">{{$t->created_at}}</td>
</tr>
</tbody>

@endforeach
</div>
</div>
</table>
<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "3px";
    } 
  });
}         
            
</script>
<script>
$(document).ready(function() {
  var table = $('#example').DataTable({
    
buttons: [
    'excel'

],
   

  });});
</script>
@endsection
