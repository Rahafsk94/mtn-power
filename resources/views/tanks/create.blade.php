@extends('layouts.app')
@section('content')
@if ($errors->any())

<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif


<form action="{{route('tanks.store')}}" method="POST">
    @csrf

 
    <div class="col-md-12">

                              
    <div class="card-body">
                    <div class="cont">
    <div class="raw">
<div class="columnn">

        <div class="form-group">
            <strong>Site Code:</strong>


                <input type="text" name="site_code" class="form-control" placeholder="Site Code" id="site_code" required>
                <span id="error-message" style="color: red; display: none;">* the site is not available *</span>
        </div>

        <div class="form-group">
        <strong>Generator Id:</strong>
        <select  type="number" name="generator_id" class="form-control" id="generator_id" placeholder="generator_id" required >
        <option value="">select</option>
        </select>
        <span id="error-message2" style="color: red; display: none;">* the generator is not available *</span>
    </div>
   
    <div class="form-group">
        <strong>Installation Date:</strong>
        <input type="date" name="installation_date" class="form-control" placeholder="Installation Date" required>

    </div>
    </div>
    <div class="columnn">
            <div class="form-group">
                <strong>Tank Index:</strong>
                <input type="number" name="tank_index" class="form-control" placeholder="Tank index" required>

            </div>


          
       

    <div class="form-group">
                <strong>Tank Sharp:</strong>
                <input type="number" name="tank_sharp" class="form-control" placeholder="Tank Sharp" required>


        </div>

    <div class="form-group">
                <strong>Tank Dimension1:</strong>
                <input type="number" name="tank_dimension1" class="form-control" placeholder="Tank Dimension1" required>

            </div>
            </div>
            <div class="columnn">
    <div class="form-group">
                <strong>Tank Dimension2:</strong>
                <input type="number" name="tank_dimension2" class="form-control" placeholder="Tank Dimension2" required>

            </div>

        <div class="form-group">
            <strong>Tank Dimension3:</strong>
            <input type="number" name="tank_dimension3" class="form-control" placeholder="Tank Dimension3" required>

        </div>


    <div class="form-group">
        <strong>Tank Diameter:</strong>
        <input type="number" name="tank_diameter" class="form-control" placeholder="Tank Diameter" required>

    </div>


    
 
    

<div class="pull-right">
    <button class="button">create</button>
   </div>


   </div>

</form>
</div>
</div>
</div>
</div>
</div>
<script>


    $("#site_code").on('change', function(){
        var siteCode = $(this).val();
       
$.ajax({
    url: "{{ route('cheack_site') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message').hide();
              }
              else{
                $('#error-message').show();
      
            document.getElementById("site_code").value="";

              }
            
    },

        });

        $.ajax({
    url: "{{ route('cheack_site_generator') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                site_code: siteCode
            },
            dataType:'json',
         
            success: function(response) {
                var option = '<option value="">Select</option>';
                if(response.length==0){


$('#generator_id').html(option);
    }

           else{
           for (var i=0;i<response.length;i++){

      
    option += '<option value="'+ response[i].id + '">' + response[i].id + '</option>';
       $('#generator_id').html(option);
    }
  
   
           }
              
            
    },

        });
     
});
 

</script>
<!--<script>


    $("#generator_id").on('change', function(){
        var id = $(this).val();
       
$.ajax({
    url: "{{ route('cheack_generator') }}",
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                generator_id: id
            },
            dataType:'json',
         
            success: function(response) {
           if(response.b){
            
           }
              if(response.a){
                $('#error-message2').hide();
              
              }
              else{
                $('#error-message2').show();
                
                document.getElementById("generator_id").value="";
        

              }
            
    },

        });
      
});
 

</script>-->
@endsection