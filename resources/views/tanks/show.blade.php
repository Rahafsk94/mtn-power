@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if(session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                    @endif
<center>
        <h3 class="header">Tanks Information</h3>

    </center>
    

    <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                            <a href="generator/export">       <button type="button" class="btn btn-outline-secondary">export</button></a>
                   

</div>
                        </h4>
               
                        <h6>
                        <form action="{{ route('searchs') }}" method="GET">
                <input type="text" name="searchs" placeholder="Search by Site Code.." required/>
                <button type="submit">Search</button>
            </form>
 
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >tank id</span> </th>