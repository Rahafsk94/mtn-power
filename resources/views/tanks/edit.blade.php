@extends('layouts.app')
@section('content')


<form action="{{route('tanks.update',$tank->id)}}" method="POST">
@csrf
    @method('PUT')
    @if ($errors->any())


    <div class="alert alert-danger">
       <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
       </ul>
    </div>

    @endif
<div class="raw">
    <div class="column">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tank index:</strong>
                <input type="number" name="tank_index"

                value="{{$tank->tank_index}}"
                class="form-control" required>

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tank sharp:</strong>
                <input type="number" name="tank_sharp"
                value="{{$tank->tank_sharp}}"
                class="form-control"  required>

            </div>


        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tank dimension1:</strong>
                <input type="number" name="tank_dimension1"
                value="{{$tank->tank_dimension1}}"
                class="form-control" required>

            </div>


        </div>
      
    </div>
<div class="column">
    
    
<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tank dimension2	:</strong>
                <input type="number" name="tank_dimension2"
                value="{{$tank->tank_dimension2}}"
                class="form-control" required>

            </div>
        </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Tank dimension3:</strong>
            <input type="number" name="tank_dimension3"
            value="{{$tank->tank_dimension3}}"
            class="form-control"  required>

        </div>
    </div>
    

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Tank diameter:</strong>
            <input type="number" name="tank_diameter"
            value="{{$tank->tank_diameter}}"
            class="form-control" required>

        </div>
    </div>

       
</div>
<div class="pull-right">
<button class="button" type="submit" >Update</button>
</div>
</form>
</div>
@endsection