@extends('layouts.app')
@section('content')
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if(session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                    @endif
<center>
        <h3 class="header">Tanks Information</h3>

    </center>


    <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">
                                @can('track')
                            <a href="{{route('tank_track')}}">    <button type="button" class="btn btn-outline-success" >Track</button></a>
                            @endcan
                            <a href="{{route('tanks.create')}}">    <button type="button" class="btn btn-outline-success" >Add new tank</button></a>

</div>
                        </h4>
               
                        <h6>
                
 
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>

                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >ID</span> </th>
  
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Site code</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Tank index</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Tank sharp</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Tank dimension1</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Tank dimension2</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Tank dimension3</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Tank diameter</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Installation date</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generators</span> </th>
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator id </span> </th>
  @can('edit')
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span> </th>
  @endcan
  @can('delete')
  <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Delete</span> </th>
  @endcan
  </tr>
        <tbody>
        @foreach($tankdata as $a)
            <tr>
            <td style="text-align:center">{{$a->id }}</td>
            <td style="text-align:center">{{$a->site->site_code }}</td>
            <td style="text-align:center">{{$a->tank_index}}</td>
            <td style="text-align:center">{{$a->tank_sharp}}</td>
            <td style="text-align:center">{{$a->tank_dimension1}}</td>
            <td style="text-align:center">{{$a->tank_dimension2}}</td>
            <td style="text-align:center">{{$a->tank_dimension3}}</td>
            <td style="text-align:center">{{$a->tank_diameter}}</td>
       
            <td style="text-align:center">     @foreach($a->generatortank as $g)<li>{{$g->installation_date->format('d-m-y')}}</li>    
            @endforeach</td>
   
            <td style="text-align:center">

 
            @foreach($a->generators as $generator)
 <li>{{$generator->generator_name}}  </li> 
  
 @endforeach

</td>
<td style="text-align:center">


@foreach($a->generators as $generator)
<li>
{{$generator->id}}</li> @endforeach</td>

@can('edit')
<td style="text-align:center"><a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('tanks.edit',$a->id)}}"></a></td>
@endcan
@can('delete')
    <td style="text-align:center">
    <form action="{{route('tankdes',$a->id)}}" method="Post">
          
          @csrf

          @method('DELETE')
         
          <button type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0);" onclick="return confirm('Are you sure you want delete the tank?')" ></button>
          </form>
          </td>
          @endcan
    @endforeach
</div>


<script>
   $(document).ready(function() {
   
   
            var table = $('#example').DataTable( {
                    
                orderCellsTop: true,
                fixedHeader: true,
                dom: 'Bfrtip',       
        buttons: [
           
    {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'csv',
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'pdf', 
                exportOptions: {
                    columns: ':visible'
                }},
                {
                extend:  'copy',
                exportOptions: {
                    columns: ':visible'
                }},
                
                'colvis'
   ],
   columnDefs: [ {
            targets: [-1,-2],
            visible: false,
      
        } ],
   columns: [
    {data: 'id', name: 'id'},
  {data: 'site_code', orderable: false},
  {data: 'tank_index', orderable: false,searchable:false},
  {data: 'tank_sharp', orderable: false,searchable:false},
  {data: 'tank_dimension1', orderable: false,searchable:false},
  {data: 'tank_dimension2', orderable: false,searchable:false},
  {data: 'tank_dimension3', orderable: false,searchable:false},
  {data: 'tank_diameter', orderable: false,searchable:false},
  {data: 'installation_date', orderable: false},
  {data: 'generator_name', orderable: false},
  {data: 'generator_id', orderable: false},
  @can('edit')
  {data: 'edit', orderable: false,searchable:false},
  @endcan
  @can('delete')
  {data: 'delete', orderable: false,searchable:false},
 @endcan
        ]
            });
        });
        
    </script>



@endsection