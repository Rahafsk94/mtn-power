@extends('layouts.app')
@section('content')

<form action="{{route('warehouses.update',$warehouse->id)}}" method="POST">

@csrf
    @method('PUT')

    @if ($errors->any())


<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>{{ $error }}</li>
       @endforeach
   </ul>
</div>

@endif




<div class="raw">
  
  <div class="columnn">
  
      <div class="form-group">
          <strong>Warehouse name:</strong>
          <input type="text" name="name" value="{{$warehouse->name}}" class="form-control" required>
  
      </div>
  </div>
  <div class="columnn">
      <div class="form-group">
              <strong>Owner:</strong>
  
  
              <input type="text" name="owner" value="{{$warehouse->owner}}" class="form-control"  required>
  
          </div>
          </div>
          <div class="columnn">
      <div class="form-group">
              <strong>Location:</strong>
  
  
              <input type="text" name="location" value="{{$warehouse->location}}" class="form-control" required>
  
          </div>
          <div class="pull-right">
      <button class="button">Update</button>
     </div>
      </div>
  
  </div>
</form>
@endsection