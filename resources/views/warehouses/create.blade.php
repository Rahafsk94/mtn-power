@extends('layouts.app')
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
   <ul>
       @foreach ($errors->all() as $error)
           <li>

            {{ $error }}</li>
       @endforeach
   </ul>
</div>
@endif
<form action="{{route('warehouses.store')}}" method="POST">

@csrf



<div class="raw">

<br><br>
<div class="card-body">
        <div class="form-group" >
            <strong>Warehouse name:</strong>


                <input type="text" name="name" class="form-control" placeholder="Warehouse name" required>
        </div>

        <div class="form-group">
        <strong>Owner:</strong>
        <input type="text" name="owner" class="form-control" placeholder="Owner" required>

    </div>
    <div class="form-group">
        <strong>Location:</strong>
        <input type="text" name="location" class="form-control" placeholder="Location" required>

    </div>
  

          
       

<div class="pull-right">
    <button class="button">create</button>
   </div>
    </div>

</div>


</form>
@endsection