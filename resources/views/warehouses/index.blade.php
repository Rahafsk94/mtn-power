@extends('layouts.app')
@section('content')

@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif
@if(session()->get('danger'))
                        <div class="alert alert-danger">
                            {{ session()->get('danger') }}
                        </div>
                    @endif
<center>
        <h3 class="header">Warehouses Information</h3>


    </center>
<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            <div class="pull-right">

                   
                                <a href="{{route('warehouses.create')}}">    <button type="button" class="btn btn-outline-success" >Add new warehouse</button></a>
                        
                               
</div>
                        </h4>
               
                        <h6>

                        <div class="dropdown">
  <button class="btn btn-outline-danger" >Warehouse equipments</button>
  <div id="myDropdown" class="dropdown-content">
    <a href="{{route('generatorswarehouse')}}">Generators</a>
    <a href="{{route('batterywarehouse')}}">Batteries</a>
    <a href="{{route('cabinetwarehouse')}}">Cabinets</a>
    <a href="{{route('solarswarehouse')}}">Solars</a>
  </div>
</div>

                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Warehouse name</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Owner</span> </th>
 
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Location</span> </th>
             
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Created at</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator </span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Battery</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Solar</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Cabinet</span> </th>
            @can('edit')
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span> </th>
            @endcan
           
         </tr>
        <tbody>
            @foreach ($warehouses as $v)
            <tr>
          
            <td style="text-align:center">{{$v->name}}</td>

            <td style="text-align:center">{{$v->owner}}</td>
            <td style="text-align:center">{{$v->location}}</td>
            <td style="text-align:center">{{$v->created_at->format('d- m- Y')}}</td>
            <td style="text-align:center">{{$v->generator}}</td>
            <td style="text-align:center">{{$v->battery}}</td>
            <td style="text-align:center">{{$v->solar}}</td>

    
            <td style="text-align:center">{{$v->cabinet}}</td>
          @can('edit')
            <td style="text-align:center"><a  class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('warehouses.edit',$v->id)}}"></a></td>
            @endcan
          
          
            @endforeach
        </table>
     
        
</form>


                    </div>
        
        

                
                </div>
               
            </div>
        </div>
    </div>


    <script>


$(document).ready(function() {

    
    
   


var table = $('#example').DataTable( {
    



   orderCellsTop: true,
   fixedHeader: true,
   dom: 'Bfrtip',       
   buttons: [
           
           {
                       extend: 'excel',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'csv',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'pdf', 
                       exportOptions: {
                           columns: ':visible'
                       }},
                       {
                       extend:  'copy',
                       exportOptions: {
                           columns: ':visible'
                       }},
                       
                       'colvis'
          ],

columns: [
    {data: 'name', name: 'name'},
  {data: 'owner', orderable: false,},

  {data: 'location', orderable: false},
  {data: 'created_at', orderable: false},
  {data: 'generator', orderable: false},
  {data: 'battery', orderable: false},
  {data: 'solar', orderable: false},

  {data: 'cabinet', orderable: false},

@can('edit')
{  data: 'edit', orderable: false },
@endcan


        ]
});
});



$(document).ready(function(){
    
    $('.editbtn').on('click', function(){

        $('#editmodal').modal('show');
        $tr =$(this).closest('tr');
        var data= $tr.children("td").map(function(){
          return $(this).text();
        }).get();

       
        $('#generator_id').val(data[0]);
     
      
        
    });
});







</script>



<script>
 
function test(a) {
var x = a.options[a.selectedIndex].label;
if(x=='reshuffling') { $("#add_fields_placeholderValue").show();}
else{
    $("#add_fields_placeholderValue").hide();
}
}


</script>





@endsection








