@extends('layouts.app')
@section('content')


<div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>
                            
All sites with its power solution
                            <div class="pull-right">

</div>
                        </h4>
               
                        <h6>
                    
            
                        </h6>
                  
                       
                    </div>
                    <div class="card-body">

                        <table class="table table-bordered table-striped" id="example" width="100%">
                            <thead>
                                <tr>


            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Warehouse</span> </th>

            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Generator</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Battery</span> </th>
           
            


            
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Solar</span> </th>
            <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Cabinet</span> </th>

            
         </tr>
        <tbody>
            @foreach ($warehouses as $v)
            <tr>
          
            
            <td style="text-align:center">{{$v->name}}</td>
            <td style="text-align:center">{{$v->generator}}</td>
            <td style="text-align:center">{{$v->battery}}</td>
      
            <td style="text-align:center">{{$v->solar}}</td>
            <td style="text-align:center">{{$v->cabinet}}</td>
            @endforeach
        </table>
@endsection