@extends('layouts.app')

@section('content')
@section('title')
Roles
@stop
@if ($message=Session::get('success'))


<div class="container">
    <div class="alert alert-primary" role="alert">
        {{$message}}
    </div>
</div>
@endif


<!-- row -->

<center>
        <h3 class="header">  <p> Roles Information</p></h3>

    </center>
    <div class="card">
<div class="card-header">
<h4>
                            <div class="pull-right">

                                <a href="{{route('roles.create')}}">    <button type="button" class="btn btn-outline-danger" >Add new role</button></a>
           

</div>
                        </h4>
</div>
<div class="card-body">

        <table class="table table-bordered table-striped" id="datatable" width="100%">
        <thead>
                                <tr>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Role id</span> </th>

                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Role name</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Edit</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Show</span> </th>
                                <th style="text-align:center" class="formTableTitle" style="text-align:center" nowrap ><span class="tableheadtext" >Delete</span> </th></tr>
                    <tbody>
@foreach ( $roles as  $item)




                    <tr>
                    <td style="text-align:center">{{$item->id}}</td>
                    
                    <td style="text-align:center">{{$item->name}}</td>
               
                  
            <td style="text-align:center">   
    
            
            <a class='fa fa-edit' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('roles.edit',$item->id)}}"></a> 
</td>
            <td style="text-align:center">   
            <a class='fa fa-eye' style='color: #00678f' style="font-size: 19px;   color: rgb(255, 0, 0);" href="{{route('roles.show',$item->id)}}"></a> 
            </td>
            <td style="text-align:center"> 
            <form action="{{route('roles.destroy',$item->id)}}" method="Post">
            @csrf
          @method('DELETE')
       <a>  <button type="submit"  class='bi bi-trash'  style='color: red' style="font-size: 19px;   color: rgb(255, 0, 0); " onclick="return confirm('delete ??')" ></button> </a> 
            </form>
       

</td>
          

                             
                           @endforeach
</tr>
</tbody>
</div>

</form>
</table>

</div>
@endsection
