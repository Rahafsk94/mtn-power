@extends('layouts.app')

@section('title')
 edit the {{$role->name}} role

@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">Permissions</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ 
            Edit Permission</span>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection

@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <button aria-label="Close" class="close" data-dismiss="alert" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>error</strong>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


{!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
<!-- row -->
<div class="roles-card">
<div class="card">
    <h5 class="card-header"> edit {{$role->name}}</h5>
    <div class="card-body">
      <h5 class="card-title"> Role name:<br>
        <br>
        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}

      </h5>
      <p class="card-text">
        <ul id="treeview1">
            <a href="#">Permissions</a>
                <ul>

                        @foreach($permission as $value)
                        <li>
                        <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                            {{ $value->name }}</label>
                        <br />
                    </li>
                        @endforeach


                </ul>

        </ul>

       <div class="pull-right">
        <button class="btn btn-primary btn-sm" type="submit" >Update</button>
    </div>
    </div>
  </div>
  </div>
@endsection

