@extends('layouts.app')





@section('content')



<!-- row -->
<div class="row">
    <div class="col-md-12">
        <div class="card mg-b-20">
            <div class="card-body">
                <h5 class="card-header"> Role name:     {{$role->name}}</h5>
                <div class="main-content-label mg-b-5">
            </div>
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4">
                        <ul id="treeview1">
                            <li><h6 class="card-title">Role permissions</h6>
                                <ul>
                                    @if(!empty($rolePermissions))
                                    @foreach($rolePermissions as $v)
                                    <li>{{ $v->name }}</li>
                                    @endforeach
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /col -->

                </div>
                <div class="pull-right">
                    <a class="btn btn-primary btn-sm" href="{{ route('roles.index') }}">go back</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- row closed -->
</div>
<!-- Container closed -->
</div>
<!-- main-content closed -->
@endsection

